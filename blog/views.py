from django.shortcuts import render, get_object_or_404
# Importar paginador
from django.core.paginator import Paginator
from blog.models import Category, Article

# Create your views here.


def filter_object(filter):
    object = Article.objects.all()

    if filter == 'recent':
        object = Article.objects.order_by('created_at')
    elif filter == '-recent':
        object = Article.objects.order_by('-created_at')
    elif filter == 'az':
        object = Article.objects.order_by('title')
    elif filter == '-az':
        object = Article.objects.order_by('-title')

    return object


def list_articles(request):
    # sacar articulo
    # articles = Article.objects.all()
    filter_get = request.GET.get('filter')

    articles = filter_object(filter_get)

    # Paginar articulo
    # define cuantos objetos por vista quiero tener
    paginator = Paginator(articles, 6)

    # RECOGER PAGIN
    page = request.GET.get('page')
    page_article = paginator.get_page(page)

    return render(request, 'pages/blog/list_articles.html', {
        'title': 'Blog',
        'articles': page_article
    })


def category(request, category_id):
    category = get_object_or_404(Category, id=category_id)

    # tambien uedo hacer category.article_set.all para traer los articulos de la categoria
    # para esto en el modelo poner related_name en vez de article_set
    filter_get = request.GET.get('filter')

    articles = Article.objects.filter(categorys=category)

    if filter_get == 'recent':
        articles = Article.objects.filter(
            categorys=category).order_by('created_at')
    elif filter_get == '-recent':
        articles = Article.objects.filter(
            categorys=category).order_by('-created_at')
    elif filter_get == 'az':
        articles = Article.objects.filter(categorys=category).order_by('title')
    elif filter_get == '-az':
        articles = Article.objects.filter(
            categorys=category).order_by('-title')

    # Paginar articulo
    # define cuantos objetos por vista quiero tener
    paginator = Paginator(articles, 6)

    # RECOGER PAGIN
    page = request.GET.get('page')
    articles = paginator.get_page(page)

    return render(request, 'pages/blog/list_categories.html', {
        'category': category,
        'articles': articles
    })


def article(request, article_id):
    article = get_object_or_404(Article, id=article_id)
    categorys = Category.objects.all()
    categorys_articles = article.categorys.all()
    dict_articles_for_category = []
    dict_articles_related = []

    for categorys_article in categorys_articles:
        category = get_object_or_404(Category, id=categorys_article.id)
        related_articles = Article.objects.filter(categorys=category)

        for related_article in related_articles:
            if related_article.id != article.id:

                dict_articles_related.append(related_article)

    for category in categorys:
        category = get_object_or_404(Category, id=category.id)

        articles_in_category = Article.objects.filter(
            categorys=category
        ).count()

        if articles_in_category > 0:
            list_count = {
                'category': category,
                'count': articles_in_category,
                'slug': f'/blog/categoria/{category.id}'
            }

            dict_articles_for_category.append(list_count)

    return render(request, 'pages/blog/detail_article.html', {
        'article': article,
        'articles_for_category': dict_articles_for_category,
        'related_articles': dict_articles_related
    })
