from blog.models import Category, Article

# Creando context processor


def get_categorys(request):
    # values list es mejor que all para seleccionar lo que quiero tener
    # pages = Page.objects.all()

    ids_categorys_in_use = Article.objects.values_list('categorys', flat=True)
    # pages = Page.objects.values_list('id', 'title', 'slug')

    # filter para paginas visibles
    # Obtener solo las categorias con articulos en eso
    categorys = Category.objects.filter(
        id__in=ids_categorys_in_use).values_list('id', 'name')
    # el parametro flat true me permite pasar un texto plano a la vista solo cuando tengo un valor en el value

    return {
        'categorys': categorys,
        'ids': ids_categorys_in_use,
    }


def get_public_articles(request):
    articles_publics = Article.objects.all()

    return {
        'articles_publics': articles_publics
    }
