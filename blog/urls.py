from django.urls import path
from . import views

urlpatterns = [
    # My urls
    path('', views.list_articles, name="blog"),
    path('categoria/<int:category_id>', views.category, name="category"),
    path('articulo/<int:article_id>', views.article, name="article"),
]
