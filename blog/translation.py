from modeltranslation.translator import translator, TranslationOptions
from .models import Article, Category


class CategoryTranslationOptions(TranslationOptions):
    fields = ('name', 'description')


class ArticleTranslationOptions(TranslationOptions):
    fields = ('title', 'description', 'content')


translator.register(Category, CategoryTranslationOptions)
translator.register(Article, ArticleTranslationOptions)
