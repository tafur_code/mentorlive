from django.shortcuts import render, get_object_or_404
# Importar paginador
from django.db import models
from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.conf import settings
from django.db.models.deletion import CASCADE
from ckeditor.fields import RichTextField

# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=100, verbose_name='Nombre')
    description = models.CharField(max_length=255, verbose_name='Descripción')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Creado el')

    class Meta:
        verbose_name = 'Categoria'
        verbose_name_plural = 'Categorias'

    def __str__(self):
        return self.name


class Article(models.Model):
    title = models.CharField(max_length=150, verbose_name='Titulo')
    description =models.TextField(
        max_length=200,
        verbose_name='Pequeña descripción',
        blank=True,
        null=True,
        default=' ',
        help_text='Maximo 200 palabas'
    )
    content = RichTextField(verbose_name='Contenido')
    # Guardar imagen dentro de la carpeta media/articles
    image = models.ImageField(
        default='null', verbose_name='Imagen', upload_to='articles')
    public = models.BooleanField(verbose_name='Publicado')

    # relaciones con modelos
    # Dentro de user guarda el id del usuario del artiuclo -> foreign para clave foranea
    # uno a uno
    user = models.ForeignKey(settings.AUTH_USER_MODEL, editable=False,
                             verbose_name='Usuario', on_delete=models.CASCADE)

    # uno a muchos, muchos articulos pueden tener multiples categorias
    categorys = models.ManyToManyField(
        Category, verbose_name='Categorias', blank=True)

    # muchos a uno
    # categorys = models.ManyToOneRel

    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Creado el', blank=True)
    updated_at = models.DateTimeField(
        auto_now=True, verbose_name='Actualizado el', blank=True)

    class Meta:
        verbose_name = 'Articulo'
        verbose_name_plural = 'Articulos'
        ordering = ['-created_at']

    def __str__(self):
        return self.title
