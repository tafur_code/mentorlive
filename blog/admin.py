from django.contrib import admin
from .models import Category, Article

class CategoryAdmin(admin.ModelAdmin):
  # Para agregar campos de solo lectura en los modelos
  readonly_fields = ('created_at',)
  list_display = ('name', 'created_at')
  search_fields = ('name', 'description')

class ArticleAdmin(admin.ModelAdmin):
  # funciones para filtros busquedas y campos en el administrador
  readonly_fields = ('user', 'created_at', 'updated_at')
  search_fields = ('title', 'user__username')
  list_display = ('title', 'user', 'public')

  def save_model(self, request, object, form, change):
    if not object.user_id:
      object.user_id = request.user.id
    
    object.save()

# Register your models here.
admin.site.register(Category, CategoryAdmin)
admin.site.register(Article, ArticleAdmin)