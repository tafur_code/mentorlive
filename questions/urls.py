from django.urls import path
from . import views

urlpatterns = [
    # My urls
    path('preguntas-frecuentes/', views.frecuent_questions, name="frecuent_question"),
]
