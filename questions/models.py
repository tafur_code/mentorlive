from django.db import models
from django.utils.translation import gettext_lazy as _


# Create your models here.
class QuickQuestions(models.Model):
    title = models.CharField(
      max_length=200, verbose_name='Titulo pregunta rapida', blank=False)
    subtitle = models.TextField(max_length=1000, verbose_name='Respuesta pregunta rapida')

    class Meta:
        verbose_name = 'Pregunta rapida'
        verbose_name_plural = 'Preguntas rapidas'

    def __str__(self):
        return self.title

CATEGORYS = (
    ('', _('Seleccione una categoria')),
    ('general', _('General')),
    ('mentores', _('Mentores')),
    ('emprendedores', _('Emprendedores')),
    ('empresas', _('Empresas')),
    ('aliados', _('Aliados')),
)

class FrecuentQuestions(models.Model):
    category = models.CharField(
        'Categoria',
        max_length=100,
        choices=CATEGORYS,
    )
    title = models.CharField(
      max_length=200, verbose_name='Titulo pregunta frecuente', blank=False)
    subtitle = models.TextField(max_length=1000, verbose_name='Respuesta pregunta frecuente')

    class Meta:
        verbose_name = 'Pregunta frecuente'
        verbose_name_plural = 'Preguntas frecuentes'

    def __str__(self):
        return self.title