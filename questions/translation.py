from modeltranslation.translator import translator, TranslationOptions
from .models import QuickQuestions, FrecuentQuestions


class QuickQuestionsTranslationOptions(TranslationOptions):
    fields = ('title', 'subtitle')

class FrecuentQuestionsTranslationOptions(TranslationOptions):
    fields = ('title', 'subtitle')


translator.register(QuickQuestions, QuickQuestionsTranslationOptions)
translator.register(FrecuentQuestions, FrecuentQuestionsTranslationOptions)
