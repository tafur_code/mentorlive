import imp
from django.shortcuts import render

from .models import FrecuentQuestions

# Create your views here.


def frecuent_questions(request):
    frecuent_questions = FrecuentQuestions.objects.all()

    return render(request, "pages/frecuent_question.html", {
        "frecuent_questions": frecuent_questions,
        "title": "Centro de ayuda"
    })
