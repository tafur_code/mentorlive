from django.contrib import admin
from .models import QuickQuestions, FrecuentQuestions

# Register your models here.


class QuickQuestionsAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    list_display = ('title', 'subtitle')


class FrecuentQuestionsAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    list_display = ('title', 'subtitle')


admin.site.register(QuickQuestions, QuickQuestionsAdmin)
admin.site.register(FrecuentQuestions, FrecuentQuestionsAdmin)
