from audioop import mul
from collections import Counter


lista = [5, 1]

conteo = Counter(lista)

resultado = {}
rta_multiplicacion = []
list_clave = []

for clave in conteo:
    valor = conteo[clave]

    if valor != 0:
        temp = 0
        resultado[clave] = valor

        multiplicacion = clave * valor
        rta_multiplicacion.append(multiplicacion)

        list_clave.append(clave)


print("rta_multi")
print(rta_multiplicacion)
print("---")

print("sum")
print(sum(rta_multiplicacion))
print(sum(list_clave))
print("---")

division = sum(rta_multiplicacion) / sum(list_clave)
print("division")
print(round(division * 100 / len(lista)))
print("---")

print("resultado")
print(resultado)
