from django.apps import AppConfig


class CitaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cita'
    verbose_name = "Reunion"
    verbose_name_plural = "Reuniones"