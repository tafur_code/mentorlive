from faulthandler import disable
from django.db import models
from django.conf import settings
from social_django.models import UserSocialAuth
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.urls import reverse

from user.models import UserCustom
from home.functions import validate_href_contnt

# for timezone()
import pytz

STATES_SCHEDULE = (
    ('espera', _('En espera')),
    ('aceptada', _('Aceptada')),
    ('aceptada_emprendedor', _('Aceptada Emprendedor')),
    ('cancelada', _('Cancelada')),
    ('cancelada_emprendedor', _('Cancelada Emprendedor')),
    ('terminada', _('Terminada')),
)

TIPO_REUNION = (
    ('', _('Seleccione una opción')),
    ('En videollamada', _('En videollamada')),
    ('Por correo electrónico', _('Por correo electrónico')),
)

import datetime
# Create your models here.

class Cita(models.Model):
    mentor = models.ForeignKey(
        UserSocialAuth, on_delete=models.CASCADE, verbose_name='Correo de contacto del Mentor')

    emprendedor = models.ForeignKey(UserCustom,
                             verbose_name='Correo de contacto del Emprendedor', on_delete=models.CASCADE)

    fecha_estimada_reunion = models.DateTimeField(
        verbose_name='Fecha estimada de la Mentoría', auto_now=False)

    fecha_confirmada_reunion = models.DateTimeField(
        verbose_name='Confirmar o Reprogramar Fecha', auto_now=False, default=timezone.now)
    
    pais_solicitud = models.CharField(
        'País de solicitud ',
        max_length=100,
    )
    
    medio_respuesta = models.CharField(
        'Medio por el que recibira la respuesta a esta solicitud de Mentoría',
        max_length=100,
        default="Correo electrónico",
    )
    
    tipo_reunion = models.CharField(
        'Cómo le gustaría reunirse con su mentor',
        max_length=100,
        choices=TIPO_REUNION,
    )

    description_text = models.TextField(
        max_length=500, verbose_name='Motivo del encuentro')

    state = models.CharField(
        'Estado',
        max_length=100,
        choices=STATES_SCHEDULE,
        blank=True
    )

    description_cancel = models.TextField(
        max_length=500, verbose_name='Motivo de cancelación', blank=True)
        
    description_confirmation = models.TextField(
        max_length=500, verbose_name='Mensaje confirmación', blank=True)
    
    slug = models.CharField(
        max_length=500, verbose_name='Enlace a la reunion', blank=True,
    )

    created_at = models.DateTimeField(
    #    verbose_name='Creado el', blank=True, default=datetime.datetime.now())
       verbose_name='Creado el', blank=True, default=timezone.now)
    
    reprogramada = models.CharField(
        max_length=1, verbose_name="Reprogramada", blank=True, default=""
    )

    class Meta:
        verbose_name = 'Mentoría'
        verbose_name_plural = 'Mentorias'

    def __str__(self):
        return f"{ self.created_at } - { self.mentor }"

    def get_absolute_url(self):
        return reverse("profile")
