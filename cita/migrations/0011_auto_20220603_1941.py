# Generated by Django 3.2.8 on 2022-06-04 00:41

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cita', '0010_auto_20220524_2151'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cita',
            name='created_at',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2022, 6, 3, 19, 41, 40, 887261), verbose_name='Creado el'),
        ),
        migrations.AlterField(
            model_name='cita',
            name='fecha_confirmada_reunion',
            field=models.DateTimeField(default=datetime.datetime(2022, 6, 3, 19, 41, 40, 887126), verbose_name='Confirmar o Reprogramar Fecha'),
        ),
    ]
