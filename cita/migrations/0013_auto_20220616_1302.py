# Generated by Django 3.2.8 on 2022-06-16 18:02

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('cita', '0012_auto_20220616_1248'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cita',
            name='created_at',
            field=models.DateTimeField(blank=True, default=datetime.datetime(2022, 6, 16, 13, 2, 58, 421376), verbose_name='Creado el'),
        ),
        migrations.AlterField(
            model_name='cita',
            name='fecha_confirmada_reunion',
            field=models.DateTimeField(default=datetime.datetime(2022, 6, 16, 18, 2, 58, 421218, tzinfo=utc), verbose_name='Confirmar o Reprogramar Fecha'),
        ),
    ]
