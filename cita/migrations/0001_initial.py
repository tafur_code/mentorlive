# Generated by Django 3.2.8 on 2022-04-14 17:10

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('social_django', '0010_uid_db_index'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cita',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha_estimada_reunion', models.DateTimeField(verbose_name='Fecha estimada de la reunión')),
                ('pais_solicitud', models.CharField(max_length=100, verbose_name='País de solicitud ')),
                ('medio_respuesta', models.CharField(default='Correo electrónico', max_length=100, verbose_name='Medio por el que recibira la respuesta a esta solicitud de mentoría')),
                ('tipo_reunion', models.CharField(blank=True, choices=[('', 'Seleccione una opción'), ('En persona', 'En persona'), ('En videollamada', 'En videollamada'), ('Por correo electrónico', 'Por correo electrónico'), ('Por Teléfono', 'Por Teléfono')], max_length=100, verbose_name='Cómo le gustaría reunirse con su mentor')),
                ('description_text', models.TextField(max_length=500, verbose_name='Motivo del encuentro')),
                ('state', models.CharField(blank=True, choices=[('espera', 'En espera'), ('aceptada', 'Aceptada'), ('cumplida', 'Cumplida'), ('cancelada', 'Cancelada')], max_length=100, verbose_name='Estado')),
                ('description_cancel', models.TextField(blank=True, max_length=500, verbose_name='Motivo de cancelación')),
                ('description_confirmation', models.TextField(blank=True, max_length=500, verbose_name='Mensaje confirmación')),
                ('emprendedor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Correo de contacto del Emprendedor')),
                ('mentor', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='social_django.usersocialauth', verbose_name='Correo de contacto del Mentor')),
            ],
            options={
                'verbose_name': 'Reunion',
                'verbose_name_plural': 'Reuniones',
            },
        ),
    ]
