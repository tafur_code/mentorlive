import datetime
from django import forms
from django.utils.translation import gettext_lazy as _
from django.db.models import Q
from social_django.models import UserSocialAuth

from home.functions import send_user_mail
from cita.models import Cita
from user.models import UserCustom


def validate_fecha_estimada(schedule):

    current_time = datetime.datetime.now()

    a = datetime.datetime(current_time.year, current_time.month, current_time.day,
                          current_time.hour, current_time.minute, current_time.second)

    b = datetime.datetime(schedule.year, schedule.month, schedule.day,
                          schedule.hour, schedule.minute, schedule.second)

    c = b-a

    if c.total_seconds() <= 172800:
        raise forms.ValidationError(
            _("Solo se puede agendar una Mentoría con 48 horas de anticipación"))


class CreateCita(forms.ModelForm):

    class Meta:
        model = Cita
        fields = (
            'mentor',
            'emprendedor',
            'fecha_estimada_reunion',
            'pais_solicitud',
            'medio_respuesta',
            'tipo_reunion',
            'description_text',
        )

    def clean_emprendedor(self):
        emprendedor = self.cleaned_data.get(
            'emprendedor'
        )

        user = UserCustom.object.filter(email=emprendedor).first()

        citas_count = Cita.objects.filter(emprendedor=user).filter(
            ~Q(state='cancelada')).filter(~Q(state='cancelada_emprendedor')).filter(~Q(state='terminada')).exists()

        if citas_count:
            raise forms.ValidationError(
                _("Este emprendedor ya tiene una Mentoría en curso."))

        return emprendedor

    def clean_fecha_estimada_reunion(self):
        fecha_estimada_reunion = self.cleaned_data.get(
            'fecha_estimada_reunion')

        validate_fecha_estimada(fecha_estimada_reunion)

        return fecha_estimada_reunion

    def clean(self):
        mentor = self.cleaned_data.get(
            'mentor'
        )
        user_emprendedor = self.cleaned_data.get(
            'emprendedor'
        )

        user = UserCustom.object.filter(email=mentor).first()
        # user_emprendedor = UserCustom.object.filter(email=emprendedor).first()
        user_social = UserSocialAuth.objects.filter(user=user).first()

        citas_canceladas = Cita.objects.filter(emprendedor=user_emprendedor).filter(
            mentor=user_social).filter(state='cancelada_emprendedor').exists()

        if citas_canceladas:
            self.add_error("mentor", _(
                "No puedes volver a asignar Mentorías con este mentor."))

    def save(self, commit=True):
        cita = super().save(commit=False)
        cita.state = 'espera'

        send_user_mail(cita, 'schedule_user')

        if commit:
            cita.save()

        return cita


class EditCita(forms.ModelForm):
    """Formulario para editar una cita en la BD
    """
    id_cita = forms.CharField(label="Id cita", required=False)

    description_confirmation = forms.CharField(
        label="Déjale un mensaje  de respuesta al emprendedor junto al enlace de la reunión o videoconferencia si así se acordó", max_length=500, required=False)

    slug = forms.CharField(
        label="Enlace a la Mentoría", max_length=500, required=True)

    class Meta:
        model = Cita
        fields = (
            'mentor',
            'emprendedor',
            'fecha_confirmada_reunion',
            'pais_solicitud',
            'medio_respuesta',
            'tipo_reunion',
            'description_cancel',
            'description_confirmation',
            'slug',
            'state',
            'reprogramada',
            'id_cita'
        )

    def clean_fecha_confirmada_reunion(self):
        fecha_confirmada_reunion = self.cleaned_data.get(
            'fecha_confirmada_reunion')

        validate_fecha_estimada(fecha_confirmada_reunion)

        return fecha_confirmada_reunion

    def clean(self):
        cleaned_data = self.cleaned_data

        cita_id = self.cleaned_data.get(
            'id_cita'
        )

        if cita_id:
            cita = Cita.objects.filter(id=cita_id).first()

            if not cita.reprogramada == "0":
                self.add_error("fecha_confirmada_reunion", _(
                    "Esta mentoría ya tiene fecha de reprogramación."))
            else:
                return cleaned_data

    def save(self, commit=True):
        schedule = super().save(commit=False)

        if commit:
            schedule.save()
            send_user_mail(schedule, 'schedule_state_user')

        return schedule


class EditCitaConfirmar(forms.ModelForm):

    class Meta:
        model = Cita
        fields = (
            'state',
            'fecha_estimada_reunion',
        )

    def save(self, commit=True):
        schedule = super().save(commit=False)
        current_time = datetime.datetime.now()

        a = datetime.datetime(current_time.year, current_time.month, current_time.day,
                              current_time.hour, current_time.minute, current_time.second)

        b = datetime.datetime(schedule.fecha_confirmada_reunion.year, schedule.fecha_confirmada_reunion.month, schedule.fecha_confirmada_reunion.day,
                              schedule.fecha_confirmada_reunion.hour, schedule.fecha_confirmada_reunion.minute, schedule.fecha_confirmada_reunion.second)

        c = b-a

        if c.total_seconds() <= 86400:
            schedule.state = "cancelada"
            schedule.description_cancel = "Se cancela por no confirmarla en el tiempo establecido."
        else:
            schedule.state = "aceptada_emprendedor"

        if commit:
            schedule.save()
            send_user_mail(schedule, 'schedule_state_user')

        return schedule


class EditCitaRemove(forms.ModelForm):

    class Meta:
        model = Cita
        fields = (
            'state',
            'fecha_confirmada_reunion',
        )

    def save(self, commit=True):
        schedule = super().save(commit=False)
        current_time = datetime.datetime.now()

        a = datetime.datetime(current_time.year, current_time.month, current_time.day,
                              current_time.hour, current_time.minute, current_time.second)

        b = datetime.datetime(schedule.fecha_confirmada_reunion.year, schedule.fecha_confirmada_reunion.month, schedule.fecha_confirmada_reunion.day,
                              schedule.fecha_confirmada_reunion.hour, schedule.fecha_confirmada_reunion.minute, schedule.fecha_confirmada_reunion.second)

        c = b-a

        if c.total_seconds() <= 86400:
            schedule.state = "cancelada_emprendedor"
            schedule.description_cancel = "Se cancela por no confirmarla en el tiempo establecido."

        if commit:
            schedule.save()
            send_user_mail(schedule, 'schedule_state_user')

        return schedule


class EditCitaFinish(forms.ModelForm):

    class Meta:
        model = Cita
        fields = (
            'state',
            'fecha_confirmada_reunion',
        )

    def save(self, commit=True):
        schedule = super().save(commit=False)
        current_time = datetime.datetime.now()

        a = datetime.datetime(current_time.year, current_time.month, current_time.day,
                              current_time.hour, current_time.minute, current_time.second)

        b = datetime.datetime(schedule.fecha_confirmada_reunion.year, schedule.fecha_confirmada_reunion.month, schedule.fecha_confirmada_reunion.day,
                              schedule.fecha_confirmada_reunion.hour, schedule.fecha_confirmada_reunion.minute, schedule.fecha_confirmada_reunion.second)

        c = b-a

        if schedule.fecha_confirmada_reunion < current_time:
            schedule.state = "terminada"

        if commit:
            schedule.save()
            send_user_mail(schedule, 'schedule_state_user')

        return schedule
