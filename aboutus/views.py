from django.shortcuts import render

from aboutus.models import Banner, Patterns, DescriptionAbout, AboutUs, Valores
from user.models import UserCustom

# Create your views here.


def about_us(request):

    banner = Banner.objects.all()
    patterns = Patterns.objects.all()
    description_about = DescriptionAbout.objects.all()
    mentors = UserCustom.object.filter(user_rol='1')
    aboutus = AboutUs.objects.all()
    valores = Valores.objects.all()

    return render(request, 'pages/about_us.html', {
        'title': "Sobre nosotros",
        'banner': banner,
        'patterns': patterns,
        'description_about': description_about,
        'list_mentors': mentors,
        'aboutus': aboutus,
        'valores': valores
    })
