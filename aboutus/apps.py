from django.apps import AppConfig


class AboutusConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'aboutus'
    verbose_name = 'Gestión de la página sobre nosotros'
