from django.urls import path
from . import views

urlpatterns = [
    # My urls
    path('sobre-nosotros', views.about_us, name="about_us"),
]
