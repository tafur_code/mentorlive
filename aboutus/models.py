from django.db import models
from home.functions import validate_href_contnt

# Create your models here.


class Banner(models.Model):
    title = models.CharField(
        max_length=100,
        verbose_name="Titulo"
    )
    description = models.TextField(
        max_length=500, verbose_name='Descripción')
    text_button = models.CharField(
        max_length=50, verbose_name='Texto botón', blank=True)
    href_button = models.CharField(
        max_length=1000,
        verbose_name='Enlace del botón',
        blank=True,
        validators=[validate_href_contnt]
    )
    image = models.ImageField(
        default='null', verbose_name='Imagen informativa', upload_to='banner_centered')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Creado el')

    class Meta:
        verbose_name = 'Banner quienes somos'
        verbose_name_plural = 'Banner quienes somos'

    def __str__(self):
        return self.title


class AboutUs(models.Model):
    image_aobutus = models.ImageField(
        default='null', verbose_name='Imagen informativa', upload_to='about_us')
    title_aboutus = "Información general Quienes somos"
    paragraph_one = models.TextField(
        max_length=1000,
        verbose_name="Parrafo uno Quienes somos"
    )
    paragraph_two = models.TextField(
        max_length=1000,
        verbose_name="Parrafo dos Quienes somos"
    )
    paragraph_our_work = models.TextField(
        max_length=1000,
        verbose_name="Parrafo Nuestro trabajo"
    )
    image_mision_vision = models.ImageField(
        default='null', verbose_name='Imagen informativa mision/vision', upload_to='about_us')
    paragraph_mision = models.TextField(
        max_length=1000,
        verbose_name="Parrafo Misión"
    )
    paragraph_vision = models.TextField(
        max_length=1000,
        verbose_name="Parrafo Visión"
    )
    paragraph_plan = models.TextField(
        max_length=1000,
        verbose_name="Parrafo Plan estratégico"
    )

    class Meta:
        verbose_name = 'Información general Quienes somos'

    def __str__(self):
        return self.title_aboutus


class DescriptionAbout(models.Model):
    title = "Descripción sobre nosotros con icono"
    description_text = models.TextField(
        max_length=500, verbose_name='Descripción sobre nosotros')
    image = models.ImageField(
        default='null', verbose_name='Imagen informativa', upload_to='about_us')
    number_icon_first = models.CharField(
        max_length=50, verbose_name='Número card uno')
    text_icon_first = models.CharField(
        max_length=100, verbose_name='Texto card uno')
    number_icon_second = models.CharField(
        max_length=50, verbose_name='Número card dos')
    text_icon_second = models.CharField(
        max_length=100, verbose_name='Texto card dos')
    number_icon_third = models.CharField(
        max_length=50, verbose_name='Número card tres')
    text_icon_third = models.CharField(
        max_length=100, verbose_name='Texto card tres')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Creado el')

    class Meta:
        verbose_name = 'Descripción sobre nosotros'
        verbose_name_plural = 'Descripción sobre nosotros'

    def __str__(self):
        return self.title


class Patterns(models.Model):
    title = models.CharField(max_length=100, verbose_name='Nombre aliado')
    brand_pattern = models.ImageField(
        default='null', verbose_name='Logo del aliado', upload_to='pattern_image')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Creado el')

    class Meta:
        verbose_name = 'Aliado'
        verbose_name_plural = 'Aliados'

    def __str__(self):
        return self.title

class Valores(models.Model):
    title = models.CharField(max_length=100, verbose_name='Titulo')
    text = models.TextField(max_length=500, verbose_name='Texto')

    class Meta:
        verbose_name = 'Valor'
        verbose_name_plural = 'Valores'

    def __str__(self):
        return self.title