from django.contrib import admin
from .models import Banner, DescriptionAbout, Patterns, AboutUs, Valores

# Register your models here.


class BannerAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    readonly_fields = ('created_at',)
    list_display = ('title', 'created_at')
    search_fields = ('title', 'description')


class AboutUsAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    list_display = ('title_aboutus',)


class DescriptionAboutAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    readonly_fields = ('created_at',)
    list_display = ('title', 'created_at')
    search_fields = ('title', 'description')


class PatternsAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    readonly_fields = ('created_at',)
    list_display = ('title', 'created_at')
    search_fields = ('title', 'description')

class ValoresAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    list_display = ('title', 'text')
    search_fields = ('title', 'text')


admin.site.register(Banner, BannerAdmin)
admin.site.register(DescriptionAbout, DescriptionAboutAdmin)
admin.site.register(Patterns, PatternsAdmin)
admin.site.register(AboutUs, AboutUsAdmin)
admin.site.register(Valores, ValoresAdmin)
