# Generated by Django 3.2.8 on 2022-02-16 00:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('aboutus', '0004_banner_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='descriptionabout',
            name='image',
            field=models.ImageField(default='null', upload_to='about_us', verbose_name='Imagen informativa'),
        ),
    ]
