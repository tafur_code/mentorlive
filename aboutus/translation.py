from modeltranslation.translator import translator, TranslationOptions
from .models import Banner, DescriptionAbout, AboutUs, Valores


class BannerTranslationOptions(TranslationOptions):
    fields = ('title', 'description', 'text_button')


class AboutUsTranslationOptions(TranslationOptions):
    fields = ('paragraph_one', 'paragraph_two', 'paragraph_our_work',
              'paragraph_mision', 'paragraph_vision', 'paragraph_plan')


class DescriptionAboutTranslationOptions(TranslationOptions):
    fields = ('description_text', 'text_icon_first',
              'text_icon_second', 'text_icon_third')

class ValoresTranslationOptions(TranslationOptions):
    fields = ('title', 'text',)


translator.register(Banner, BannerTranslationOptions)
translator.register(DescriptionAbout, DescriptionAboutTranslationOptions)
translator.register(AboutUs, AboutUsTranslationOptions)
translator.register(Valores, ValoresTranslationOptions)
