from django.shortcuts import render
from django.views.generic import CreateView
from django.contrib.messages.views import SuccessMessageMixin

from .models import Contact
from .forms import CreateMessageContact

# Create your views here.
class ContactUser(SuccessMessageMixin, CreateView):
    model = Contact
    form_class = CreateMessageContact
    template_name = 'pages/contact.html'
    success_message = "Mensaje enviado con exito"

    def get(self, request):

      form = self.form_class

      return render(request, self.template_name, {'title': 'Contacto', 'form': form})
    # def get_queryset(self):
    #     return self.model.object.filter(is_staff=True)
