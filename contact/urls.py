from django.urls import path
from . import views

urlpatterns = [
    # My urls
    path('contacto/', views.ContactUser.as_view(), name="contact"),
]
