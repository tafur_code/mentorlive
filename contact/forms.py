from django import forms

from home.functions import send_user_mail
from .models import Contact

class CreateMessageContact(forms.ModelForm):
  class Meta:
    model = Contact
    fields = ('name_contact', 'phone_contact', 'email_contact', 'type_question', 'message_contact')

  def save(self, commit=True):
    contact_message = super().save(commit=False)

    if commit:
        contact_message.save()
        send_user_mail(contact_message, 'contact_user')

    return contact_message
