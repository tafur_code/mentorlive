from django.contrib import admin

from .models import Contact

# Register your models here.
class ContactUserAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    readonly_fields = ('name_contact', 'type_question', 'email_contact', 'phone_contact', 'message_contact')
    list_display = ('name_contact', 'type_question')
    search_fields = ('name_contact', 'description')

admin.site.register(Contact, ContactUserAdmin)
