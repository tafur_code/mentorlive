from django.core.checks import messages
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

TYPE_QUESTION = (
    ('pregunta', _('Pregunta')),
    ('inquietud', _('Inquietud')),
    ('reclamo', _('Reclamo')),
)

STATE = (
    ('espera', _('En espera')),
    ('respondida', _('Respondida')),
)

# Create your models here.
class Contact(models.Model):
  name_contact = models.CharField(
    max_length=250, verbose_name='Nombres'
  )
  phone_contact = models.CharField(
      max_length=500, verbose_name='Telefono')
  email_contact = models.CharField(
      max_length=500, verbose_name='Correo electronico')
  type_question = models.CharField(
      'Tipo de solicitud',
      max_length=100,
      choices=TYPE_QUESTION,
  )
  message_contact = models.TextField(
    max_length=500, verbose_name='Mensaje de contacto'
  )
  message_rta = models.TextField(
    max_length=500, verbose_name='Respuesta del admin', default='Sin respuesta'
  )
  state = models.CharField(
      'Estado',
      max_length=100,
      choices=STATE,
  )

  class Meta:
      verbose_name = 'Contacto'
      verbose_name_plural = 'Contactos'

  def get_absolute_url(self):
      return reverse("profile")

  def __str__(self):
      return f'{ self.name_contact } | { self.type_question }'