# Generated by Django 3.2.8 on 2021-12-28 16:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0003_usercustom_user_rol'),
    ]

    operations = [
        migrations.AddField(
            model_name='usercustom',
            name='recover_password',
            field=models.CharField(blank=True, default='null', max_length=500, null=True),
        ),
    ]
