# Generated by Django 3.2.8 on 2022-01-20 23:14

import ckeditor.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0009_auto_20220117_2152'),
    ]

    operations = [
        migrations.AddField(
            model_name='usercustom',
            name='days',
            field=ckeditor.fields.RichTextField(blank=True, help_text='Agregue una lista con sus días de disponibilidad', null=True, verbose_name='Dias disponibilidad'),
        ),
        migrations.AlterField(
            model_name='usercustom',
            name='abilities',
            field=models.TextField(blank=True, default='', help_text='Describa sus habilidades en un parrafo o coma por coma.', max_length=200, null=True, verbose_name='Habilidades'),
        ),
        migrations.AlterField(
            model_name='usercustom',
            name='description',
            field=models.TextField(blank=True, default='', max_length=200, null=True, verbose_name='Descripción'),
        ),
        migrations.AlterField(
            model_name='usercustom',
            name='linkeding_image',
            field=models.CharField(blank=True, default='null', max_length=500, null=True, verbose_name='Imagen linkedin'),
        ),
        migrations.AlterField(
            model_name='usercustom',
            name='managerial_experience',
            field=models.CharField(blank=True, default='', max_length=500, null=True, verbose_name='Experiencia gerencial'),
        ),
        migrations.AlterField(
            model_name='usercustom',
            name='professional_experience',
            field=models.CharField(blank=True, default='null', max_length=500, null=True, verbose_name='Experiencia profesional'),
        ),
        migrations.AlterField(
            model_name='usercustom',
            name='recover_password',
            field=models.CharField(blank=True, default='', max_length=500, null=True),
        ),
    ]
