import datetime
from pyexpat import model
from attr import fields
from django import forms
from django.utils.translation import gettext_lazy as _

from home.functions import send_user_mail
from user.models import GENDER, UserCustom


class UserForm(forms.ModelForm):
    """Formulario de registro para un usuario en la BD

      variables:
        -password1: Contraseña
        -password2: Verficar Contraseña
    """
    password1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Contraseña *',
            'id': 'password1',
            'required': 'required'
        }
    ))

    password2 = forms.CharField(label='Confirmar contraseña', widget=forms.PasswordInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Confirmar contraseña *',
            'id': 'password2',
            'required': 'required'
        }
    ))

    class Meta:
        model = UserCustom
        fields = (_('names'), 'lastnames', 'email', 'avatar_image')
        widget = {
            'names': forms.TextInput(attrs={'placeholder': 'username'}),
            'lastnames': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese su apellidos',
                }
            ),
            'email': forms.EmailInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Correo electrónico',
                }
            ),
            'gender': forms.ChoiceField(
                widget=forms.Select,
                choices=GENDER
            ),
            'avatar_image': forms.ImageField()
        }

    def clean_password2(self):
        """ validación para contraseña antes de encriptar"""

        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Las contraseñas no coinciden")

        return password2

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')

        if len(password1) < 8:
            raise forms.ValidationError(
                "La contraseña debe tener un mínimo de 8 caracteres.")

        return password1

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        user.username = self.cleaned_data['email']

        if commit:
            user.save()

        send_user_mail(user, 'register_user')

        return user


class EditUserForm(forms.ModelForm):
    """Formulario de edición para un usuario en la BD
    """
    facebook_url = forms.CharField(label="Facebook url", required=False)
    linkedin_url = forms.CharField(label="Linkedin url", required=False)

    class Meta:
        model = UserCustom
        fields = ('names', 'lastnames', 'email', 'avatar_image', 'prefered_language',
                  'speak_language',  'country', 'department',
                  'city', 'number_phone', 'birt_year', 'gender', 'facebook_url', 'linkedin_url')

    def clean_facebook_url(self):

        facebook_url = self.cleaned_data.get('facebook_url')

        if not 'https://www.facebook' in facebook_url:
            raise forms.ValidationError(
                "Verifique que sea una url de facebook")

        return facebook_url

    def clean_linkedin_url(self):

        linkedin_url = self.cleaned_data.get('linkedin_url')

        if linkedin_url:

            if not 'https://www.linkedin' in linkedin_url:
                raise forms.ValidationError(
                    "Verifique que sea una url de facebook")

        return linkedin_url

    def save(self, commit=True):
        user = super().save(commit=False)
        user.facebook_url = self.cleaned_data['facebook_url']
        user.linkedin_url = self.cleaned_data['linkedin_url']

        if commit:
            user.save()
            send_user_mail(user, 'edit_user')

        return user


class ConfirmMailForm(forms.ModelForm):
    """Formulario de edición para un usuario en la BD
    """

    class Meta:
        model = UserCustom
        fields = ('confirm_email', 'email')
    
    def save(self, commit=True):
        user = super().save(commit=False)

        if commit:
            user.save()
            # send_user_mail(user, 'edit_user')

        return user

DAYS = (
    ('lunes', 'Lunes'),
    ('martes', 'Martes'),
    ('miercoles', 'Miercoles'),
    ('jueves', 'Jueves'),
    ('viernes', 'Viernes'),
    ('sabado', 'Sábado'),
    ('domingo', 'Domingo'),)

YEAR = (
    ('1900')
)


current_time = datetime.datetime.now()
current_year = current_time.year


class CompleteDataOfStepsUserMentor(forms.ModelForm):
    birt_year = forms.ChoiceField(
        label='Año de nacimiento',
        required=True,
        choices=[(i, i) for i in reversed(range(1900, 2000+1))],
    )

    facebook_url = forms.CharField(label="Facebook url", required=False)

    linkedin_url = forms.CharField(label="Linkedin url", required=False)

    class Meta:
        model = UserCustom
        fields = (_('names'), 'lastnames', 'linkeding_image', 'avatar_image', 'prefered_language', 'speak_language', 'country', 'department', 'city',
                  'number_phone', 'birt_year', 'gender', 'facebook_url', 'linkedin_url')
        widget = {
            'names': forms.TextInput(attrs={'placeholder': 'username'}),
            'lastnames': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese su apellidos',
                }
            ),
            'email': forms.EmailInput(
                attrs={
                    'class': 'form-control',
                    'placeholder': 'Correo electrónico',
                }
            ),
            'avatar_image': forms.ImageField()
        }

    def save(self, commit=True):
        user = super().save(commit=False)

        if commit:
            user.save()

        return user


class CompleteDataOfStepsUser(forms.ModelForm):
    birt_year = forms.ChoiceField(
        label='Año de nacimiento',
        required=True,
        choices=[(i, i) for i in reversed(range(1900, 2000+1))],
    )

    facebook_url = forms.CharField(
        label='Facebook url',
        required=False
    )

    linkedin_url = forms.CharField(
        label='Linkedin url',
        required=False
    )

    class Meta:
        model = UserCustom
        fields = ('prefered_language', 'speak_language', 'country', 'department', 'city',
                  'number_phone', 'birt_year', 'gender', 'facebook_url', 'linkedin_url')

    def save(self, commit=True):
        user = super().save(commit=False)

        if commit:
            user.save()

        return user


class ValidateSession(forms.ModelForm):
    class Meta:
        model = UserCustom
        fields = ('recover_password',)

    def save(self, commit=True):
        user = super().save(commit=False)

        if commit:
            user.save()

        return user
