from django.urls import path, include, re_path
from django.contrib.auth.views import PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, PasswordResetCompleteView

from . import views
from rol_users.views import UpdateUserEmprendedor, UpdateUserMentor

from suggestion.views import CreteSuggestionView

urlpatterns = [
    # My urls
    path('registrar-usuario',
         views.RegistroUsuario.as_view(), name='register'),

    path('editar-usuario/<pk>',
        views.UpdateUser.as_view(), name="edit"),

    path('confirmar-email/<pk>',
        views.ConfirmMailView.as_view(), name="confirm_mail"),

    path('editar-datos-emprendedor/<pk>',
         UpdateUserEmprendedor.as_view(), name="edit_data_emprendedor"),

    path('editar-datos-mentor/<pk>',
         UpdateUserMentor.as_view(), name="edit_data_mentor"),

    path('completar-datos-mentor/<pk>',
         views.CompleteDataMentor.as_view(), name="complete_data_mentor"),

    path('completar-datos-mentor-tres/',
         views.CompleteDataTwoMentor.as_view(), name="complete_data_two_mentor"),

    path('completar-datos-emprendedor/<pk>',
         views.CompleteDataEmprendedor.as_view(), name="complete_data_emprendedor"),

    path('completar-datos-emprendedor-tres/',
         views.CompleteDataTwoEmprendedor.as_view(), name="complete_data_two_emprendedor"),

    # Perfil para editar información personal
    path('perfil/', views.user_profile, name='profile'),

    # Perfil para que el emprendedor vea el video de bienvenida
    path('perfil-bienvenida/<pk>',
         views.ValidateFirstSession.as_view(), name='profile_welcome'),

    path('perfil-publico/<int:user_id>',
         views.UserProfilePublic.as_view(), name='profile_public'),

    path('crear-cita/',
         views.UserProfilePublicForm.as_view(), name='create_cita'),

    path('enviar-sugerencia/',
         CreteSuggestionView.as_view(), name='create_suggestion'),

    path('editar-cita/<pk>',
        views.UpdateCita.as_view(), name="edit_cita"),

    path('confirmar-cita/<pk>',
        views.UpdateCitaAjax.as_view(), name="edit_cita_ajax"),

    path('cancelar-cita/<pk>',
        views.UpdateCitaAjaxToRemove.as_view(), name="edit_cita_ajax_to_remove"),

    path('terminar-cita/<pk>',
        views.UpdateCitaAjaxToFinish.as_view(), name="edit_cita_ajax_to_finish"),

    path('actualizar-sugerencia/<pk>',
        views.UpdateSuggestionAjax.as_view(), name="edit_suggestion_ajax"),

    path('descartar-sugerencia/<pk>',
        views.UpdateSuggestionAjaxCancel.as_view(), name="edit_suggestion_ajax_descartar"),

    # Login general
    path('iniciar-sesion', views.login_page, name='login'),

    # Login cuando viene de registrar el usuario
    path('iniciar-sesion-segundo-paso',
         views.login_page_step, name='login_steps'),

    path('logout/', views.logout_user, name="logout"),

    path('encuentra-un-mentor/', views.list_of_mentors, name="encuentra_mentor"),

    path('buscar-mentor/', views.search_mentor, name="search_mentor"),

    path('buscar-emprendedor/', views.search_emprendedor, name="search_emprendedor"),

    path('notificaciones/', views.notificaciones, name="notifications"),

    path('social-auth/', include('social_django.urls', namespace="social")),

    path('reset/password_reset',
         PasswordResetView.as_view(
             template_name='pages/user/recover_password/password_reset_forms.html',
             email_template_name="pages/user/recover_password/password_reset_email.html"),
         name='password_reset'
         ),

    path('reset/password_reset_done',
         PasswordResetDoneView.as_view(
             template_name='pages/user/recover_password/password_reset_done.html'
         ),
         name='password_reset_done'
         ),

    path('reset/password_reset_done', PasswordResetDoneView.as_view(
        template_name='pages/user/recover_password/password_reset_done.html'), name='password_reset_done'),

    re_path(r'^reset/(?P<uidb64>[0-9A-za-z_\-]+)/(?P<token>.+)/$', PasswordResetConfirmView.as_view(
        template_name='pages/user/recover_password/password_reset_confirm.html'), name='password_reset_confirm'),

    path('reset/done',
         PasswordResetCompleteView.as_view(
             template_name='pages/user/recover_password/password_reset_complete.html'
         ),
         name='password_reset_complete'
         ),
]
