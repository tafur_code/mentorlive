from django.apps import AppConfig


class UserConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'user'
    verbose_name = 'usuario'
    verbose_name_plural = 'usuarios'

class EmprendedorConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'emprendedor'
    verbose_name = 'emprendedor'
    verbose_name_plural = 'emprendedores'
