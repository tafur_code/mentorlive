from email.policy import default
import requests
import datetime

from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from django.conf import settings
from home.functions import send_user_mail
from home.apis import ConnectApi

# Create your models here.


class UserManager(BaseUserManager):
    def create_user(self, email, username, password=None, user_rol=None):

        if not 'admin' in username:
            username = email
            user_rol = 1

        if not email:
            raise ValueError('El usuario debe tener un correo electronico')

        user = self.model(
            username=username,
            email=self.normalize_email(email),
            user_rol=user_rol
        )

        send_user_mail(user, 'mentor_register')

        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, username, email, password):
        user = self.create_user(
            email,
            username=username,
            password=password,
        )

        user.user_staff = True
        user.save()

        return user


GENDER = (
    ('', _('Seleccione un genero')),
    ('Masculino', _('Masculino')),
    ('Femenino', _('Femenino'))
)

YEAR = (
    ('', _('Seleccione un año')),
    ('1900', '1900')
)

PREFERD_IDIOMA = (
    ('Español', _('Español')),
    ('Ingles', _('Ingles'))
)
# http://xml.coverpages.org/country3166.html
# var myDate = new Date();
# var year = myDate.getFullYear();
# for(var i = 1900; i < year+1; i++){
# document.write('<option value="'+i+'">'+i+'</option>');
# }

url_api_paises = 'https://restcountries.com/v2/all'
data_save = requests.get(url_api_paises)
rta_json = data_save.json()


class UserCustom(AbstractBaseUser):
    username = models.CharField(
        'Nombre de usuario', unique=True, max_length=100, blank=True, null=True)
    names = models.CharField('Nombres', max_length=200)
    lastnames = models.CharField(
        'Apellidos', max_length=200)
    email = models.EmailField('Correo electrónico',
                              unique=True, max_length=254)
    avatar_image = models.ImageField(
        null=True, verbose_name='Foto de perfil', upload_to='avatar_user', default="null")
    prefered_language = models.CharField(
        'Idioma de preferencia',
        max_length=500,
        choices=PREFERD_IDIOMA,
        default=" "
    )
    speak_language = models.CharField(
        'Idiomas hablados', max_length=2000, null=True)
    country = models.CharField(
        'País',
        max_length=500,
        choices=[(data_paises['name'], data_paises['name'])
                 for data_paises in rta_json],
        null=True
    )
    department = models.CharField(
        'Estado / Departamento', max_length=500, null=True)
    city = models.CharField(
        'Ciudad', max_length=500, null=True)
    number_phone = models.BigIntegerField(
        'Número telefónico', null=True
    )
    birt_year = models.CharField(
        'Año de nacimiento',
        max_length=500,
        # choices=[(i, i) for i in range(1900, current_year+1)],
        # choices=YEAR,
        null=True
    )
    gender = models.CharField(
        'Genero',
        max_length=100,
        choices=GENDER,
        null=True
    )
    facebook_url = models.CharField(
        max_length=500, default="")
    linkedin_url = models.CharField(
        max_length=500,  default="")

    is_active = models.BooleanField(default=True)
    user_staff = models.BooleanField(default=False)
    user_rol = models.CharField(
        max_length=2, blank=True, null=True, default=" "
    )
    recover_password = models.CharField(
        max_length=500, blank=True, null=True, default="")

    linkeding_image = models.CharField(
        'Imagen linkedin',
        max_length=500, blank=True, null=True)
    
    confirm_email = models.CharField(
        max_length=10, blank=True, null=True, default="0")

    object = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = "usuario"

    def __str__(self):
        return f'{self.email}'

    def get_absolute_url(self):
        return reverse("profile")

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.user_staff
