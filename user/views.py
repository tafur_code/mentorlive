import imp
from user.models import rta_json
from django import forms
import django_filters
from django.forms import ChoiceField, MultipleChoiceField
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse
from collections import Counter
from django.utils import translation
import re
import csv
import datetime

from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.views.generic import CreateView, UpdateView, ListView
from django.contrib.auth import authenticate, login, logout
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required
import requests
from social_django.models import UserSocialAuth
from django.forms.utils import ValidationError
from django.utils.translation import gettext_lazy as _
# Fuente: https://www.iteramos.com/pregunta/2802/como-puedo-hacer-un-filtrado-not-equal-en-django-
from django.db.models import Q

from user.models import UserCustom
from .forms import UserForm, ValidateSession, CompleteDataOfStepsUser, CompleteDataOfStepsUserMentor, EditUserForm, ConfirmMailForm
from rol_users.models import Emprendedor, Mentor
from rol_users.forms import CompleteDataTwoOfStepsUser, CompleteDataTwoOfStepsUserMentor
from home.functions import validate_user_authenticate, validate_user_authenticate_emprendedor, validate_title_of_page_profile
# from appointments.models import ScheduleAppointment
from mentors.models import mentorsCan, stepsMentoria
from questions.models import QuickQuestions, FrecuentQuestions
from home.apis import ConnectApi
from rol_users.models import Emprendedor, Mentor
from cita.models import Cita
from cita.forms import CreateCita, EditCita, EditCitaConfirmar, EditCitaRemove, EditCitaFinish
from rating.models import RatingUser
from suggestion.models import Suggestion
from suggestion.forms import EditSuggestion

# Create your views here.


def user_profile(request):
    # if request.user:
    user = request.user
    form = EditUserForm
    data_mentor = []
    data_emprendedor = []

    # Valida si el usuario viene del login con linkedin o del registro de usuario normal
    if isinstance(request.user, UserCustom):

        # Valida si hay una sesión y si hay un nombre rgistrado
        if validate_user_authenticate(request.user) and not request.user.prefered_language == None:

            if not user.user_rol == '1':

                if user.confirm_email == "0":
                    return redirect('confirm_mail',  pk=request.user.id)
                else:
                    data_emprendedor = Emprendedor.objects.filter(
                        id_emprendedor=user.id).first()

                    if isinstance(data_emprendedor, Emprendedor):

                        return render(request, "pages/user/profile.html", {
                            "title": f"Perfil { user.names }",
                            'form': form,
                            'data_mentor': data_mentor,
                            'data_emprendedor': data_emprendedor,
                        })

                    else:
                        return redirect('complete_data_emprendedor',  pk=request.user.id)

            else:

                data_mentor = Mentor.objects.filter(id_mentor=user.id).first()

                if isinstance(data_mentor, Mentor):
                    return render(request, "pages/user/profile.html", {
                        "title": f"Perfil { user.names }",
                        'form': form,
                        'data_mentor': data_mentor,
                        'data_emprendedor': data_emprendedor,
                    })
                else:
                    return redirect('complete_data_mentor',  pk=request.user.id)

        # Redirecciona a completar los datos del mentor o emprenededor cuando viene del login con linkedin o cuanod no hay datos
        else:
            if request.user.user_rol == "1":
                return redirect('complete_data_mentor',  pk=request.user.id)
            else:
                return redirect('complete_data_emprendedor',  pk=request.user.id)

    # Redirecciona a completar los datos del emprendedor cuando viene del registro
    else:
        return redirect('login')


def login_page(request):
    if validate_user_authenticate(request.user):
        return redirect('profile')
    else:
        if request.method == 'POST':
            # recogiendo datos
            email = request.POST.get('email')
            password = request.POST.get('password')

            user = authenticate(request, username=email, password=password)

            if user is not None:

                if user.confirm_email == "0":
                    messages.warning(
                        request, _('Confirma tu correo electrónico para continuar con tu registro'))
                else:

                    login(request, user)
                    return redirect('profile')

            else:
                messages.warning(
                    request, _('Verifique sus credenciales para iniciar sesión'))

    return render(request, 'pages/user/login.html', {
        'title': 'Iniciar sesión'
    })

# Vista cuando el usuario viene del registro


def login_page_step(request):
    if validate_user_authenticate(request.user):
        return redirect('profile')
    else:
        if request.method == 'POST':
            # recogiendo datos
            email = request.POST.get('email')
            password = request.POST.get('password')

            user = authenticate(request, username=email, password=password)

            if user is not None:
                login(request, user)

                if user.recover_password == '1':
                    return redirect('profile')
                else:
                    # return redirect('profile_welcome', pk=user.id)
                    return redirect('complete_data_emprendedor', pk=user.id)

            else:
                messages.warning(
                    request, 'Verifique sus credenciales para iniciar sesión')

    return render(request, 'pages/user/login_step.html', {
        'title': 'Paso 2 de 3'
    })


def logout_user(request):
    logout(request)

    return redirect('index_vacio')


def filter_object(filter, id_filter):
    object = UserCustom.object.filter(user_rol='1')

    if id_filter == 'filter_pe':
        object = UserCustom.object.filter(country=filter)
    elif id_filter == 'filter_me':
        object = UserCustom.object.filter(country=filter)
    elif id_filter == 'filter_co':
        object = UserCustom.object.filter(country=filter)

    return object


def list_of_mentors(request):
    list_mentors_can = mentorsCan.objects.all()
    steps_mentoria = stepsMentoria.objects.all()
    quick_questions = QuickQuestions.objects.all()
    frecuent_questions = FrecuentQuestions.objects.all()

    return render(request, 'pages/mentors/encuentra_mentor.html', {
        'title': 'Encuentra mentor',
        'list_mentors': list_of_mentors,
        'list_mentors_can': list_mentors_can,
        'steps_mentoria': steps_mentoria,
        'quick_questions': quick_questions,
        'frecuent_questions': frecuent_questions,
    })


AREAS = (
    ('', _("Seleccione una opción")),
    ('Contabilidad', _("Contabilidad")),
    ('Auditorias', _("Auditorias")),
    ('Ingresos / Egresos', _("Ingresos / Egresos")),
    ('Presupuesto', _("Presupuesto")),
    ('Flujo de Efectivo', _("Flujo de Efectivo")),
    ('Planeación Financiera', _("Planeación Financiera")),
    ('Préstamos y Financiamiento', _("Préstamos y Financiamiento")),
    ('Otro tipo de Contabilidad', _("Otro tipo de Contabilidad")),
    ('Impuestos', _("Impuestos")),
    ('Compensaciones y Prestaciones', _("Compensaciones y Prestaciones")),
    ('Contratistas y Consultores', _("Contratistas y Consultores")),
    ('Administración de personal', _("Administración de personal")),
    ('Capacitación de personal', _("Capacitación de personal")),
    ('Otra forma de recursos humanos', _("Otra forma de recursos humanos")),
    ('Políticas del personal', _("Políticas del personal")),
    ('Reclutamiento y Contratación', _("Reclutamiento y Contratación")),
    ('Gestión de voluntarios', _("Gestión de voluntarios")),
    ('Aduanas y Aranceles', _("Aduanas y Aranceles")),
    ('Exportación e Importación', _("Exportación e Importación")),
    ('Mercados globales', _("Mercados globales")),
    ('Otro sector internacional', _("Otro sector internacional")),
    ('Outsourcing', _("Outsourcing")),
    ('Contratos', _("Contratos")),
    ('Derecho laboral', _("Derecho laboral")),
    ('Propiedad intelectual', _("Propiedad intelectual")),
    ('Otra área legal', _("Otra área legal")),
    ('Derecho de la propiedad', _("Derecho de la propiedad")),
    ('Derecho fiscal', _("Derecho fiscal")),
    ('Establecimiento de consejos directivos', _(
        "Establecimiento de consejos directivos")),
    ('Seguros comerciales', _("Seguros comerciales")),
    ('Estrategias de negocios', _("Estrategias de negocios")),
    ('Reacudación de fondos', _("Reacudación de fondos")),
    ('Crecimiento y Desarrollo Profesional', _(
        "Crecimiento y Desarrollo Profesional")),
    ('Liderazgo', _("Liderazgo")),
    ('Otras áreas administrativas', _("Otras áreas administrativas")),
    ('Planeamiento y Programación por metas', _(
        "Planeamiento y Programación por metas")),
    ('Administración de proyectos', _("Administración de proyectos")),
    ('Equilibrio trabajo - vida personal', _("Equilibrio trabajo - vida personal")),
    ('Publicidad y Promoción', _("Publicidad y Promoción")),
    ('Branding e Identidad Corporativa', _("Branding e Identidad Corporativa")),
    ('Desarrollo de negocios', _("Desarrollo de negocios")),
    ('Distribución', _("Distribución")),
    ('Investigación de mercados', _("Investigación de mercados")),
    ('Materiales de mercadotecnia', _("Materiales de mercadotecnia")),
    ('Estrategias de mercadotecnia', _("Estrategias de mercadotecnia")),
    ('Otra forma de mercadotecnia', _("Otra forma de mercadotecnia")),
    ('Fijación de precios', _("Fijación de precios")),
    ('Desarrollo de productos', _("Desarrollo de productos")),
    ('Relaciones públicas y Medios', _("Relaciones públicas y Medios")),
    ('Redes sociales', _("Redes sociales")),
    ('Marketing en línea', _("Marketing en línea")),
    ('Redacción y Edición', _("Redacción y Edición")),
    ('Manejo de inventarios', _("Manejo de inventarios")),
    ('Logística', _("Logística")),
    ('Manufactura', _("Manufactura")),
    ('Otras operaciones', _("Otras operaciones")),
    ('Embalaje y etiquetado', _("Embalaje y etiquetado")),
    ('Mejora de procesos y optimización', _("Mejora de procesos y optimización")),
    ('Adquisiciones y Proveedores', _("Adquisiciones y Proveedores")),
    ('Diseño de programas y evaluación', _("Diseño de programas y evaluación")),
    ('Gestión de calidad', _("Gestión de calidad")),
    ('Transporte y Entrega', _("Transporte y Entrega")),
    ('Servicio / Atención al cliente y Gestion de relaciones comerciales',
     _("Servicio / Atención al cliente y Gestion de relaciones comerciales")),
    ('Contratos gubernamentales', _("Contratos gubernamentales")),
    ('Generacion de contactos', _("Generacion de contactos")),
    ('Otras áreas de ventas', _("Otras áreas de ventas")),
    ('Venta al público', _("Venta al público")),
    ('Venta de servicios', _("Venta de servicios")),
    ('Venta al por mayor B2B', _("Venta al por mayor B2B")),
    ('Planificación empresarial', _("Planificación empresarial")),
    ('Franquicias', _("Franquicias")),
    ('Cómo comenzar', _("Cómo comenzar")),
    ('Estructura legal', _("Estructura legal")),
    ('Localización y Zonificación', _("Localización y Zonificación")),
    ('Otras áreas de Planificación empresarial', _(
        "Otras áreas de Planificación empresarial")),
    ('Otras áreas de Planificación empresarial76', _(
        "Otras áreas de Planificación empresarial")),
    ('Eficiencia estratégica', _("Eficiencia estratégica")),
    ('Negocios sustentables', _("Negocios sustentables")),
    ('Productos ecológicos', _("Productos ecológicos")),
    ('Otras áreas de sustentabilidad', _("Otras áreas de sustentabilidad")),
    ('Comercio electrónico', _("Comercio electrónico")),
    ('Gestión de tecnología', _("Gestión de tecnología")),
    ('Tecnología e Internet', _("Tecnología e Internet")),
    ('Planificación de recursos tecnológicos', _(
        "Planificación de recursos tecnológicos")),
    ('Telecomunicaciones', _("Telecomunicaciones")),
    ('Diseño de páginas web', _("Diseño de páginas web")),
    ('Otras áreas en tecnología e internet', _(
        "Otras áreas en tecnología e internet")),
)

INDUSTRIA_NEGOCIO = (
    ('', _("Seleccione una opción")),
    ('03', _("Agricultura / Granja / Rancho")),
    ('04', _("Animales / Mascotas")),
    ('05', _("Arquitectura / Diseño de interiores")),
    ('48', _("Artículos deportivos / Recreación / Actividades al aire libre")),
    ('06', _("Artesanías / Manualidades")),
    ('07', _("Artes: Música / Plásticas / Escénicas")),
    ('42', _("Asistencia personal ejecutiva")),
    ('09', _("Belleza / Cuidado del cabello / Cosméticos")),
    ('44', _("Bienes raices")),
    ('22', _("Comercio electrónico / Ventas en Línea")),
    ('49', _("Conducción de vehiculos")),
    ('11', _("Consultoría de negocios / Coaching")),
    ('14', _("Construcción / Personal de obra / Reparaciones")),
    ('15', _("Consejería / Terapia / Salud mental")),
    ('12', _("Cuidado de niños")),
    ('02', _("Cuidado de adultos mayores / Atención médica domiciliaria")),
    ('18', _("Distribución y Transporte")),
    ('29', _("Diseño gráfico / Desarrollo de sitios web")),
    ('19', _("Educación / Capacitación")),
    ('30', _("Energías renovables")),
    ('20', _("Entretenimiento / Recreación / Eventos")),
    ('21', _("Exportación / Importación")),
    ('28', _("Fabricación de muebles / Reparacion / Venta")),
    ('43', _("Fotografía / Sonido / Servicios de video")),
    ('25', _("Floristería / Regalos")),
    ('33', _("Joyería / Artículos de lujo")),
    ('34', _("Jardinería")),
    ('10', _("Librerias / Puestos de periódicos y revistas")),
    ('35', _("Lavandería / Tintorería")),
    ('37', _("Manufactura")),
    ('16', _("Marketing digital / Comercio electrónico / Redes sociales")),
    ('39', _("Medios / Editorial")),
    ('38', _("Mercadotecnia / Publicidad")),
    ('23', _("Moda / Boda / Accesorios")),
    ('41', _("Multimedia / Redes sociales")),
    ('26', _("Productos alimenticios / Abarrotes")),
    ('45', _("Reclutamiento / Contratación / Servicios personales")),
    ('54', _("Redacción / Edición")),
    ('46', _("Restaurante / Cafe / Bar / Banquetería")),
    ('08', _("Refracciones / Mecánico")),
    ('24', _("Servicios Financieros / Seguros")),
    ('27', _("Silvicultura / Productos de madera")),
    ('17', _("Servicios para personas con discapacidad")),
    ('13', _("Servicios informáticos / Tecnología de la información")),
    ('01', _("Servicios contables y fiscales")),
    ('31', _("Salud / Bienestar / Actividad física / Ejercicio")),
    ('32', _("Servicios de limpieza comerciales y domésticos")),
    ('36', _("Servicios legales")),
    ('40', _("Sin fines de Lucro / Empresa social")),
    ('50', _("Traducción / Guía")),
    ('47', _("Venta al público")),
    ('52', _("Veterinaria")),
    ('51', _("Viajes / Turismo / Hotelería")),
    ('53', _("Vinos / Licores")),
)

TIPO_NEGOCIO = (
    ('', _("Seleccione una opción")),
    ('lucro', _("De lucro")),
    ('sinlucro', _("Sin fines de lucro")),
    ('empresa_social', _("Empresa social")),
    ('no_se', _("No estoy seguro")),
)


class ProductFilter(django_filters.FilterSet):
    industria_negocio = django_filters.CharFilter(
        label='ip', lookup_expr='icontains')

    ayudas_para_negocio = django_filters.CharFilter(
        label='s',
        lookup_expr='icontains'
    )

    ubicacion_negocio = django_filters.CharFilter(
        lookup_expr='icontains'
    )

    class Meta:
        model = Mentor
        fields = ['ayudas_para_negocio', 'años_experiencia_dueño',
                  'ubicacion_negocio', 'industria_negocio']


class FiltersEmprendedor(django_filters.FilterSet):
    industria_negocio = django_filters.CharFilter(
        label='ip', lookup_expr='icontains')

    ayudas_para_negocio = django_filters.CharFilter(
        label='s',
        lookup_expr='icontains'
    )

    ubicacion_negocio = django_filters.CharFilter(
        lookup_expr='icontains'
    )

    tipo_negocio_en_mente = django_filters.CharFilter(
        lookup_expr='icontains'
    )

    class Meta:
        model = Mentor
        fields = ['ayudas_para_negocio', 'tipo_negocio_en_mente',
                  'ubicacion_negocio', 'industria_negocio']


def search_mentor(request):
    card_mentor = []
    data_card_mentor = {}
    list_ayudas = []
    data_paises_list = []

    filter_result = ProductFilter(request.GET, queryset=Mentor.objects.all())

    for mentors in filter_result.qs:
        users = UserSocialAuth.objects.filter(user=mentors.id_mentor)

        if users:

            string = mentors.ayudas_para_negocio
            string = re.sub("\[|\'|\]", "", string)
            list_ayudas = string.split(sep=',')

            for user in users:

                image_avatar = None

                if not user.user.user_rol == '1':
                    image_avatar = user.user.avatar_image.url
                else:

                    if user.user.avatar_image != "null":
                        image_avatar = user.user.avatar_image.url
                    else:
                        image_avatar = user.user.linkeding_image

                diferencia_dias = 0

                if Cita.objects.filter(mentor=user, state="aceptada"):

                    citas = Cita.objects.filter(
                        mentor=user, state="aceptada").order_by('-created_at').first()

                    current_time = datetime.datetime.now()
                    a = datetime.datetime(current_time.year, current_time.month, current_time.day,
                                          current_time.hour, current_time.minute, current_time.second)

                    b = datetime.datetime(citas.created_at.year, citas.created_at.month, citas.created_at.day,
                                          citas.created_at.hour, citas.created_at.minute, citas.created_at.second)

                    c = a-b
                    minutes = divmod(c.seconds, 60)

                    if c.days <= 0:
                        if minutes[0] <= 5:
                            diferencia_dias = f"un momento"
                        elif minutes[0] <= 60:
                            diferencia_dias = f"{ minutes[0] } minutos"
                        else:
                            diferencia_dias = f"{ round(c.seconds/3600) } horas"
                    else:
                        if c.days == 1:
                            diferencia_dias = f"{ c.days } dia"
                        else:
                            diferencia_dias = f"{ c.days } dias"

                rating_user = RatingUser.objects.filter(user=user.user)

                sum_list = []
                sum_dos = []

                if rating_user:
                    sum_list.append(rating_user)

                    for sum_test in sum_list:
                        for este in sum_test:
                            sum_dos.append(este.rating_user)

                        data_card_mentor = {
                            'id': user.user.id,
                            'nombre': f"{user.user.names} {user.user.lastnames}",
                            'image_avatar': image_avatar,
                            'ocupacion': mentors.ocupacion,
                            'pais': f"{user.user.city}, { user.user.country}",
                            'ayudas': list_ayudas,
                            'diferencia_dias': diferencia_dias,
                            'rating': sum(sum_dos)
                        }

                else:

                    data_card_mentor = {
                        'id': user.user.id,
                        'nombre': f"{user.user.names} {user.user.lastnames}",
                        'image_avatar': image_avatar,
                        'ocupacion': mentors.ocupacion,
                        'pais': f"{user.user.city}, { user.user.country}",
                        'ayudas': list_ayudas,
                        'diferencia_dias': diferencia_dias,
                        'rating': 0
                    }

                card_mentor.append(data_card_mentor)

    for data_paises in rta_json:
        data_paises_list.append(data_paises['name'])

    return render(request, 'pages/mentors/search_mentor.html', {
        'title': 'Buscar Mentor',
        'filter': filter_result,
        'areas': AREAS,
        'industrias': INDUSTRIA_NEGOCIO,
        'data_paises_list': data_paises_list,
        'card_mentor': card_mentor
    })


def search_emprendedor(request):
    card_emprendedor = []
    data_card_emprendedor = {}
    list_ayudas = []
    data_paises_list = []
    citas = ""

    filter_result = FiltersEmprendedor(
        request.GET, queryset=Emprendedor.objects.all())

    for emprendedores in filter_result.qs:
        users = UserCustom.object.filter(id=emprendedores.id_emprendedor)
        if users:

            string = emprendedores.ayudas_para_negocio
            string = re.sub("\[|\'|\]", "", string)
            list_ayudas = string.split(sep=',')

            for user in users:
                image_avatar = user.avatar_image.url

                citas = Cita.objects.filter(
                    emprendedor=user, state="aceptada").order_by('-created_at').first()

                current_time = datetime.datetime.now()
                a = datetime.datetime(current_time.year, current_time.month, current_time.day,
                                      current_time.hour, current_time.minute, current_time.second)

                if(citas):
                    b = datetime.datetime(citas.created_at.year, citas.created_at.month, citas.created_at.day,
                                          citas.created_at.hour, citas.created_at.minute, citas.created_at.second)

                    c = a-b
                    minutes = divmod(c.seconds, 60)
                    diferencia_dias = ""

                    if c.days <= 0:
                        if minutes[0] <= 5:
                            diferencia_dias = f"un momento"
                        elif minutes[0] <= 60:
                            diferencia_dias = f"{ minutes[0] } minutos"
                        else:
                            diferencia_dias = f"{ round(c.seconds/3600) } horas"
                    else:
                        if c.days == 1:
                            diferencia_dias = f"{ c.days } dia"
                        else:
                            diferencia_dias = f"{ c.days } dias"

                else:
                    diferencia_dias = 0

                data_card_emprendedor = {
                    'id': user.id,
                    'nombre': f"{user.names} {user.lastnames}",
                    'image_avatar': image_avatar,
                    'ocupacion': emprendedores.ocupacion,
                    'pais': f"{user.city}, { user.country}",
                    'ayudas': list_ayudas,
                    'diferencia_dias': diferencia_dias
                }

            card_emprendedor.append(data_card_emprendedor)

    for data_paises in rta_json:
        data_paises_list.append(data_paises['name'])

    return render(request, 'pages/emprendedor/search_emprendedor.html', {
        'title': 'Buscar Mentor',
        'filter': filter_result,
        'areas': AREAS,
        'industrias': INDUSTRIA_NEGOCIO,
        'tipo_negocio': TIPO_NEGOCIO,
        'data_paises_list': data_paises_list,
        'card_emprendedor': card_emprendedor
    })


class RegistroUsuario(SuccessMessageMixin, CreateView):
    model = UserCustom
    form_class = UserForm
    template_name = 'pages/user/register.html'
    success_message = _(
        "Bienvenido a Altruism Now Mentors, antes de continuar con el registro, por favor confirma tu correo electronico.")

    def get(self, request):

        if(validate_user_authenticate(request.user)):
            return redirect('profile')
        else:
            form = self.form_class

            return render(request, self.template_name, {'title': 'Registro usuario', 'form': form})


class UserProfilePublic(SuccessMessageMixin, ListView):
    template_name = 'pages/user/profile_public.html'
    success_message = "Se a enviado la solicitud para su encuentro"
    list_days_emprendedor = None
    list_ayudas_emprendedor = None
    list_days_mentor = None
    list_ayudas_mentor = None
    list_dias_mentor = None
    data_mentor = []
    data_emprendedor = []
    all_emprendedores = UserCustom.object.filter(
        ~Q(user_rol='1')).filter(~Q(user_rol='ad'))

    def get_data_of_rol(self, user):
        if user.is_authenticated:
            if not user.user_rol == '1':
                self.data_emprendedor = Emprendedor.objects.filter(
                    id_emprendedor=user.id).first()
                return ""

            else:
                self.data_mentor = Mentor.objects.filter(
                    id_mentor=user.id).first()

    def get_pais_of_rols(self, user, data_mentor_days, data_emprendedor_days):
        if user.user_rol == '1':
            if data_mentor_days.ubicacion_negocio:
                string = data_mentor_days.ubicacion_negocio
                string = re.sub("\[|\'|\]", "", string)
                self.list_days_mentor = string.split(sep=',')

                string_dos = data_mentor_days.ayudas_para_negocio
                string_dos = re.sub("\[|\'|\]", "", string_dos)
                self.list_ayudas_mentor = string_dos.split(sep=',')

                string_tres = data_mentor_days.dias_disponibilidad
                string_tres = re.sub("\[|\'|\]", "", string_tres)
                self.list_dias_mentor = string_tres.split(sep=',')

        else:

            if data_emprendedor_days.ubicacion_negocio:
                string = data_emprendedor_days.ubicacion_negocio
                string = re.sub("\[|\'|\]", "", string)
                self.list_days_emprendedor = string.split(sep=',')

                string_dos = data_emprendedor_days.ayudas_para_negocio
                string_dos = re.sub("\[|\'|\]", "", string_dos)
                self.list_ayudas_emprendedor = string_dos.split(sep=',')

    def get_data_of_user(self, emprendedor):

        user_id = UserCustom.object.filter(id=emprendedor.id).first()
        user_none = False
        is_data_user_null = False

        if not isinstance(user_id, UserCustom):
            user_none = True

        elif len(user_id.prefered_language) <= 1:
            is_data_user_null = True

        return is_data_user_null, user_none

    def get_data_of_emprendedor(self, emprendedor):
        emprendedor_id = Emprendedor.objects.filter(
            id_emprendedor=emprendedor.id).first()
        user_none = False

        user_id = UserCustom.object.filter(id=emprendedor.id).first()

        is_data_emprendedor_null = False

        if not isinstance(user_id, UserCustom):
            user_none = True

        elif len(user_id.prefered_language) > 1 and not isinstance(emprendedor_id, Emprendedor):
            is_data_emprendedor_null = True

        return is_data_emprendedor_null, user_none

    def get(self, request, user_id):
        user = get_object_or_404(UserCustom, id=user_id)
        id_user_social = ""
        num_id = 0

        if user.user_rol == '1':
            id_user_social = UserSocialAuth.objects.filter(
                user=user).first()
            num_id = id_user_social.id
        else:
            if not request.user.id == user_id:
                id_user_social = UserCustom.object.filter(
                    id=request.user.id).first()

                id_social_auth = UserSocialAuth.objects.filter(
                    user=id_user_social).first()

                if id_user_social:
                    num_id = id_social_auth.id
                else:
                    num_id = 0
            else:
                num_id = 0

        data_emprendedor_days = Emprendedor.objects.filter(
            id_emprendedor=user.id).first()
        data_mentor_days = Mentor.objects.filter(id_mentor=user.id).first()
        user_social = ""
        total_citas = ""
        citas_aceptadas = ""
        total_citas_aceptadas = ""
        ultimo_encuentro = ""

        if user.user_rol == '1':
            user_social = UserSocialAuth.objects.filter(user=user)
            total_citas = Cita.objects.filter(
                state='aceptada', mentor__in=user_social).count()
            citas_aceptadas = Cita.objects.filter(
                state='aceptada_emprendedor', mentor__in=user_social).count()
            ultimo_encuentro = Cita.objects.filter(
                state='aceptada', mentor__in=user_social).order_by("-created_at").first()

            total_citas_aceptadas = citas_aceptadas + total_citas

        else:
            total_citas = Cita.objects.filter(
                state='aceptada', emprendedor=user).count()
            citas_aceptadas = Cita.objects.filter(
                state='aceptada_emprendedor', emprendedor=user).count()
            ultimo_encuentro = Cita.objects.filter(
                state='aceptada', emprendedor=user).order_by("-created_at").first()

            total_citas_aceptadas = citas_aceptadas + total_citas

        current_time = datetime.datetime.now()

        a = datetime.datetime(current_time.year, current_time.month, current_time.day,
                              current_time.hour, current_time.minute, current_time.second)

        if(ultimo_encuentro):
            b = datetime.datetime(ultimo_encuentro.created_at.year, ultimo_encuentro.created_at.month, ultimo_encuentro.created_at.day,
                                  ultimo_encuentro.created_at.hour, ultimo_encuentro.created_at.minute, ultimo_encuentro.created_at.second)

            c = a-b
            minutes = divmod(c.seconds, 60)
            diferencia_dias = ""

            if c.days <= 0:
                if minutes[0] <= 5:
                    diferencia_dias = f"un momento"
                elif minutes[0] <= 60:
                    diferencia_dias = f"{ minutes[0] } minutos"
                else:
                    diferencia_dias = f"{ round(c.seconds/3600) } horas"
            else:
                if c.days == 1:
                    diferencia_dias = f"{ c.days } dia"
                else:
                    diferencia_dias = f"{ c.days } dias"
        else:
            diferencia_dias = 0

        self.get_pais_of_rols(user, data_mentor_days, data_emprendedor_days)
        self.get_data_of_rol(user)

        list_country_emprendedor = []
        for country_emprendedor in self.all_emprendedores:

            if not country_emprendedor.country == None:
                if not country_emprendedor.country in list_country_emprendedor:
                    list_country_emprendedor.append(
                        country_emprendedor.country)

        # Rating of users
        # if request.user.user_rol == '1':
        rating_user = RatingUser.objects.filter(user=user)
        # else:
        # rating_user = RatingUser.objects.filter(user=user)

        return render(
            request,
            self.template_name,
            {
                "title": f"Perfil público { validate_title_of_page_profile(user) }",
                'user_public': user,
                'list_days_emprendedor': self.list_days_emprendedor,
                'list_ayudas_emprendedor': self.list_ayudas_emprendedor,
                'list_days_mentor': self.list_days_mentor,
                'list_ayudas_mentor': self.list_ayudas_mentor,
                'list_dias_mentor': self.list_dias_mentor,
                'data_mentor': self.data_mentor,
                'data_emprendedor': self.data_emprendedor,
                'list_country_emprendedor': list_country_emprendedor,
                'total_mentorias': total_citas_aceptadas,
                "diferencia_dias": diferencia_dias,
                'id_user_mentor': num_id,
                'rating_user': rating_user,
                'class_rating': def_rating_users(rating_user)[0],
                'rating_result': def_rating_users(rating_user)[1],
                "data_user_null": self.get_data_of_user(request.user),
                "data_emprendedor_null": self.get_data_of_emprendedor(request.user)
            }
        )

    def get_success_url(self):
        return reverse('profile')


class UserProfilePublicForm(SuccessMessageMixin, CreateView):
    model = Cita
    form_class = CreateCita
    template_name = 'pages/user/profile_public_create.html'
    success_message = _(
        "¡Bien Hecho! Hemos enviado tu solicitud, muy pronto recibirás contacto del Mentor para empezar a trabajar en ese negocio.")
    all_emprendedores = UserCustom.object.filter(
        ~Q(user_rol='1')).filter(~Q(user_rol='ad'))
    msg_captcha = ""

    def get(self, request):
        user = None
        id_mentor = None
        id_mentor_of_emprendedor = None
        id_emprendedor = None
        email = None
        email_emprendedor = None
        data_mentor_id = None
        user_mentor = None
        is_emprendedor = False

        if request.GET.get('id_mentor') and not request.GET.get('id_emprendedor'):
            id_mentor = request.GET.get('id_mentor')
            email = request.GET.get('email')
            data_mentor_id = Mentor.objects.filter(id_mentor=id_mentor).first()
            user_mentor = UserSocialAuth.objects.filter(user=id_mentor).first()
            user = UserCustom.object.filter(id=id_mentor).first()

        elif request.GET.get('id_emprendedor'):
            id_mentor_of_emprendedor = request.GET.get('id_mentor')
            id_emprendedor = request.GET.get('id_emprendedor')
            email_emprendedor = request.GET.get('email')
            email = request.GET.get('email_mentor')
            is_emprendedor = True
            data_mentor_id = Emprendedor.objects.filter(
                id_emprendedor=id_emprendedor).first()
            user_mentor = UserSocialAuth.objects.filter(
                user=id_mentor_of_emprendedor).first()
            user = UserCustom.object.filter(id=id_emprendedor).first()

        list_country_emprendedor = []
        for country_emprendedor in self.all_emprendedores:

            if not country_emprendedor.country == None:
                if not country_emprendedor.country in list_country_emprendedor:
                    list_country_emprendedor.append(
                        country_emprendedor.country)

        if request.GET.get('captcha_none') == 'true':
            self.msg_captcha = "Para continuar valide el Captcha"

        return render(
            request,
            self.template_name,
            {
                'title': 'Agendar una cita',
                'form': self.form_class,
                'data_mentor_id': data_mentor_id,
                'user_mentor': user_mentor,
                'email_mentor': email,
                'list_country_emprendedor': list_country_emprendedor,
                'msg_captcha': self.msg_captcha,
                'is_emprendedor': is_emprendedor,
                'id_emprendedor': id_emprendedor,
                'email_emprendedor': email_emprendedor,
                'user_public': user,
            }
        )

    def form_valid(self, form):
        # Este método se llama cuando se han publicado datos de formulario válidos.
        if self.request.POST['g-recaptcha-response']:
            # form.send_email()
            return super().form_valid(form)
        else:
            new_uri = f"{self.request.build_absolute_uri()}&captcha_none=true"
            return redirect(new_uri)

    def get_success_url(self):
        return reverse('profile')


class UpdateUser(SuccessMessageMixin, UpdateView):
    model = UserCustom
    form_class = EditUserForm
    template_name = 'pages/user/edit_user.html'
    success_message = "Se editaron los campos satisfactoriamente"
    data_emprendedor = None
    data_mentor = None
    extra_context = {'title': 'Editar usuario'}


def def_rating_users(rating_user):
    sum_rating = []

    for rating_user_number in rating_user:
        sum_rating.append(rating_user_number.rating_user)

    class_rating = ""
    resultado = 0

    if len(rating_user) != 0:
        resultado = sum(sum_rating) / len(sum_rating)

        if resultado <= 1.5:
            class_rating = f"is_rating_number_1"
        elif resultado > 1.5 and resultado <= 2.0:
            class_rating = f"is_rating_number_2"
        elif resultado > 2.0 and resultado <= 3.5:
            class_rating = f"is_rating_number_3"
        elif resultado > 3.5 and resultado <= 4.5:
            class_rating = f"is_rating_number_4"
        elif resultado > 4.5 and resultado <= 5.0:
            class_rating = f"is_rating_number_5"
    else:
        class_rating = f"is_rating_number_0"

    format_result = 0

    if resultado > 0:
        format_result = '{:.2}'.format(resultado)

    result = [class_rating, format_result]

    return result


def notificaciones(request):
    count_pendientes = 0
    count_aceptadas = 0
    count_aceptadas_emprendedor = 0
    count_solicitudes_pendientes = 0
    count_rechazadas = 0
    total_aceptadas = 0
    sugerencias_mentorias = ""

    if request.user.user_rol == '1':
        user = UserSocialAuth.objects.filter(user=request.user).first()
        citas = Cita.objects.filter(mentor=user).order_by('-created_at')
        count_pendientes = Cita.objects.filter(
            mentor=user, state="espera").count()
        count_aceptadas = Cita.objects.filter(
            mentor=user, state="aceptada").count()
        count_aceptadas_emprendedor = Cita.objects.filter(
            mentor=user, state="aceptada_emprendedor").count()
        count_rechazadas = Cita.objects.filter(
            mentor=user, state="cancelada").count()
        count_rechazadas_emprendedor = Cita.objects.filter(
            mentor=user, state="cancelada_emprendedor").count()
        total_aceptadas = count_aceptadas + count_aceptadas_emprendedor
        total_canceladas = count_rechazadas + count_rechazadas_emprendedor
    else:
        user = UserCustom.object.filter(id=request.user.id).first()
        citas = Cita.objects.filter(emprendedor=user).order_by('-created_at')
        count_pendientes = Cita.objects.filter(
            emprendedor=user, state="espera").count()
        count_aceptadas = Cita.objects.filter(
            emprendedor=user, state="aceptada").count()
        count_aceptadas_emprendedor = Cita.objects.filter(
            emprendedor=user, state="aceptada_emprendedor").count()
        count_rechazadas = Cita.objects.filter(
            emprendedor=user, state="cancelada").count()
        count_rechazadas_emprendedor = Cita.objects.filter(
            emprendedor=user, state="cancelada_emprendedor").count()
        total_aceptadas = count_aceptadas + count_aceptadas_emprendedor
        total_canceladas = count_rechazadas + count_rechazadas_emprendedor

        sugerencias_mentorias = Suggestion.objects.filter(
            emprendedor=user).order_by('-created_at')
        count_solicitudes_pendientes = sugerencias_mentorias.filter(
            state="espera").count()

    list_notificaciones = []
    list_sugges = []
    list_notificaciones_aceptada = []
    list_notificaciones_cancelada = []
    data_list_notificaciones = {}
    data_list_suggess = {}

    current_time = datetime.datetime.now()
    a = datetime.datetime(current_time.year, current_time.month, current_time.day,
                          current_time.hour, current_time.minute, current_time.second)

    for cita in citas:
        if cita.fecha_estimada_reunion.hour <= 9:
            if cita.fecha_estimada_reunion.minute <= 9:
                hora_reunion = f"0{cita.fecha_estimada_reunion.hour}:0{cita.fecha_estimada_reunion.minute}"
            else:
                hora_reunion = f"0{cita.fecha_estimada_reunion.hour}:{cita.fecha_estimada_reunion.minute}"
        else:
            if cita.fecha_estimada_reunion.minute <= 9:
                hora_reunion = f"{cita.fecha_estimada_reunion.hour}:0{cita.fecha_estimada_reunion.minute}"
            else:
                hora_reunion = f"{cita.fecha_estimada_reunion.hour}:{cita.fecha_estimada_reunion.minute}"

        if cita.fecha_confirmada_reunion.hour <= 9:
            if cita.fecha_confirmada_reunion.minute <= 9:
                hora_reunion_confirmada = f"0{cita.fecha_confirmada_reunion.hour}:0{cita.fecha_confirmada_reunion.minute}"
            else:
                hora_reunion_confirmada = f"0{cita.fecha_confirmada_reunion.hour}:{cita.fecha_confirmada_reunion.minute}"
        else:
            if cita.fecha_confirmada_reunion.minute <= 9:
                hora_reunion_confirmada = f"{cita.fecha_confirmada_reunion.hour}:0{cita.fecha_confirmada_reunion.minute}"
            else:
                hora_reunion_confirmada = f"{cita.fecha_confirmada_reunion.hour}:{cita.fecha_confirmada_reunion.minute}"

        b = datetime.datetime(cita.created_at.year, cita.created_at.month, cita.created_at.day,
                              cita.created_at.hour, cita.created_at.minute, cita.created_at.second)

        a.replace(tzinfo=datetime.timezone.utc)
        b.replace(tzinfo=datetime.timezone.utc)

        c = a-b
        minutes = divmod(c.seconds, 60)
        diferencia_dias = ""

        if c.days <= 0:

            if minutes[0] == 0:
                if translation.get_language() == "es":
                    diferencia_dias = f"un momento"
                else:
                    diferencia_dias = f"one moment"

            elif minutes[0] <= 1:
                if translation.get_language() == "es":
                    diferencia_dias = f"{ minutes[0] } minuto"
                else:
                    diferencia_dias = f"{ minutes[0] } minute"

            elif minutes[0] > 1 and minutes[0] < 60:
                if translation.get_language() == "es":
                    diferencia_dias = f"{ minutes[0] } minutos"
                else:
                    diferencia_dias = f"{ minutes[0] } minutes"
            else:
                if translation.get_language() == "es":
                    diferencia_dias = f"{ round(c.seconds/3600) } horas"
                else:
                    diferencia_dias = f"{ round(c.seconds/3600) } hours"
        else:
            if c.days == 1:
                if translation.get_language() == "es":
                    diferencia_dias = f"{ c.days } dia"
                else:
                    diferencia_dias = f"{ c.days } day"

            else:
                if translation.get_language() == "es":
                    diferencia_dias = f"{ c.days } dias"
                else:
                    diferencia_dias = f"{ c.days } days"

        if request.user.user_rol == '1':
            rating_user = RatingUser.objects.filter(user=cita.emprendedor)
        else:
            rating_user = RatingUser.objects.filter(user=cita.mentor.user)

        data_list_notificaciones = {
            'id': cita.id,
            'emprendedor': cita.emprendedor,
            'mentor': cita.mentor,
            'diferencia_dias': diferencia_dias,
            'description': cita.description_text,
            'fecha_reunion': cita.fecha_estimada_reunion,
            'fecha_confirmada_reunion': cita.fecha_confirmada_reunion,
            'hora_reunion': hora_reunion,
            'hora_reunion_confirmada': hora_reunion_confirmada,
            'state': cita.state,
            'mensaje_aceptada': cita.description_confirmation,
            'mensaje_cancelada': cita.description_cancel,
            'slug': cita.slug,
            'rating_user': rating_user,
            'class_rating': def_rating_users(rating_user)[0],
            'rating_result': def_rating_users(rating_user)[1]
        }

        if cita.state == "espera":
            list_notificaciones.append(data_list_notificaciones)
        elif cita.state == "aceptada" or cita.state == "aceptada_emprendedor":
            list_notificaciones_aceptada.append(data_list_notificaciones)
        elif cita.state == "cancelada" or cita.state == "cancelada_emprendedor":
            list_notificaciones_cancelada.append(data_list_notificaciones)

    for sugges in sugerencias_mentorias:
        b = datetime.datetime(sugges.created_at.year, sugges.created_at.month, sugges.created_at.day,
                              sugges.created_at.hour, sugges.created_at.minute, sugges.created_at.second)

        a.replace(tzinfo=datetime.timezone.utc)
        b.replace(tzinfo=datetime.timezone.utc)

        c = a-b
        minutes = divmod(c.seconds, 60)
        diferencia_dias_sug = ""

        if c.days <= 0:

            if minutes[0] == 0:
                if translation.get_language() == "es":
                    diferencia_dias_sug = f"un momento"
                else:
                    diferencia_dias_sug = f"one moment"

            elif minutes[0] <= 1:
                if translation.get_language() == "es":
                    diferencia_dias_sug = f"{ minutes[0] } minuto"
                else:
                    diferencia_dias_sug = f"{ minutes[0] } minute"

            elif minutes[0] > 1 and minutes[0] < 60:
                if translation.get_language() == "es":
                    diferencia_dias_sug = f"{ minutes[0] } minutos"
                else:
                    diferencia_dias_sug = f"{ minutes[0] } minutes"
            else:
                if translation.get_language() == "es":
                    diferencia_dias_sug = f"{ round(c.seconds/3600) } horas"
                else:
                    diferencia_dias_sug = f"{ round(c.seconds/3600) } hours"
        else:
            if c.days == 1:
                if translation.get_language() == "es":
                    diferencia_dias_sug = f"{ c.days } dia"
                else:
                    diferencia_dias_sug = f"{ c.days } day"

            else:
                if translation.get_language() == "es":
                    diferencia_dias_sug = f"{ c.days } dias"
                else:
                    diferencia_dias_sug = f"{ c.days } days"

        data_list_suggess = {
            'id': sugges.id,
            'emprendedor': sugges.emprendedor,
            'mentor': sugges.mentor,
            'description': sugges.description_text,
            'diferencia_dias': diferencia_dias_sug,
            'state': sugges.state
        }

        list_sugges.append(data_list_suggess)

    return render(request, 'pages/user/notifications.html', {
        'title': 'Notificaciones',
        'list_notificaciones': list_notificaciones,
        'list_notificaciones_aceptada': list_notificaciones_aceptada,
        'list_notificaciones_cancelada': list_notificaciones_cancelada,
        'count_pendientes': count_pendientes,
        'count_aceptadas': total_aceptadas,
        'count_rechazadas': total_canceladas,
        'count_solicitudes_pendientes': count_solicitudes_pendientes,
        'sugerencias_mentorias': list_sugges
    })


class UpdateCita(SuccessMessageMixin, UpdateView):
    model = Cita
    form_class = EditCita
    template_name = 'pages/appointment/edit_appointment.html'
    success_message = _("Se actualizo el estado de la cita seleccionada")


class ConfirmMailView(SuccessMessageMixin, UpdateView):
    model = UserCustom
    form_class = ConfirmMailForm
    template_name = 'pages/user/confirm_mail.html'
    success_message = _("¡Correo electronico confirmado!")


class UpdateCitaAjaxToRemove(SuccessMessageMixin, UpdateView):
    model = Cita
    form_class = EditCitaRemove
    template_name = 'pages/appointment/edit_appointment_ajax_to_confirm_or_remove.html'
    success_message = _("Se actualizo el estado de la cita seleccionada")

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            form = self.form_class(request.POST, instance=self.get_object())
            current_time = datetime.datetime.now()
            a = datetime.datetime(current_time.year, current_time.month, current_time.day,
                                  current_time.hour, current_time.minute, current_time.second)

            b = datetime.datetime(form.instance.fecha_confirmada_reunion.year, form.instance.fecha_confirmada_reunion.month, form.instance.fecha_confirmada_reunion.day,
                                  form.instance.fecha_confirmada_reunion.hour, form.instance.fecha_confirmada_reunion.minute, form.instance.fecha_confirmada_reunion.second)
            c = b-a

            if c.total_seconds() <= 86400:
                mensaje = _(
                    "Lo sentimos, la Mentoría se cancelo por no confirmarla en el tiempo establecido")

            if form.is_valid():
                form.save()
                error = "no hay error"
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 201
                return response
            else:
                error = form.errors
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return redirect('notifications')


class UpdateCitaAjaxToFinish(SuccessMessageMixin, UpdateView):
    model = Cita
    form_class = EditCitaFinish
    template_name = 'pages/appointment/edit_appointment_ajax_to_finish.html'
    success_message = _("Se actualizo el estado de la cita seleccionada")

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            form = self.form_class(request.POST, instance=self.get_object())
            current_time = datetime.datetime.now()
            mensaje = ""

            if form.instance.fecha_confirmada_reunion > current_time:
                mensaje = _(
                    "Mentoría terminada")

            if form.is_valid():
                form.save()
                error = "no hay error"
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 201
                return response
            else:
                error = form.errors
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return redirect('notifications')


class UpdateCitaAjax(SuccessMessageMixin, UpdateView):
    model = Cita
    form_class = EditCitaConfirmar
    template_name = 'pages/appointment/edit_appointment_ajax.html'
    success_message = _("Se actualizo el estado de la cita seleccionada")

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            form = self.form_class(request.POST, instance=self.get_object())
            current_time = datetime.datetime.now()
            a = datetime.datetime(current_time.year, current_time.month, current_time.day,
                                  current_time.hour, current_time.minute, current_time.second)

            b = datetime.datetime(form.instance.fecha_estimada_reunion.year, form.instance.fecha_estimada_reunion.month, form.instance.fecha_estimada_reunion.day,
                                  form.instance.fecha_estimada_reunion.hour, form.instance.fecha_estimada_reunion.minute, form.instance.fecha_estimada_reunion.second)
            c = b-a

            if c.total_seconds() <= 86400:
                mensaje = _(
                    "Lo sentimos, la Mentoría se cancelo por no confirmarla en el tiempo establecido")
            else:
                mensaje = _("Se ha confirmado la fecha de la Mentoría")

            if form.is_valid():
                form.save()
                error = "no hay error"
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 201
                return response
            else:
                error = form.errors
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return redirect('notifications')


class UpdateSuggestionAjax(SuccessMessageMixin, UpdateView):
    model = Suggestion
    form_class = EditSuggestion
    template_name = 'pages/suggestion/edit_suggestion_ajax.html'
    # success_message = _("Se acepto")

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            form = self.form_class(request.POST, instance=self.get_object())

            mensaje = _(
                "Sugerencia aceptada correctamente")

            if form.is_valid():
                form.save()
                error = "no hay error"
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 201
                return response
            else:
                error = form.errors
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return redirect('notifications')


class UpdateSuggestionAjaxCancel(SuccessMessageMixin, UpdateView):
    model = Suggestion
    form_class = EditSuggestion
    template_name = 'pages/suggestion/edit_suggestion_ajax_cancel.html'

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            form = self.form_class(request.POST, instance=self.get_object())

            mensaje = _(
                "Sugerencia descartada correctamente")

            if form.is_valid():
                form.save()
                error = "no hay error"
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 201
                return response
            else:
                error = form.errors
                response = JsonResponse({'mensaje': mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return redirect('notifications')


# Formulario con los datos que requiere un mentor


class CompleteDataMentor(SuccessMessageMixin, UpdateView):
    model = UserCustom
    # form_class = EditMentorForm
    form_class = CompleteDataOfStepsUserMentor
    template_name = 'pages/user/complete_data_mentor.html'
    success_message = _(
        "¡Estupendo! Ahora cuéntanos un poco acerca de tu negocio y tu experiencia como mentor.")
    image_linkedin = None
    user_name = None
    user_lastname = None
    image_linkedin_none = ""

    def get_linkedin_image(self, user):
        for users in UserSocialAuth.objects.all():
            if users.extra_data['email_address'] == user.email:

                acces_token = users.extra_data['access_token']

                # # configuramos la url
                URL = f'https://api.linkedin.com/v2/me?projection=(id,profilePicture(displayImage~digitalmediaAsset:privatePlayableStreams~digitalmediaAsset:playableStreams))&oauth2_access_token={ acces_token }'

                # # solicitamos la información y guardamos la respuesta en data.
                data = requests.get(URL)

                rta_json = data.json()

                if len(rta_json) == 2:
                    self.image_linkedin = rta_json['profilePicture']['displayImage~'][
                        'elements'][2]['identifiers'][0]['identifier']
                else:
                    self.image_linkedin = ""
                    self.image_linkedin_none = "true"

                self.avatar_image = self.image_linkedin
                self.user_name = users.extra_data['first_name']
                self.user_lastname = users.extra_data['last_name']

    def get(self, request, pk):
        self.get_linkedin_image(request.user)

        return render(
            request,
            self.template_name,
            {
                'title': 'Completar datos',
                'form': self.form_class,
                'image_linkedin': self.image_linkedin,
                'user_name': self.user_name,
                'user_lastname': self.user_lastname,
                "image_linkedin_none": self.image_linkedin_none
            }
        )

    def get_success_url(self):
        return reverse('complete_data_two_mentor')


class CompleteDataTwoMentor(SuccessMessageMixin, CreateView):
    model = Mentor
    form_class = CompleteDataTwoOfStepsUserMentor
    template_name = 'pages/user/complete_data_two_mentor.html'
    success_message = _(
        "¡Estupendo! Tus datos como mentor se guardaron correctamente.")

    def get(self, request):
        return render(
            request,
            self.template_name,
            {
                'title': 'Completar datos',
                'form': self.form_class,
            }
        )

    def get_success_url(self):
        return reverse('profile')


class CompleteDataEmprendedor(SuccessMessageMixin, UpdateView):
    model = UserCustom
    form_class = CompleteDataOfStepsUser
    template_name = 'pages/user/complete_data_emprendedor.html'
    success_message = _(
        "¡Estupendo! Ahora cuéntanos un poco acerca de tu negocio y tu experiencia como emprendedor.")

    def get(self, request, pk):

        return render(
            request,
            self.template_name,
            {
                'title': 'Completar datos',
                'form': self.form_class,
            }
        )

    def get_success_url(self):
        return reverse('complete_data_two_emprendedor')


class CompleteDataTwoEmprendedor(SuccessMessageMixin, CreateView):
    model = Emprendedor
    form_class = CompleteDataTwoOfStepsUser
    template_name = 'pages/user/complete_data_two_emprendedor.html'
    success_message = _(
        "¡Estupendo! Tus datos como emprendedor se guardaron correctamente.")
    # conection = ConnectApi()

    def get(self, request):

        return render(
            request,
            self.template_name,
            {
                'title': 'Completar datos',
                'form': self.form_class,
            }
        )

    def get_success_url(self):
        return reverse('profile_welcome', kwargs={'pk': self.request.user.id})


class ValidateFirstSession(SuccessMessageMixin, UpdateView):
    model = UserCustom
    form_class = ValidateSession
    template_name = 'pages/user/first_session.html'
    success_message = _("Bienvenido a Altruism Now Mentors")

    def get(self, request,  *args, **kwargs):

        if validate_user_authenticate_emprendedor(request.user, UserCustom) == False:
            return redirect('index_vacio')
        else:
            return render(
                request,
                self.template_name,
                {
                    'title': 'Validar Sesión',
                    'form': self.form_class,
                }
            )

    def get_success_url(self):
        return reverse('search_mentor')
# def create_event():
#     event_title = "jajaja no te creo"
#     event_desc = "jajaja y en la descrip"
#     start_date = "2015-05-28T09:00:00-07:00"
#     end_date = "2015-05-28T17:00:00-07:00"
#     calendar_service = get_calendar_service()

#     event_result = calendar_service.events().insert(calendarId='primary',
#         body={
#             "summary": event_title,
#             "description": event_desc,
#             "start": {"dateTime": start_date, "timeZone": 'America/Bogota'},
#             "end": {"dateTime": end_date, "timeZone": 'America/Bogota'},
#         }
#     ).execute()
