from django import forms

from .models import RatingUser


class CreateRating(forms.ModelForm):

    class Meta:
        model = RatingUser
        fields = (
            'user',
            'rating_user',
            'description_rating',
        )

    def save(self, commit=True):
        rating = super().save(commit=False)

        if commit:
            rating.save()

        return rating
