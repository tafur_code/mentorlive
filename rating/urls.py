from django.urls import path

from . import views

urlpatterns = [
    # My urls
    path('calificacion-usuario/', views.CreateRating.as_view(), name="rating_user"),
]
