import imp
from django.contrib import admin

from .models import RatingUser

# Register your models here.
admin.site.register(RatingUser)
