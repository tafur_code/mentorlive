import imp
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.urls import reverse

from user.models import UserCustom

# Create your models here.
NUMBERS_RATING = (
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
)


class RatingUser(models.Model):
    user = models.ForeignKey(UserCustom, verbose_name=_(
        "Usuario"), on_delete=models.CASCADE)
    rating_user = models.IntegerField(
        'Calificación',
        choices=NUMBERS_RATING,
    )
    description_rating = models.TextField(
        max_length=500, verbose_name='Comparte detalles de tu experiencia con el mentor', default="none")

    def __str__(self):
        return f"{ self.user } - { self.rating_user }"

    def get_absolute_url(self):
        return reverse("profile")
