from statistics import mode
from django.shortcuts import render
from django.views.generic import CreateView
from django.http import JsonResponse
from django.shortcuts import redirect

from . import models, forms
# Create your views here.


class CreateRating(CreateView):
    model = models.RatingUser
    form_class = forms.CreateRating
    template_name = 'pages/rating/create_rating.html'
    mensaje = ""

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            form = self.form_class(request.POST)

            if form.is_valid():
                form.save()
                self.mensaje = "Se ha calificado el usuario correctamente, sera redireccionado en 3 segundos"
                error = "no hay error"
                response = JsonResponse(
                    {'mensaje': self.mensaje, 'error': error})
                response.status_code = 201
                return response
            else:
                error = form.errors
                response = JsonResponse(
                    {'mensaje': self.mensaje, 'error': error})
                response.status_code = 400
                return response
        else:
            return redirect('notifications')
