from django.contrib import admin
from .models import SocialNetworks, BrandSettings, ContactInformation, TestimonalList, ListWhyUs

# Register your models here.


class SocialNetworksAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    readonly_fields = ('created_at',)
    list_display = ('title', 'created_at')
    search_fields = ('title', 'description')


class BrandSettingsAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    readonly_fields = ('created_at',)
    list_display = ('title', 'created_at')
    search_fields = ('title', 'description')


class ContactInformationAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    readonly_fields = ('created_at',)
    list_display = ('title', 'created_at')
    search_fields = ('title', 'description')


class TestimonialListAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    readonly_fields = ('created_at',)


class WhyUsAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    readonly_fields = ('created_at',)


admin.site.register(SocialNetworks, SocialNetworksAdmin)
admin.site.register(BrandSettings, BrandSettingsAdmin)
admin.site.register(ContactInformation, ContactInformationAdmin)
admin.site.register(TestimonalList, TestimonialListAdmin)
admin.site.register(ListWhyUs, WhyUsAdmin)
