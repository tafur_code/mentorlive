from modeltranslation.translator import translator, TranslationOptions
from .models import BrandSettings, TestimonalList, ListWhyUs


class BrandSettingsTranslationOptions(TranslationOptions):
    fields = ('description_footer', 'text_copyright')


class TestimonalListTranslationOptions(TranslationOptions):
    fields = ('razon_emprendimiento', 'description')


class WhyUsTranslationOptions(TranslationOptions):
    fields = ('title', 'description_why')


translator.register(BrandSettings, BrandSettingsTranslationOptions)
translator.register(TestimonalList, TestimonalListTranslationOptions)
translator.register(ListWhyUs, WhyUsTranslationOptions)
