from .models import SocialNetworks, TestimonalList, BrandSettings, ContactInformation, ListWhyUs


def get_social_networks(request):

    social_networks = SocialNetworks.objects.all()

    return {
        'social_networks': social_networks,
    }


def get_brand_settings(request):
    brand_settings = BrandSettings.objects.all()

    return {
        'brand_settings': brand_settings
    }


def get_contact_information(request):
    contact_information = ContactInformation.objects.all()

    return {
        'contact_information': contact_information
    }


def get_testimonals_list(request):
    testimonial_list = TestimonalList.objects.all()

    return {
        'testimonial_list': testimonial_list
    }


def list_why_us(request):
    list_why_us = ListWhyUs.objects.all()

    return {
        'list_why_us': list_why_us
    }
