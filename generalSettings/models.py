from django.db import models

# Richtext of django
from ckeditor.fields import RichTextField

from user.models import UserCustom
from django.utils.translation import gettext_lazy as _

# Create your models here.


class SocialNetworks(models.Model):
    title = "Social networks"
    facebook_url = models.CharField(
        max_length=1000,
        verbose_name="Enlace facebook"
    )
    instagram_url = models.CharField(
        max_length=1000,
        verbose_name="Enlace instagram"
    )
    linkedin_url = models.CharField(
        max_length=1000,
        verbose_name="Enlace linkedin"
    )
    whatsapp_number = models.CharField(
        max_length=20,
        verbose_name="Número whatsapp",
        help_text="Ingresar el indicativo del pais antes del número sin el (+) ni espacios"
    )
    tiktok_url = models.CharField(
        max_length=1000,
        verbose_name="Enlace tiktok",
        default=' '
    )

    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Creado el'
    )

    class Meta:
        verbose_name = 'Redes sociales comercio'
        verbose_name_plural = 'Redes sociales comercio'

    def __str__(self):
        return self.title


class BrandSettings(models.Model):
    title = "Configuración de marca"
    logo_footer = models.ImageField(
        default='null', verbose_name='Logo del footer', upload_to='brand_settings')
    description_footer = models.TextField(
        max_length=500, verbose_name='Descripción footer')
    text_copyright = models.CharField(
        max_length=200, verbose_name='Texto copyright')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Creado el'
    )

    class Meta:
        verbose_name = 'Configuraciones de marca'
        verbose_name_plural = 'Configuraciones de marca'

    def __str__(self):
        return self.title


class ContactInformation(models.Model):
    title = "Información de contacto"
    number_support = models.CharField(
        max_length=500, verbose_name='Número para soporte')
    business_hours = models.CharField(
        max_length=500, verbose_name='Horario de atención')
    contact_email = models.CharField(
        max_length=300, verbose_name='Correo de contacto')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Creado el'
    )

    class Meta:
        verbose_name = 'Información de contacto'
        verbose_name_plural = 'Información de contacto'

    def __str__(self):
        return self.title


class TestimonalList(models.Model):
    avatar_user = models.ImageField(
        default='null', verbose_name='Avatar del emprendedor', upload_to='avatar_emprendedor')

    name_user = models.CharField(
        max_length=100, verbose_name='Nombre emprendedor')

    razon_emprendimiento = models.CharField(
        max_length=100, verbose_name='Razón del emprendimiento')

    name_mentor = models.CharField(
        max_length=100, verbose_name='Nombre del mentor', default='')

    description = models.TextField(
        default='', verbose_name='Descripción del testimonio', max_length=500
    )

    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Creado el'
    )

    class Meta:
        verbose_name = 'Testimonio'
        verbose_name_plural = 'Testimonios'

    def __str__(self):
        return f'{ self.name_user } | { self.razon_emprendimiento }'


class ListWhyUs(models.Model):
    cover = models.ImageField(
        default='null', verbose_name='Imagen', upload_to='general')
    title = models.CharField(
        max_length=100, verbose_name='Titulo por que elegirnos')
    description_why = RichTextField(
        verbose_name='Lista por que elegirnos', config_name='list_plans_ckeditor')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Creado el')

    class Meta:
        verbose_name = 'Por qué elegir Altruism Now'
        verbose_name_plural = 'Por qué elegir Altruism Now'

    def __str__(self):
        return self.title
