from django import forms
from django.utils.translation import gettext_lazy as _

from .models import Emprendedor, Mentor
from user.models import rta_json

AREAS = (
    ('', _("Seleccione una opción")),
    ('Contabilidad', _("Contabilidad")),
    ('Auditorias', _("Auditorias")),
    ('Ingresos / Egresos', _("Ingresos / Egresos")),
    ('Presupuesto', _("Presupuesto")),
    ('Flujo de Efectivo', _("Flujo de Efectivo")),
    ('Planeación Financiera', _("Planeación Financiera")),
    ('Préstamos y Financiamiento', _("Préstamos y Financiamiento")),
    ('Otro tipo de Contabilidad', _("Otro tipo de Contabilidad")),
    ('Impuestos', _("Impuestos")),
    ('Compensaciones y Prestaciones', _("Compensaciones y Prestaciones")),
    ('Contratistas y Consultores', _("Contratistas y Consultores")),
    ('Administración de personal', _("Administración de personal")),
    ('Capacitación de personal', _("Capacitación de personal")),
    ('Otra forma de recursos humanos', _("Otra forma de recursos humanos")),
    ('Políticas del personal', _("Políticas del personal")),
    ('Reclutamiento y Contratación', _("Reclutamiento y Contratación")),
    ('Gestión de voluntarios', _("Gestión de voluntarios")),
    ('Aduanas y Aranceles', _("Aduanas y Aranceles")),
    ('Exportación e Importación', _("Exportación e Importación")),
    ('Mercados globales', _("Mercados globales")),
    ('Otro sector internacional', _("Otro sector internacional")),
    ('Outsourcing', _("Outsourcing")),
    ('Contratos', _("Contratos")),
    ('Derecho laboral', _("Derecho laboral")),
    ('Propiedad intelectual', _("Propiedad intelectual")),
    ('Otra área legal', _("Otra área legal")),
    ('Derecho de la propiedad', _("Derecho de la propiedad")),
    ('Derecho fiscal', _("Derecho fiscal")),
    ('Establecimiento de consejos directivos', _(
        "Establecimiento de consejos directivos")),
    ('Seguros comerciales', _("Seguros comerciales")),
    ('Estrategias de negocios', _("Estrategias de negocios")),
    ('Reacudación de fondos', _("Reacudación de fondos")),
    ('Crecimiento y Desarrollo Profesional', _(
        "Crecimiento y Desarrollo Profesional")),
    ('Liderazgo', _("Liderazgo")),
    ('Otras áreas administrativas', _("Otras áreas administrativas")),
    ('Planeamiento y Programación por metas', _(
        "Planeamiento y Programación por metas")),
    ('Administración de proyectos', _("Administración de proyectos")),
    ('Equilibrio trabajo - vida personal', _("Equilibrio trabajo - vida personal")),
    ('Publicidad y Promoción', _("Publicidad y Promoción")),
    ('Branding e Identidad Corporativa', _("Branding e Identidad Corporativa")),
    ('Desarrollo de negocios', _("Desarrollo de negocios")),
    ('Distribución', _("Distribución")),
    ('Investigación de mercados', _("Investigación de mercados")),
    ('Materiales de mercadotecnia', _("Materiales de mercadotecnia")),
    ('Estrategias de mercadotecnia', _("Estrategias de mercadotecnia")),
    ('Otra forma de mercadotecnia', _("Otra forma de mercadotecnia")),
    ('Fijación de precios', _("Fijación de precios")),
    ('Desarrollo de productos', _("Desarrollo de productos")),
    ('Relaciones públicas y Medios', _("Relaciones públicas y Medios")),
    ('Redes sociales', _("Redes sociales")),
    ('Marketing en línea', _("Marketing en línea")),
    ('Redacción y Edición', _("Redacción y Edición")),
    ('Manejo de inventarios', _("Manejo de inventarios")),
    ('Logística', _("Logística")),
    ('Manufactura', _("Manufactura")),
    ('Otras operaciones', _("Otras operaciones")),
    ('Embalaje y etiquetado', _("Embalaje y etiquetado")),
    ('Mejora de procesos y optimización', _("Mejora de procesos y optimización")),
    ('Adquisiciones y Proveedores', _("Adquisiciones y Proveedores")),
    ('Diseño de programas y evaluación', _("Diseño de programas y evaluación")),
    ('Gestión de calidad', _("Gestión de calidad")),
    ('Transporte y Entrega', _("Transporte y Entrega")),
    ('Servicio / Atención al cliente y Gestion de relaciones comerciales',
     _("Servicio / Atención al cliente y Gestion de relaciones comerciales")),
    ('Contratos gubernamentales', _("Contratos gubernamentales")),
    ('Generacion de contactos', _("Generacion de contactos")),
    ('Otras áreas de ventas', _("Otras áreas de ventas")),
    ('Venta al público', _("Venta al público")),
    ('Venta de servicios', _("Venta de servicios")),
    ('Venta al por mayor B2B', _("Venta al por mayor B2B")),
    ('Planificación empresarial', _("Planificación empresarial")),
    ('Franquicias', _("Franquicias")),
    ('Cómo comenzar', _("Cómo comenzar")),
    ('Estructura legal', _("Estructura legal")),
    ('Localización y Zonificación', _("Localización y Zonificación")),
    ('Otras áreas de Planificación empresarial', _(
        "Otras áreas de Planificación empresarial")),
    ('Otras áreas de Planificación empresarial76', _(
        "Otras áreas de Planificación empresarial")),
    ('Eficiencia estratégica', _("Eficiencia estratégica")),
    ('Negocios sustentables', _("Negocios sustentables")),
    ('Productos ecológicos', _("Productos ecológicos")),
    ('Otras áreas de sustentabilidad', _("Otras áreas de sustentabilidad")),
    ('Comercio electrónico', _("Comercio electrónico")),
    ('Gestión de tecnología', _("Gestión de tecnología")),
    ('Tecnología e Internet', _("Tecnología e Internet")),
    ('Planificación de recursos tecnológicos', _(
        "Planificación de recursos tecnológicos")),
    ('Telecomunicaciones', _("Telecomunicaciones")),
    ('Diseño de páginas web', _("Diseño de páginas web")),
    ('Otras áreas en tecnología e internet', _(
        "Otras áreas en tecnología e internet")),
    ('Salud y Alimentación', _("Salud y Alimentación")),
    ('Instrospección, Amor y Sanación de Traumas', _(
        "Instrospección, Amor y Sanación de Traumas")),
    ('Instroducción al Pensamiento Crítico', _(
        "Instroducción al Pensamiento Crítico")),
    ('Manejo de Inteligencia Emocional', _("Manejo de Inteligencia Emocional")),
    ('Desarrollo de Mindfulness', _("Desarrollo de Mindfulness")),
)


class CompleteDataTwoOfStepsUser(forms.ModelForm):

    ayudas_para_negocio = forms.MultipleChoiceField(
        label='Selecciona 3 áreas de especialización que pudieran ayudar a tu negocio en la actualidad.',
        required=True,
        widget=forms.SelectMultiple,
        choices=AREAS,
    )

    ubicacion_negocio = forms.MultipleChoiceField(
        required=True,
        widget=forms.SelectMultiple,
        choices=[(data_paises['name'], data_paises['name'])
                 for data_paises in rta_json],
    )

    class Meta:
        model = Emprendedor
        fields = ('__all__')

    def clean_ayudas_para_negocio(self):
        ayudas_para_negocio = self.cleaned_data.get('ayudas_para_negocio')

        if len(ayudas_para_negocio) < 3:
            raise forms.ValidationError(
                _("Seleccione mínimo 3 areas de especialización"))

        return ayudas_para_negocio

    def save(self, commit=True):
        user = super().save(commit=False)
        if commit:
            user.save()

        return user


class EditDataUserEmprendedor(forms.ModelForm):
    ayudas_para_negocio = forms.MultipleChoiceField(
        label='Selecciona 3 áreas de especialización que pudieran ayudar a tu negocio en la actualidad.',
        required=True,
        widget=forms.SelectMultiple,
        choices=AREAS,
    )

    ubicacion_negocio = forms.MultipleChoiceField(
        required=True,
        widget=forms.SelectMultiple,
        choices=[(data_paises['name'], data_paises['name'])
                 for data_paises in rta_json],
    )

    class Meta:
        model = Emprendedor
        fields = ('__all__')

    def save(self, commit=True):
        user = super().save(commit=False)

        if commit:
            user.save()

        return user


DIAS = (
    ('', _("Seleccione una opción")),
    ('Lunes', _("Lunes")),
    ('Martes', _("Martes")),
    ('Miercoles', _("Miercoles")),
    ('Jueves', _("Jueves")),
    ('Viernes', _("Viernes")),
    ('Sabado', _("Sábado")),
    ('Domingo', _("Domingo")),
)


class CompleteDataTwoOfStepsUserMentor(forms.ModelForm):
    ayudas_para_negocio = forms.MultipleChoiceField(
        label='Selecciona 3 áreas  de especialización en los que pudieras ayudar a los emprendedores',
        required=True,
        widget=forms.SelectMultiple,
        choices=AREAS,
    )

    ubicacion_negocio = forms.MultipleChoiceField(
        required=True,
        widget=forms.SelectMultiple,
        choices=[(data_paises['name'], data_paises['name'])
                 for data_paises in rta_json],
    )

    dias_disponibilidad = forms.MultipleChoiceField(
        label='¿Que días tienes disponibles para la mentoria?',
        required=True,
        widget=forms.SelectMultiple,
        choices=DIAS,
    )

    class Meta:
        model = Mentor
        fields = ('__all__')
    
    def clean_ayudas_para_negocio(self):
        ayudas_para_negocio = self.cleaned_data.get('ayudas_para_negocio')

        if len(ayudas_para_negocio) < 3:
            raise forms.ValidationError(
                _("Seleccione mínimo 3 areas de especialización"))
        
        return ayudas_para_negocio

    def save(self, commit=True):
        user = super().save(commit=False)

        if commit:
            user.save()

        return user
# GENDER = (
#     ('', _('Seleccione un genero')),
#     ('M', _('Masculino')),
#     ('F', _('Femenino'))
# )


class EditDataUserMentor(forms.ModelForm):

    ayudas_para_negocio = forms.MultipleChoiceField(
        label='Selecciona 3 áreas de especialización que pudieran ayudar a tu negocio en la actualidad.',
        required=True,
        widget=forms.SelectMultiple,
        choices=AREAS,
    )

    ubicacion_negocio = forms.MultipleChoiceField(
        required=True,
        widget=forms.SelectMultiple,
        choices=[(data_paises['name'], data_paises['name'])
                 for data_paises in rta_json],
    )

    dias_disponibilidad = forms.MultipleChoiceField(
        label='¿Que días tienes disponibles para la mentoria?',
        required=True,
        widget=forms.SelectMultiple,
        choices=DIAS,
    )

    class Meta:
        model = Mentor
        fields = ('__all__')

    def save(self, commit=True):
        user = super().save(commit=False)

        if commit:
            user.save()

        return user
