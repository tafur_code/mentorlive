from statistics import mode
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.validators import MinLengthValidator

from django.conf import settings
from user.models import rta_json

ETAPA_NEGOCIO = (
    ('', _('Seleccione una opción')),
    (_('01'), _('Tengo una idea de Negocio / Aún no esta en funcionamiento y no cuento con un prototipo ni con clientes')),
    (_('02'), _('Mi negocio esta en funcionamiento pero aún no he generado ingresos Los ingresos son el resultado de vender un producto o un servicio.')),
    (_('03'), _('Ya tengo clientes e ingresos pero mi negocio aún no es lucrativo / Un negocio no es lucrativo cuando los ingresos son menores a la ganancia estimada.')),
    (_('04'), _('Mi negocio está operando a gran escala y tengo utilidades. / Escalar significa que tu negocio es capaz de manejar descuentos y compras a volumen. Un negocio es lucrativo cuando los ingresos generados son mayores a los gastos.')),
)

TIPO_NEGOCIO = (
    ('', _("Seleccione una opción")),
    ('lucro', _("De lucro")),
    ('sinlucro', _("Sin fines de lucro")),
    ('empresa_social', _("Empresa social")),
    ('no_se', _("No estoy seguro")),
)

INDUSTRIA_NEGOCIO = (
    ('', _("Seleccione una opción")),
    ('03', _("Agricultura / Granja / Rancho")),
    ('04', _("Animales / Mascotas")),
    ('05', _("Arquitectura / Diseño de interiores")),
    ('48', _("Artículos deportivos / Recreación / Actividades al aire libre")),
    ('06', _("Artesanías / Manualidades")),
    ('07', _("Artes: Música / Plásticas / Escénicas")),
    ('42', _("Asistencia personal ejecutiva")),
    ('09', _("Belleza / Cuidado del cabello / Cosméticos")),
    ('44', _("Bienes raices")),
    ('22', _("Comercio electrónico / Ventas en Línea")),
    ('49', _("Conducción de vehiculos")),
    ('11', _("Consultoría de negocios / Coaching")),
    ('14', _("Construcción / Personal de obra / Reparaciones")),
    ('15', _("Consejería / Terapia / Salud mental")),
    ('12', _("Cuidado de niños")),
    ('02', _("Cuidado de adultos mayores / Atención médica domiciliaria")),
    ('18', _("Distribución y Transporte")),
    ('29', _("Diseño gráfico / Desarrollo de sitios web")),
    ('19', _("Educación / Capacitación")),
    ('30', _("Energías renovables")),
    ('20', _("Entretenimiento / Recreación / Eventos")),
    ('21', _("Exportación / Importación")),
    ('28', _("Fabricación de muebles / Reparacion / Venta")),
    ('43', _("Fotografía / Sonido / Servicios de video")),
    ('25', _("Floristería / Regalos")),
    ('33', _("Joyería / Artículos de lujo")),
    ('34', _("Jardinería")),
    ('10', _("Librerias / Puestos de periódicos y revistas")),
    ('35', _("Lavandería / Tintorería")),
    ('37', _("Manufactura")),
    ('16', _("Marketing digital / Comercio electrónico / Redes sociales")),
    ('39', _("Medios / Editorial")),
    ('38', _("Mercadotecnia / Publicidad")),
    ('23', _("Moda / Boda / Accesorios")),
    ('41', _("Multimedia / Redes sociales")),
    ('26', _("Productos alimenticios / Abarrotes")),
    ('45', _("Reclutamiento / Contratación / Servicios personales")),
    ('54', _("Redacción / Edición")),
    ('46', _("Restaurante / Cafe / Bar / Banquetería")),
    ('08', _("Refracciones / Mecánico")),
    ('24', _("Servicios Financieros / Seguros")),
    ('27', _("Silvicultura / Productos de madera")),
    ('17', _("Servicios para personas con discapacidad")),
    ('13', _("Servicios informáticos / Tecnología de la información")),
    ('01', _("Servicios contables y fiscales")),
    ('31', _("Salud / Bienestar / Actividad física / Ejercicio")),
    ('32', _("Servicios de limpieza comerciales y domésticos")),
    ('36', _("Servicios legales")),
    ('40', _("Sin fines de Lucro / Empresa social")),
    ('50', _("Traducción / Guía")),
    ('47', _("Venta al público")),
    ('52', _("Veterinaria")),
    ('51', _("Viajes / Turismo / Hotelería")),
    ('53', _("Vinos / Licores")),
)

# Create your models here.

class Emprendedor(models.Model):
    # uno a uno
    id_emprendedor  = models.IntegerField(
        'emprendedor',
    )
    
    etapa_negocio = models.CharField(
        '¿En que etapa de desarrollo se encuentra tu negocio?',
        max_length=500,
        default=" ",
        choices=ETAPA_NEGOCIO
    )

    objetivos_retos_cumplidos = models.TextField(
        '¿Objetivos o retos que ya haz cumplido?',
        max_length=2000,
        default=" "
    )

    tipo_negocio_en_mente = models.CharField(
        '¿Qué idea o tipo de negocio tienes en mente?',
        max_length=500,
        choices=TIPO_NEGOCIO,
        default=" "
    )

    porque_con_mentor = models.TextField(
        '¿Por qué quieres trabajar con un mentor?',
        max_length=2000,
        default=" "
    )
    
    nombre_negocio = models.CharField(
        '¿Cómo se llama tu negocio?',
        max_length=800,
        default=" "
    )

    metas_futuras = models.TextField(
        '¿Cuales son tus metas Futuras?',
        max_length=2000,
        default=" "
    )

    fecha_lanzamiento = models.DateTimeField(
        '¿Fecha de lanzamiento de tu negocio?'
    )

    ocupacion = models.CharField(
        '¿Cuál es tu ocupación?',
        max_length=800,
        default=" "
    )

    industria_negocio = models.CharField(
        '¿A qué industria pertenece tu negocio?',
        max_length=500,
        choices=INDUSTRIA_NEGOCIO,
        default=" "
    )

    descripcion_negocio = models.TextField(
        'Escribe una breve descripción de tu negocio para que los mentores lo conozcan.',
        max_length=2000,
        default=" "
    )

    profesion = models.CharField(
        '¿Cuál es tu Profesión?',
        max_length=800,
        default=" "
    )

    retos_comom_emprendedor = models.TextField(
        '¿Cuales son tus retos como emprendedor?',
        max_length=2000,
        default=" "
    )

    ayudas_para_negocio = models.CharField(
        'Selecciona 3 áreas de especialización que pudieran ayudar a tu negocio en la actualidad. ',
        max_length=500,
        default=" ",
    )

    ubicacion_negocio = models.CharField(
        '¿Dónde se ubica tu negocio?',
        max_length=500,
        null=True,
    )

    url_sitio_web = models.CharField(
        '¿URL o sitio web de tu negocio?',
        max_length=800,
        blank=True,
        null=True
    )

class Mentor(models.Model):
        # uno a uno
    id_mentor  = models.IntegerField(
        'mentor',
    )
    
    etapa_empresa = models.CharField(
        '¿En que etapa de desarrollo se encuentra tu empresa?',
        max_length=500,
        default=" ",
        choices=ETAPA_NEGOCIO
    )

    dias_disponibilidad = models.CharField(
        '¿Que días tienes disponibles para la mentoria?',
        max_length=500,
        default=" ",
    )

    descripcion_preparacion_emprendedor = models.TextField(
        '¿Cómo debe prepararse el emprendedor antes de comenzar con tu mentoría?',
        max_length=200,
        validators=[MinLengthValidator(100)],
        default=" "
    )

    tipo_empresa = models.CharField(
        '¿Qué tipo de empresa tienes?',
        max_length=500,
        choices=TIPO_NEGOCIO,
        default=" "
    )

    años_experiencia = models.IntegerField(
        '¿Cuántos años de experiencia tienes en áreas de negocios? Sugerimos 2 o más años.',
        default=" "
    )

    industria_negocio = models.CharField(
        '¿A qué industria pertenece tu negocio?',
        max_length=200,
        choices=INDUSTRIA_NEGOCIO,
        default=" "
    )

    años_experiencia_dueño = models.IntegerField(
        '¿Cuántos años de experiencia tienes como dueño de un negocio?',
        default=" "
    )

    descripcion_negocio = models.TextField(
        'Escribe una breve descripción de tu negocio para que los emprendedores lo conozcan.',
        max_length=200,
        validators=[MinLengthValidator(100)],
        default=" "
    )

    nombre_empresa = models.CharField(
        '¿Cómo se llama tu empresa?',
        max_length=500,
        default=" "
    )

    descripcion_experiencia_laboral = models.TextField(
        'Describe tu experiencia laboral',
        max_length=200,
        validators=[MinLengthValidator(100)],
        default=" "
    )

    cargo_empresa = models.CharField(
        '¿Cuál es tu cargo en la empresa?',
        max_length=200,
        default=" "
    )

    como_ayudar_emprendedores = models.TextField(
        '¿Cómo puedes ayudar a los emprendedores?*',
        max_length=200,
        validators=[MinLengthValidator(100)],
        default=" "
    )

    ocupacion = models.CharField(
        '¿Cuál es tu ocupación?',
        max_length=200,
        default=" "
    )

    mentoria_exitosa = models.TextField(
        '¿Qué esperas de una relación de mentoría exitosa?',
        max_length=200,
        validators=[MinLengthValidator(100)],
        default=" "
    )

    profesion = models.CharField(
        '¿Cuál es tu Profesión?',
        max_length=200,
        default=" "
    )

    ayudas_para_negocio = models.CharField(
        'Selecciona 3 áreas  de especialización en los que pudieras ayudar a los emprendedores',
        max_length=500,
        default=" ",
    )

    ubicacion_negocio = models.CharField(
        '¿Dónde se ubica tu negocio?',
        max_length=500,
        null=True,
    )

    url_sitio_web = models.CharField(
        '¿URL o sitio web de tu negocio?',
        max_length=2000,
        blank=True,
        null=True
    )
