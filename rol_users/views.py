from django.views.generic import UpdateView
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import render
from django.urls import reverse


from .models import Emprendedor, Mentor
from .forms import EditDataUserEmprendedor, EditDataUserMentor

# Create your views here.
class UpdateUserEmprendedor(SuccessMessageMixin, UpdateView):
    model = Emprendedor
    form_class = EditDataUserEmprendedor
    template_name = 'pages/user/edit_data_emprendedor.html'
    success_message = "Se editaron los campos satisfactoriamente"
    extra_context = {'title': 'Editar datos emprendedor'}

    def get_success_url(self):
         return reverse('profile')

class UpdateUserMentor(SuccessMessageMixin, UpdateView):
    model = Mentor
    form_class = EditDataUserMentor
    template_name = 'pages/user/edit_data_mentor.html'
    success_message = "Se editaron los campos satisfactoriamente"
    extra_context = {'title': 'Editar datos mentor'}

    def get_success_url(self):
         return reverse('profile')