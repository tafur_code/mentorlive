from django.apps import AppConfig


class RolUsersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'rol_users'
