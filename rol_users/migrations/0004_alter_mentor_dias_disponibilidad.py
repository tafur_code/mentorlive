# Generated by Django 3.2.8 on 2022-04-14 17:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rol_users', '0003_mentor_dias_disponibilidad'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mentor',
            name='dias_disponibilidad',
            field=models.CharField(default=' ', max_length=500, verbose_name='¿Que días tienes disponibles para la mentoria?'),
        ),
    ]
