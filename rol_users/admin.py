from django.contrib import admin

from .models import Emprendedor, Mentor

# Register your models here.
admin.site.register(Emprendedor)
admin.site.register(Mentor)
