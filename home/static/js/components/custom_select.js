import "../tomselect.min.js";

const lang = document.querySelector("html").getAttribute("lang")

const filterPais = document.querySelector("#id_dias_disponibilidad");

let placeholder = ""

if (lang == "en") {
  placeholder = "Select an option"
} else {
  placeholder = "Seleccione una opción"
}

if (filterPais) {
  new TomSelect(filterPais, {
    optionClass: "o-group-tags",
    itemClass: "o-tag",
    maxItems: 5,

    placeholder: placeholder,

    render: {
      no_results: function (data, escape) {
        return '<span class="u-hidden"></span>';
      },
    },

    plugins: {
      remove_button: {
        title: "Eliminar opción",
        className: "o-chip__close i-close",
      },
    },
    onDelete: () => {
      return true;
    },
  });
}

const filterTheme = document.querySelector("#id_speak_language");

if (filterTheme) {
  new TomSelect(filterTheme, {
    createOnBlur: true,
    create: true,
    optionClass: "o-group-tags",
    itemClass: "o-tag",
    maxItems: 5,
    placeholder: placeholder,

    render: {
      option_create: function (data, escape) {
        if (lang == "en") {
          return (
            '<div class="o-form__create create"> <span class="o-form__create-title">Add: </span> <strong class="o-tag">' +
            escape(data.input) +
            "</strong></div>"
          );
        } else {
          return (
            '<div class="o-form__create create"> <span class="o-form__create-title">Agregar: </span> <strong class="o-tag">' +
            escape(data.input) +
            "</strong></div>"
          );
        }
      },
      no_results: function (data, escape) {
        return '<span class="u-hidden"></span>';
      },
    },

    plugins: {
      remove_button: {
        title: "Eliminar opción",
        className: "o-chip__close i-close",
      },
    },
    onDelete: () => {
      return true;
    },
  });
}

const filterSpeakLanguage = document.querySelector("#speak_language");

if (filterSpeakLanguage) {
  new TomSelect(filterSpeakLanguage, {
    persist: false,
    createOnBlur: true,
    create: true,
  });
}

const filterCountry = document.querySelector("#id_ubicacion_negocio");

if (filterCountry) {
  new TomSelect(filterCountry, {
    placeholder: placeholder,
    itemClass: "o-tag",
    plugins: {
      remove_button: {
        title: "Eliminar Tema",
        className: "o-chip__close i-close",
      },
    },
    render: {
      no_results: function (data, escape) {
        return (
          '<span>No se encontraron resultados para "' +
          escape(data.input) +
          '"</span>'
        );
      },
    },

    onDelete: () => {
      return true;
    },
  });
}

const filterAyudas = document.querySelector("#id_ayudas_para_negocio");

const optionsTest = [];

if (filterAyudas) {
  for (let i = 0; i < filterAyudas.options.length; i++) {
    const element = filterAyudas.options[i];
    let ttt = {};

    if (i >= 1 && i <= 9) {
      ttt = { class: "contabilidad", value: element.text, name: element.text };
    } else if (i >= 10 && i <= 17) {
      ttt = { class: "recursos", value: element.text, name: element.text };
    } else if (i >= 18 && i <= 22) {
      ttt = { class: "internacional", value: element.text, name: element.text };
    } else if (i >= 19 && i <= 28) {
      ttt = { class: "leyes", value: element.text, name: element.text };
    } else if (i >= 29 && i <= 38) {
      ttt = { class: "admin", value: element.text, name: element.text };
    } else if (i >= 39 && i <= 52) {
      ttt = { class: "mercadotecnia", value: element.text, name: element.text };
    } else if (i >= 52 && i <= 62) {
      ttt = { class: "op", value: element.text, name: element.text };
    } else if (i >= 63 && i <= 69) {
      ttt = { class: "ventas", value: element.text, name: element.text };
    } else if (i >= 70 && i <= 75) {
      ttt = { class: "desarrollo", value: element.text, name: element.text };
    } else if (i >= 76 && i <= 80) {
      ttt = {
        class: "sustentabilidad",
        value: element.text,
        name: element.text,
      };
    } else if (i >= 81 && i <= 87) {
      ttt = { class: "tecnologia", value: element.text, name: element.text };
    } else if (i >= 88) {
      ttt = { class: "habilidades_blandas", value: element.text, name: element.text };
    }

    optionsTest.push(ttt);
  }

  let listOptgroups = []

  if (lang == "en") {
    listOptgroups = [
      {
        value: "contabilidad",
        label: "Accounting and Finance",
      },
      {
        value: "recursos",
        label: "Human Resources",
      },
      {
        value: "internacional",
        label: "International",
      },
      {
        value: "leyes",
        label: "Laws and Law",
      },
      {
        value: "admin",
        label: "Management",
      },
      {
        value: "mercadotecnia",
        label: "Marketing",
      },
      {
        value: "op",
        label: "Operations",
      },
      {
        value: "ventas",
        label: "Sales",
      },
      {
        value: "desarrollo",
        label: "Developing",
      },
      {
        value: "sustentabilidad",
        label: "Sustainability",
      },
      {
        value: "tecnologia",
        label: "Technology and internet",
      },
      {
        value: "habilidades_blandas",
        label: "Soft Skills",
      },
    ]
  } else {
    listOptgroups = [
      {
        value: "contabilidad",
        label: "Contabilidad y Finanzas",
      },
      {
        value: "recursos",
        label: " Recursos Humanos",
      },
      {
        value: "internacional",
        label: " Internacional",
      },
      {
        value: "leyes",
        label: " Leyes y Derecho",
      },
      {
        value: "admin",
        label: "Administración",
      },
      {
        value: "mercadotecnia",
        label: "Mercadotecnia",
      },
      {
        value: "op",
        label: "Operaciones",
      },
      {
        value: "ventas",
        label: "Ventas",
      },
      {
        value: "desarrollo",
        label: "Desarrollo",
      },
      {
        value: "sustentabilidad",
        label: "Sustentabilidad",
      },
      {
        value: "tecnologia",
        label: "Tecnología e internet",
      },
      {
        value: "habilidades_blandas",
        label: "Habilidades Blandas",
      },
    ]
  }

  new TomSelect("#id_ayudas_para_negocio", {
    options: optionsTest,
    maxOptions: null,
    maxItems: 3,
    optionClass: "o-group-tags",
    itemClass: "o-tag",
    placeholder: placeholder,
    optgroups: listOptgroups,
    optgroupField: "class",
    labelField: "value",
    searchField: ["name"],
    render: {
      optgroup_header: function (data, escape) {
        return (
          '<div class="optgroup-header"> <h2>' +
          escape(data.label) +
          "</h2></div>"
        );
      },
    },
    plugins: {
      remove_button: {
        title: "Eliminar opción",
        className: "o-chip__close i-close",
      },
    },
    onDelete: () => {
      return true;
    },
  });
}


setTimeout(() => {
  function diasdispo() {
    setTimeout(() => {

      const countrys = document.querySelector(".js-diasdispo").innerHTML;

      if (countrys) {
        const countrs_dos = countrys.replace("['", '').replace("']", "")
        const countrs_tres = countrs_dos.replace("'", "")
        const countrs_cuatro = countrs_tres.replace("'", "")
        const countrs_cinco = countrs_cuatro.replace("'", "")
        const countrs_seis = countrs_cinco.replace("'", "")
        const countrs_siete = countrs_seis.replace("'", "")
        const countrs_ocho = countrs_siete.replace("'", "")
        const countrs_nueve = countrs_ocho.replace("'", "")
        const countrs_diez = countrs_nueve.replace("'", "")

        let arr = countrs_diez.split(',');
        //dividir la cadena de texto por una coma

        setTimeout(() => {
          const idTest = document.querySelector("#id_dias_disponibilidad-ts-control");
          const idTestDos = document.querySelector("#id_dias_disponibilidad-ts-dropdown");

          idTest.click()

          setTimeout(() => {
            const groupTags = idTestDos.querySelectorAll(".o-group-tags");

            groupTags.forEach(element => {

              arr.forEach(arrayObject => {

                if (element.dataset.value == arrayObject.trim()) {
                  element.click()

                  idTestDos.closest(".ts-dropdown").style.display = "none"
                  idTestDos.closest(".ts-dropdown").style.visibility = "hidden"
                }

              });

            });
          }, 200)

        }, 100)
      }

    }, 100)
  }

  diasdispo();
}, 200)


setTimeout(() => {
  function ubicacionnego() {
    setTimeout(() => {

      const countrys = document.querySelector(".js-ubicacionnego").innerHTML;

      if (countrys) {

        const countrs_dos = countrys.replace("['", '').replace("']", "")
        const countrs_tres = countrs_dos.replace("'", "").replace("'", "")
        const countrs_cuatro = countrs_tres.replace("'", "").replace("'", "")
        const countrs_cinco = countrs_cuatro.replace("'", "").replace("'", "")
        const countrs_seis = countrs_cinco.replace("'", "").replace("'", "")
        const countrs_siete = countrs_seis.replace("'", "").replace("'", "")
        const countrs_ocho = countrs_siete.replace("'", "").replace("'", "")

        let arr = countrs_ocho.split(',');
        //dividir la cadena de texto por una coma

        setTimeout(() => {
          const idTest = document.querySelector("#id_ubicacion_negocio-ts-control");
          const idTestDos = document.querySelector("#id_ubicacion_negocio-ts-dropdown");

          idTest.click()

          setTimeout(() => {
            const groupTags = idTestDos.querySelectorAll(".option");

            groupTags.forEach(element => {

              arr.forEach(arrayObject => {


                if (element.dataset.value == arrayObject.trim() || element.dataset.value == arrayObject.trim()) {
                  element.click()

                  idTestDos.closest(".ts-dropdown").style.display = "none"
                  idTestDos.closest(".ts-dropdown").style.visibility = "hidden"
                }
              });

            });
          }, 200)

        }, 100)

      }
    }, 200)
  }

  console.log("DOS")
  ubicacionnego()
}, 300)

setTimeout(() => {
  function ayudaspara_negocio() {
    setTimeout(() => {

      const countrys = document.querySelector(".js-ayudaspara_negocio").innerHTML;

      if (countrys) {

        const countrs_dos = countrys.replace("['", '').replace("']", "")
        const countrs_tres = countrs_dos.replace("'", "").replace("'", "")
        const countrs_cuatro = countrs_tres.replace("'", "").replace("'", "")

        console.log(countrs_cuatro)

        let arr = countrs_cuatro.split(',');
        //dividir la cadena de texto por una coma
        console.log("arr");
        console.log(arr);



        setTimeout(() => {
          const idTest = document.querySelector("#id_ayudas_para_negocio-ts-control");
          const idTestDos = document.querySelector("#id_ayudas_para_negocio-ts-dropdown");

          idTest.click()

          setTimeout(() => {
            const groupTags = idTestDos.querySelectorAll(".o-group-tags");

            groupTags.forEach(element => {

              arr.forEach(arrayObject => {


                if (element.dataset.value == arrayObject.trim() || element.dataset.value == arrayObject.trim()) {
                  element.click()

                  idTestDos.closest(".ts-dropdown").style.display = "none"
                  idTestDos.closest(".ts-dropdown").style.visibility = "hidden"
                }
              });

            });
          }, 200)

        }, 100)

      }
    }, 300)

  }

  console.log("TERES")
  ayudaspara_negocio()
}, 400)
