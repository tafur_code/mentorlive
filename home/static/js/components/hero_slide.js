function renderCardSlider(element) {
  let el = document.querySelector(element);

  if (el) {
    new Swiper(el, {
      slidesPerView: 1,
      spaceBetween: 24,
      effect: "fade",

      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
    });
  }
}

renderCardSlider(".js-slider-hero");
