function itemMenuActive() {
  const linksMenu = document.querySelectorAll(".js-menu-link");
  const pathnameWindow = window.location.pathname;

  linksMenu.forEach((currentLink) => {
    const idMenuLink = currentLink.getAttribute("id");

    if (pathnameWindow.indexOf(idMenuLink) > -1 && pathnameWindow.length > 4) {
      currentLink.classList.add("is-active");
    } else if (pathnameWindow.length == 4 && idMenuLink == "home") {
      currentLink.classList.add("is-active");
    }
  });
}

itemMenuActive();
