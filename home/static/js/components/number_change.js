if (document.getElementById("indicativos")) {
  const value = JSON.parse(document.getElementById("indicativos").textContent);
  const containerIndicator = document.createElement("ul");
  const fieldNumber = document.querySelector(".js-form-flag");
  const btnDropdown = document.querySelectorAll(".js-btn-dropdown");

  if (fieldNumber) {
    if (value) {
      value.forEach((indicativo) => {
        let option = document.createElement("li");
        const img = document.createElement("img");
        const p = document.createElement("p");

        option.classList.add("js-flag-change");

        img.setAttribute(
          "src",
          `https://flagcdn.com/${indicativo["ISO2"].toLowerCase()}.svg`
        );

        p.innerHTML = indicativo["PHONE_CODE"];

        fieldNumber.appendChild(containerIndicator);
        fieldNumber.appendChild(img);
        containerIndicator.appendChild(option);
        option.appendChild(img);
        option.appendChild(p);
      });

      function showContent() {
        btnDropdown.forEach((element) => {
          element.addEventListener("click", () => {
            element
              .closest(".js-form-flag")
              .classList.toggle("is-content-show");
          });
        });
      }

      function keyUpChangeFlag() {
        const input = document.querySelector("#id_number_phone");

        input.addEventListener("keyup", (e) => {
          if (e.target.value.length <= 4) {
            if (
              e.target.value.includes(300) ||
              e.target.value.includes(301) ||
              e.target.value.includes(302) ||
              e.target.value.includes(303) ||
              e.target.value.includes(304) ||
              e.target.value.includes(305) ||
              e.target.value.includes(310) ||
              e.target.value.includes(311) ||
              e.target.value.includes(312) ||
              e.target.value.includes(313) ||
              e.target.value.includes(314) ||
              e.target.value.includes(315) ||
              e.target.value.includes(316) ||
              e.target.value.includes(317) ||
              e.target.value.includes(318) ||
              e.target.value.includes(320) ||
              e.target.value.includes(321) ||
              e.target.value.includes(322) ||
              e.target.value.includes(323) ||
              e.target.value.includes(319) ||
              e.target.value.includes(350) ||
              e.target.value.includes(351)
            ) {
              fieldNumber
                .querySelector("img")
                .setAttribute("src", "https://flagcdn.com/co.svg");

              fieldNumber.querySelector(".o-form__flag-number").innerHTML =
                "57";
            } else if (
              e.target.value.includes(464) ||
              e.target.value.includes(716) ||
              e.target.value.includes(729) ||
              e.target.value.includes(732) ||
              e.target.value.includes(734) ||
              e.target.value.includes(736) ||
              e.target.value.includes(764) ||
              e.target.value.includes(770) ||
              e.target.value.includes(776) ||
              e.target.value.includes(778) ||
              e.target.value.includes(786) ||
              e.target.value.includes(788) ||
              e.target.value.includes(789)
            ) {
              fieldNumber
                .querySelector("img")
                .setAttribute("src", "https://flagcdn.com/ag.svg");

              fieldNumber.querySelector(".o-form__flag-number").innerHTML =
                "1 268";
            } else if (e.target.value.includes(9)) {
              fieldNumber
                .querySelector("img")
                .setAttribute("src", "https://flagcdn.com/ar.svg");

              fieldNumber.querySelector(".o-form__flag-number").innerHTML =
                "54";
            } else {
              fieldNumber.querySelector("img").setAttribute("src", "");
              fieldNumber.querySelector(".o-form__flag-number").innerHTML =
                "00";
            }
          }
        });
      }

      function clickToChangeFlag() {
        const flagBtn = document.querySelectorAll(".js-flag-change");

        flagBtn.forEach((element) => {
          element.addEventListener("click", () => {
            let srcImgCurrentFlag = element
              .querySelector("img")
              .getAttribute("src");
            let textCurrentIndicative = element.querySelector("p").innerHTML;

            fieldNumber
              .querySelector("img")
              .setAttribute("src", srcImgCurrentFlag);
            fieldNumber.querySelector(".o-form__flag-number").innerHTML =
              textCurrentIndicative;

            element
              .closest(".js-form-flag")
              .classList.remove("is-content-show");
          });
        });
      }

      showContent();
      keyUpChangeFlag();
      clickToChangeFlag();
    }
  }
}
