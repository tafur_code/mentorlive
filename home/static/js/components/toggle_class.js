class toggleClass {
  constructor({ element }) {
    this.element = element;
    this.parentElement = this.element.closest(".js-target-class");
    this.classToToggle = this.element.dataset.classIs;
    this.init();
  }

  init() {
    this.toggleEventToChangeClass();
  }

  /**
   * Add or Remove class of attr dataset class-is
   */
  toggleEventToChangeClass() {
    this.element.addEventListener("click", () => {
      if (this.parentElement) {
        this.parentElement.classList.toggle(this.classToToggle);
      }
    });
  }
}

const callToToggleClass = document.querySelectorAll(".js-btn-toggle-class");
callToToggleClass.forEach((item) => {
  new toggleClass({
    element: item,
  });
});

// function hoverLanguageButton() {
//   const btnLanguage = document.querySelector(".c-top-menu__list.js-target-class");

//   btnLanguage.addEventListener("mouseover", () => {
//     const parent = btnLanguage.closest(".js-target-class");

//     parent.classList.add("is-open-menu");
//   });
// }

// function leaveLanguageButton() {
//   const btnLanguage = document.querySelector(".c-top-menu__list.js-target-class");

//   btnLanguage.addEventListener("mouseleave", () => {
//     if (btnLanguage.classList.contains("is-open-menu")) {
//       btnLanguage.classList.remove("is-open-menu");
//     }
//   });
// }

// hoverLanguageButton();
// leaveLanguageButton();
