function renderCardSlider(element) {
  let el = document.querySelector(element);

  if (el) {
    var nextEl;
    var prevEl;
    var wrapper = el.closest(".js-slide-wrapper");

    if (wrapper) {
      nextEl = wrapper.querySelector(".swiper-button-next");
      prevEl = wrapper.querySelector(".swiper-button-prev");
    } else {
      nextEl = this.element.querySelector(".swiper-button-next");
      prevEl = this.element.querySelector(".swiper-button-prev");
    }

    new Swiper(el, {
      slidesPerView: 1,
      spaceBetween: 24,

      navigation: {
        nextEl: nextEl,
        prevEl: prevEl,
      },
    });
  }
}

renderCardSlider(".js-slider-quote");