function setIdentifierLanguage() {
  window.addEventListener("DOMContentLoaded", () => {
    const identifierLanguage = document.querySelectorAll(".js-language");
    const pathnameWindow = window.location.pathname;

    identifierLanguage.forEach((element) => {
      const idLanguage = element.getAttribute("id");

      if (pathnameWindow.indexOf(idLanguage) > -1) {
        element.classList.remove("u-hidden");
      } else {
        element.classList.add("u-hidden");
      }
    });
  });
}

setIdentifierLanguage();
