function renderSliderAllies(element) {
  let el = document.querySelector(element);

  if (el) {
    var nextEl;
    var prevEl;
    var wrapper = el.closest(".js-slide-wrapper");

    if (wrapper) {
      nextEl = wrapper.querySelector(".swiper-button-next");
      prevEl = wrapper.querySelector(".swiper-button-prev");
    }

    new Swiper(el, {
      slidesPerView: "auto",
      height: "auto",
      spaceBetween: 16,

      breakpoints: {
        720: {
          slidesPerView: 2,
        },
        1024: {
          slidesPerView: 3,
        },
      },
      navigation: {
        nextEl: nextEl,
        prevEl: prevEl,
      },
    });
  }
}

renderSliderAllies(".js-slider-allies");
