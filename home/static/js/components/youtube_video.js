const modal = document.querySelector(".js-target-modal_visible");

modal.classList.add("is-modal_visible");
document.querySelector("body").style.overflow = "hidden";

// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement("script");

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName("script")[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
  player = new YT.Player("player", {
    height: "360",
    width: "640",
    videoId: "wyQSqhyIHJc",
    playerVars: {
      controls: '0',
      disablekb: '1',
      rel: '0'
    },
    events: {
      onReady: onPlayerReady,
      onStateChange: onPlayerStateChange,
    },
  });
}

// is-form-show is-form-show-questions

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {

  var prodId = getParameterByName('return');

  if (prodId == "1") {
    modal.classList.add("is-form-show")
    modal.classList.add("is-form-show-questions")

    document.querySelector(".c-video").classList.add("u-hidden")
  } else {

    event.target.playVideo();

  }
}

function returnVideoToPause() {
  const btnReturnVideo = document.querySelector(".js-return-video")

  btnReturnVideo.addEventListener("click", e => {
    console.log(player.playVideo())
  })
}

returnVideoToPause()

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
var done = false;
function onPlayerStateChange(event) {

  const coverVideo = document.querySelector(".js-cover-video")

  if (event.data == 0) {
    modal.classList.add("is-form-show");
    setTimeout(() => {
      modal.classList.add("is-form-show-questions");
    }, 800)
    document
      .querySelector("#form-validate-session")
      .classList.remove("u-hidden");
  }

  if (event.data == 2) {
    coverVideo.classList.add("is-video-stop")

  } else if (event.data == 1) {
    coverVideo.classList.remove("is-video-stop")
  }

}

var swiper = new Swiper(".mySwiper", {
  followFinger: false,
  allowTouchMove: false,
  navigation: {
    nextEl: document.querySelector(".js-btn-next"),
    prevEl: ".swiper-button-prev",
  },
});

const btnNextForm = document.querySelector(".js-btn-next")

btnNextForm.addEventListener("click", () => {
  let contador = 0
  let contador_dos = 0


  const qOne = document.querySelectorAll('[name=q_1]');
  const qTwo = document.querySelectorAll('[name=q_2]');
  const qThree = document.querySelectorAll('[name=q_3]');
  const qFour = document.querySelectorAll('[name=q_4]');
  const qFive = document.querySelectorAll('[name=q_5]');

  const title = document.querySelector(".js-title-question");
  const msj = document.querySelector(".js-message-question");

  qOne.forEach(opcion => {
    //si la opcion recorrida esta seleccionada mostramos esa opcion
    if (opcion.checked) {
      if (opcion.value == "c") {
        contador++
      } else {
        contador--
      }

      contador_dos++

    }
  })

  qTwo.forEach(opcion => {
    //si la opcion recorrida esta seleccionada mostramos esa opcion
    if (opcion.checked) {
      if (opcion.value == "b") {
        contador++
      } else {
        contador--
      }

      contador_dos++
    }
  })

  qThree.forEach(opcion => {
    //si la opcion recorrida esta seleccionada mostramos esa opcion
    if (opcion.checked) {
      if (opcion.value == "a") {
        contador++
      } else {
        contador--
      }

      contador_dos++
    }
  })

  qFour.forEach(opcion => {
    //si la opcion recorrida esta seleccionada mostramos esa opcion
    if (opcion.checked) {
      if (opcion.value == "a") {
        contador++
      } else {
        contador--
      }

      contador_dos++
    }
  })

  qFive.forEach(opcion => {
    //si la opcion recorrida esta seleccionada mostramos esa opcion
    if (opcion.checked) {
      if (opcion.value == "a") {
        contador++
      } else {
        contador--
      }

      contador_dos++
    }
  })


  if (contador_dos >= 5) {
    title.closest(".mySwiper").classList.add("is-finally")
  }

  if (contador >= 5) {
    title.closest(".is-finally").classList.add("allgood")

    title.innerHTML = "¡Bien hecho!"
    msj.innerHTML = "Ahora puedes iniciar a disfrutar de todas las ventajas de Altruism Now Mentors, haz clic en continuar"
    document.querySelector("#form-validate-session").style.display = "block"
  }

  btnNextForm.classList.add("is-disabled")
  const slideActive = document.querySelector(".swiper-slide-active")

  if (slideActive.classList.contains("is-finally")) {
    btnNextForm.classList.add("opacity0")
    document.querySelector("#header_question").style.display = "none";
  }
})

const radios = document.querySelectorAll("input[type='radio']")

radios.forEach(element => {

  element.addEventListener("change", () => {
    btnNextForm.classList.remove("is-disabled")
  })

});

document.querySelector("#boton-volver-intentarlo").addEventListener("click", () => {
  window.location.reload()
})