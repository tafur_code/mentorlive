function showModal() {
  const btnsModal = document.querySelectorAll(".js-btn-modal");
  const listModals = document.querySelectorAll(".js-target-modal");
  const classActive = "is-modal_visible";

  btnsModal.forEach((currentBtn) => {
    currentBtn.addEventListener("click", () => {
      const datasetButton = currentBtn.dataset.buttonModal;

      listModals.forEach((modalCurrent) => {
        const datasetTarget = modalCurrent.dataset.targetModal;
        if (datasetButton == datasetTarget) {
          modalCurrent.classList.add(classActive);
        }
      });
    });
  });
}

function closeModal() {
  const classActive = "is-modal_visible";
  const btnCloseModal = document.querySelectorAll(".js-btn-modal-close");

  btnCloseModal.forEach((btnCurrent) => {
    btnCurrent.addEventListener("click", (e) => {
      parent = e.target.closest("." + classActive);

      if (parent.classList.contains(classActive)) {
        parent.classList.remove(classActive);
      }

      if (parent.querySelector(".js-user-rating")) {
        listFigure = parent.querySelector(".js-user-rating").querySelectorAll("figure")

        listFigure.forEach(element => {
          parent.querySelector(".js-user-rating").removeChild(element)
        });
      }
    });
  });
}
showModal();
closeModal();
