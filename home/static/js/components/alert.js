function setAlerts() {
  // Selector de los botones de las alertas
  const alertsBtnClose = Array.from(
    document.getElementsByClassName("js-messageClose")
  );
  // Verifica si existe al menos un botón de cerrar alerta para agregarles el evento
  if (alertsBtnClose[0]) {
    console.log(alertsBtnClose[0]);
    // Agrega evento de cerrar al botón cerrar (X) de las alertas
    alertsBtnClose.forEach((x) => {
      const alertElement = x.closest(".js-messagesItem");
      const waitingTimeToDisplay = 1000;
      const leftTimeToDisplay = 15000;
      // Muestra la alerta 1 segundo (valor guardado en la variable waitingTimeToDisplay) después de cargar la pagina
      setTimeout(() => {
        alertElement.classList.remove("is-messageClose");
        alertElement.classList.add("has-messageOpen");

        // Después de un tiempo se retira de escena el mensaje
        setTimeout(() => {
          alertElement.classList.add("is-messageClose");
          alertElement.classList.remove("has-messageOpen");
        }, leftTimeToDisplay);
      }, waitingTimeToDisplay);

      // Al hacer click en el botón cerrar agrega la clase is-messageClose la cual contiene las animaciones y los estilos para cerrar la alerta
      x.addEventListener("click", function () {
        alertElement.classList.add("has-messageClose");
      });
    });
  }
}

setAlerts();
