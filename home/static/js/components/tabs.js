function openCity(evt, cityName) { // Declare all variables
    var i,
        tabcontent,
        tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("js-content");

    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("c-tabs__item");
    for (i = 0; i < tabcontent.length; i++) {
        tablinks[i].classList.remove("is-tab-active");
    }

    // Show the current tab, and add an "is-tab-active" class to the link that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.classList.add("is-tab-active");
}


class Tabs {
    constructor() {
        this.btns = document.querySelectorAll(".js-tab-btn");
        this.contents = document.querySelectorAll(".js-tab-content");
        this.classActive = "active"
        this.classToHideElemet = "u-hidden"

        this.init()
    }

    init() {
        this.showTab()
    }

    showTab() {
        this.btns.forEach(btn => {
            btn.addEventListener("click", () => {
                const dataBtn = btn.dataset.tabButton;

                this.btns.forEach(validateBtn => {
                    if (validateBtn.classList.contains(this.classActive)) {
                        validateBtn.classList.remove(this.classActive)
                    }

                    this.contents.forEach(content => {
                        const dataContent = content.dataset.tabContent;

                        if (dataContent == dataBtn) {
                            content.classList.remove(this.classToHideElemet)
                        } else {
                            content.classList.add(this.classToHideElemet)
                        }

                    });
                });

                btn.classList.add(this.classActive);

            })
        });
    }
}

new Tabs()
