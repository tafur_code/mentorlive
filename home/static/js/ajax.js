function abrir_modal_edicion(url) {
  $('#edicion').load(url, function () {
    $(this).parent().parent().parent().addClass("is-modal_visible")
  });
}

function abrir_modal_edicion_suggestion(url) {
  $('#edicion_suggestion').load(url, function () {

    $(this).parent().parent().parent().addClass("is-modal_visible")

    const loaders = document.querySelectorAll(".loader")
    loaders.forEach(element => {
      element.classList.add("u-hidden")
    });

  });
}

function abrir_modal_edicion_suggestion_cancel(url) {
  $('#edicion_suggestion_cancel').load(url, function () {
    console.log("CARGOOO")
    console.log(url)
    $(this).parent().parent().parent().addClass("is-modal_visible")

    const loaders = document.querySelectorAll(".loader")
    loaders.forEach(element => {
      element.classList.add("u-hidden")
    });

  });
}

function onClickChangeStateSuggestion() {
  const btnSuggesEdit = document.querySelectorAll(".js-btn-sugges-edit")

  btnSuggesEdit.forEach(element => {
    element.addEventListener("click", e => {
      e.target.querySelector(".loader").classList.remove("u-hidden")
    })
  });
}

onClickChangeStateSuggestion()

function abrir_modal_rating(url, event) {
  $('#rating').load(url, function () {
    $(this).parent().parent().parent().addClass("is-modal_visible")

    idUserCurrent = event.target.closest(".c-card-notify").querySelector(".js-id-user-rating")
    idUserCurrentNum = event.target.closest(".c-card-notify").querySelector(".js-id-user-rating-num")

    if (idUserCurrent.innerHTML == "mentor") {
      data = event.target.closest(".c-card-notify").querySelector(".c-card-notify__bottom")
      dataImage = data.querySelector(".c-card-notify__figure")
      clonImage = dataImage.cloneNode(true)
      clonName = clonImage.querySelector("img").getAttribute("alt").replace("perfil de ", "")
    }
    else {
      data = event.target.closest(".c-card-notify").querySelector(".c-card-notify__top")
      dataImage = data.querySelector(".c-card-notify__figure")
      clonImage = dataImage.cloneNode(true)
      clonName = clonImage.querySelector("img").getAttribute("alt").replace("perfil de ", "")
    }

    dataModal = this.closest(".c-modal__content").querySelector(".js-user-rating")
    dataModalName = this.closest(".c-modal__content").querySelector(".js-user-rating-name")
    dataModalNameTitle = this.closest(".c-modal__content").querySelector(".js-user-rating-name-title")
    inputIdUserRating = this.closest(".c-modal__content").querySelector("#id-rating-user")

    changeValueRating(this.closest(".c-modal__content"))

    dataModal.appendChild(clonImage)
    dataModalName.innerHTML = clonName
    dataModalNameTitle.innerHTML = clonName
    inputIdUserRating.value = idUserCurrentNum.innerHTML
  });
}

function changeValueRating(inputRating) {
  if (inputRating) {
    starRating = document.querySelectorAll("[name='estrellas']")
    inputRatingInput = inputRating.querySelector("[name='rating_user']")

    starRating.forEach(element => {

      element.addEventListener("click", e => {
        inputRatingInput.value = e.target.value
      })
    });
  }
}

changeValueRating()

function editar() {
  $.ajax({
    data: $('#form_edicion').serialize(),
    url: $('#form_edicion').attr('action'),
    type: $('#form_edicion').attr('method'),

    success: function (response) {
      showNotification(response.mensaje);
    },
    error: function (error) {
      showNotification(error.responseJSON.mensaje);
    }
  });
}

function editarSuggestion() {
  const idFormEdicionSuge = document.querySelector('#form_edicion_suggestion').querySelector('.loader')
  idFormEdicionSuge.classList.remove("u-hidden")

  $.ajax({
    data: $('#form_edicion_suggestion').serialize(),
    url: $('#form_edicion_suggestion').attr('action'),
    type: $('#form_edicion_suggestion').attr('method'),

    success: function (response) {
      showNotificationSugges(response.mensaje);

      const idFormEdicionSuge = document.querySelector('#form_edicion_suggestion').querySelector('.loader')
      console.log(idFormEdicionSuge)
      idFormEdicionSuge.classList.add("u-hidden")
    },
    error: function (error) {
      showNotificationSugges(error.responseJSON.mensaje);

      const btn = document.querySelectorAll(".js-btnsuggestion-editin")

      btn.forEach(element => {
        element.addEventListener("click", () => {
          element.querySelector(".loader").classList.add("u-hidden")
        })
      });
    }
  });
}

function editarSuggestionDescartar() {
  const idFormEdicionSuge = document.querySelector('#form_edicion_suggestion_cancel').querySelector('.loader')
  idFormEdicionSuge.classList.remove("u-hidden")

  $.ajax({
    data: $('#form_edicion_suggestion_cancel').serialize(),
    url: $('#form_edicion_suggestion_cancel').attr('action'),
    type: $('#form_edicion_suggestion_cancel').attr('method'),

    success: function (response) {
      showNotificationSugges(response.mensaje);

      const idFormEdicionSuge = document.querySelector('#form_edicion_suggestion_cancel').querySelector('.loader')
      console.log(idFormEdicionSuge)
      idFormEdicionSuge.classList.add("u-hidden")
    },
    error: function (error) {
      showNotificationSugges(error.responseJSON.mensaje);

      const btn = document.querySelectorAll(".js-btnsuggestion-editin")

      btn.forEach(element => {
        element.addEventListener("click", () => {
          element.querySelector(".loader").classList.add("u-hidden")
        })
      });
    }
  });
}

function crear() {
  $.ajax({
    data: $('#form_rating').serialize(),
    url: $('#form_rating').attr('action'),
    type: $('#form_rating').attr('method'),

    success: function (response) {
      showNotification(response.mensaje);
    },
    error: function (error) {
      showNotification(error.responseJSON.mensaje);
    }
  });
}


function showNotification(msj) {
  $(".js-error-message").removeClass("u-hidden")
  $(".js-error-message").html(msj)

  setInterval(() => {
    $(".js-error-message").addClass("u-hidden")
  }, 4000)

  setInterval(() => {
    location.reload();
  }, 5000)
}

function showNotificationSugges(msj) {

  $(".js-error-message").removeClass("u-hidden")
  $(".js-error-message").html(msj)

  setInterval(() => {
    $(".js-error-message").addClass("u-hidden")
  }, 2000)

  window.location.reload();

}
