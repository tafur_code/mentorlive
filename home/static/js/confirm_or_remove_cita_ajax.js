class ConfirmOrRemoveCitaAjax {
  constructor() {
    this.spanToRemove = document.querySelector(".js-get-data-for-mentoria-active")
    this.spanToFinished = document.querySelector(".js-get-data-for-mentoria-finished")

    if (this.spanToRemove) {
      this.isExistiMentoriaToRemove = this.spanToRemove.dataset.countCita
      this.idMentoriaToRemove = this.spanToRemove.dataset.idMentoriaActive
      this.url = `/cancelar-cita/${this.idMentoriaToRemove}`
    }

    if (this.spanToFinished) {
      this.isExistiMentoriaToFinished = this.spanToFinished.dataset.countCitaFinished
      this.idMentoriaToFinished = this.spanToFinished.dataset.idMentoriaFinished
      this.url_finish = `/terminar-cita/${this.idMentoriaToFinished}`
    }

    this.init()
  }

  init() {
    this.abrir_modal_edicion_suggestion_base()
    this.abrir_modal_edicion_suggestion_base_finish()
    this.interval()
  }

  abrir_modal_edicion_suggestion_base() {
    if (this.isExistiMentoriaToRemove == "True") {
      $('#edicion_suggestion_base').load(this.url, function () {
      });
    }
  }

  abrir_modal_edicion_suggestion_base_finish() {
    if (this.isExistiMentoriaToFinished == "True") {
      $('#edicion_suggestion_base').load(this.url_finish, function () {
        console.log("FORM CARGADOA PARA FINIALIZAR")
      });
    }
  }

  interval() {
    const intervalMine = setInterval(() => {
      let ctaForm = document.querySelector("#yes_want_base")

      if (ctaForm) {
        clearInterval(intervalMine)
        ctaForm.click()
      }
    }, 1000)
  }


}

new ConfirmOrRemoveCitaAjax()

function editar_base() {
  $.ajax({
    data: $('#form_edicion_base').serialize(),
    url: $('#form_edicion_base').attr('action'),
    type: $('#form_edicion_base').attr('method'),

    success: function (response) {
      // showNotification(response.mensaje);
      console.log(response.mensaje)
    },
    error: function (error) {
      console.log(error)
      // showNotification(error.responseJSON.mensaje);
    }
  });
}
