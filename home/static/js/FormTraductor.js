
const listForOfNames = document.querySelectorAll("label")
const lang = document.querySelector("html").getAttribute("lang")
const opinion = document.querySelectorAll("#opinion");

if (lang == "en") {
    opinion.forEach(element => {
        element.innerHTML = "opinions"
    });
}

listForOfNames.forEach(element => {
    switch (element.getAttribute("for")) {
        case "id_names":
            changeText(element, "Names", "")
            break;
        case "id_name_contact":
            changeText(element, "Names", "")
            break;
        case "id_phone_contact":
            changeText(element, "Telephone", "")
            break;
        case "id_lastnames":
            changeText(element, "Lastnames", "")
            break;
        case "id_email":
            changeText(element, "Email", "")
            break;
        case "id_email_contact":
            changeText(element, "Email", "")
            break;
        case "id_avatar_image":
            changeText(element, "Avatar image", "")
            break;
        case "id_type_question":
            changeText(element, "Question type", "")
            break;
        case "id_message_contact":
            changeText(element, "Contact message", "")
            break;
        case "password1":
            changeText(element, "Password", "Password")
            break;
        case "password2":
            changeText(element, "Password Confirm", "Password Confirm")
            break;
        case "id_prefered_language":
            changeText(element, "Preferred language:", "")
            break;
        case "id_speak_language":
            changeText(element, "Spoken languages:", "")
            break;
        case "id_country":
            changeText(element, "Country:", "")
            break;
        case "id_department":
            changeText(element, "State / Department:", "")
            break;
        case "id_city":
            changeText(element, "City:", "")
            break;
        case "id_number_phone":
            changeText(element, "Number phone:", "")
            break;
        case "id_birt_year":
            changeText(element, "Year of birth:", "")
            break;
        case "id_gender":
            changeText(element, "Gender:", "")
            break;
        case "id_etapa_negocio":
            changeText(element, "What stage of development is your business at?", "")
            break;
        case "id_objetivos_retos_cumplidos":
            changeText(element, "Objectives or challenges that have already fulfilled?", "")
            break;
        case "id_tipo_negocio_en_mente":
            changeText(element, "What idea or type of business do you have in mind?", "")
            break;
        case "id_porque_con_mentor":
            changeText(element, "Why do you want to work with a mentor?", "")
            break;
        case "id_nombre_negocio":
            changeText(element, "What is your business called?", "")
            break;
        case "id_metas_futuras":
            changeText(element, "What are your future goals?", "")
            break;
        case "id_fecha_lanzamiento":
            changeText(element, "Launch date of your business?", "")
            break;
        case "id_ocupacion":
            changeText(element, "What is your occupation?", "")
            break;
        case "id_industria_negocio":
            changeText(element, "What industry does your business belong to?", "")
            break;
        case "id_descripcion_negocio":
            changeText(element, "Write a brief description of your business so that mentors know about it", "")
            break;
        case "id_profesion":
            changeText(element, "What is your profession?", "")
            break;
        case "id_retos_comom_emprendedor":
            changeText(element, "What are your challenges as an entrepreneur?", "")
            break;
        case "id_ayudas_para_negocio":
            changeText(element, "What stage of development is your business at?", "Select an option")
            break;
        case "id_ubicacion_negocio":
            changeText(element, "Where is your business located?", "Filter by")
            break;
        case "id_url_sitio_web":
            changeText(element, "URL or website of your business?", "")
            break;
        case "id_mentor":
            changeText(element, "Mentor contact email:", "")
            break;
        case "id_emprendedor":
            changeText(element, "Entrepreneur contact email:", "")
            break;
        case "id_fecha_estimada_reunion":
            changeText(element, "Estimated Mentoring Date:", "")
            break;
        case "id_pais_solicitud":
            changeText(element, "Country of request :", "")
            break;
        case "id_medio_respuesta":
            changeText(element, "Means by which you will receive the response to this Mentoring request:", "")
            break;
        case "tipo_reunion_mentor":
            changeText(element, "How would you like to meet with your mentor:", "")
            break;
        case "tipo_reunion_emprendedor":
            changeText(element, "How would you like to meet with your entrepreneur:", "")
            break;
        case "id_description_text":
            changeText(element, "Reason for meeting:", "")
            break;
        case "id_description_rating":
            changeText(element, "Share details of your experience with the entrepreneur:", "")
            break;
        case "id_dias_disponibilidad":
            changeText(element, "What days do you have available for mentoring?", "")
            break;
        case "id_descripcion_preparacion_emprendedor":
            changeText(element, "How should the entrepreneur prepare before starting with your mentoring?", "")
            break;
        case "id_tipo_empresa":
            changeText(element, "What type of company do you have?", "")
            break;
        case "id_mentoria_exitosa":
            changeText(element, "What do you expect from a successful mentoring relationship?", "")
            break;
        case "id_como_ayudar_emprendedores":
            changeText(element, "How can you help entrepreneurs?", "")
            break;
        case "id_como_ayudar_emprendedores":
            changeText(element, "How can you help entrepreneurs?", "")
            break;
        case "id_cargo_empresa":
            changeText(element, "What is your position in the company?", "")
            break;
        case "id_descripcion_experiencia_laboral":
            changeText(element, "Describe your work experience:", "")
            break;
        case "id_nombre_empresa":
            changeText(element, "What's the name of your company?", "")
            break;
        case "id_años_experiencia_dueño":
            changeText(element, "How many years of experience do you have as a business owner?", "")
            break;
        case "id_años_experiencia":
            changeText(element, "How many years of experience do you have in business areas? We suggest 2 or more years.", "")
            break;
        case "id_etapa_empresa":
            changeText(element, "At what stage of development is your company?", "")
            break;
    }
});

function changeText(element, text, placeholder) {

    if (lang == "en") {
        element.innerHTML = text

        if (element.closest(".o-form__label").nextElementSibling.querySelector("input")) {
            placeholderSibling = element.closest(".o-form__label").nextElementSibling.querySelector("input").getAttribute("placeholder")

            if (placeholderSibling) {
                element.closest(".o-form__label").nextElementSibling.querySelector("input").setAttribute("placeholder", placeholder)
            }
        }

    }
}


if (lang == "en") {
    var optionsEtapaEmpresa = () => {
        if (document.querySelector("#id_etapa_empresa")) {
            const optionsList = document.querySelector("#id_etapa_empresa").querySelectorAll("option")


            optionsList.forEach(optionEtapaEmpresa => {
                switch (optionEtapaEmpresa.getAttribute("value")) {
                    case "":
                        optionEtapaEmpresa.innerHTML = "Select an option"
                        break;

                    case "01":
                        optionEtapaEmpresa.innerHTML = "I have a business idea / It is not yet operational and I don't have a prototype or customers"
                        break;

                    case "02":
                        optionEtapaEmpresa.innerHTML = "My business is up and running but I have not yet generated income / Income is the result of sell a product or a service"
                        break;

                    case "03":
                        optionEtapaEmpresa.innerHTML = "I already have customers and income but my business is not yet lucrative / A business is not lucrative when income is less than the estimated profit."
                        break;

                    case "04":
                        optionEtapaEmpresa.innerHTML = "My business is operating at large scale and I have utilities. / Scaling means your business is able to handle discounts and volume purchases. a business is lucrative when the income generated are older than bills."
                        break;
                }

            });
        }
    }

    var optionsEtapaNegocio = () => {
        if (document.querySelector("#id_etapa_negocio")) {
            const optionsList = document.querySelector("#id_etapa_negocio").querySelectorAll("option")


            optionsList.forEach(optionEtapaEmpresa => {
                switch (optionEtapaEmpresa.getAttribute("value")) {
                    case "":
                        optionEtapaEmpresa.innerHTML = "Select an option"
                        break;

                    case "01":
                        optionEtapaEmpresa.innerHTML = "I have a business idea / It is not yet operational and I don't have a prototype or customers"
                        break;

                    case "02":
                        optionEtapaEmpresa.innerHTML = "My business is up and running but I have not yet generated income / Income is the result of sell a product or a service"
                        break;

                    case "03":
                        optionEtapaEmpresa.innerHTML = "I already have customers and income but my business is not yet lucrative / A business is not lucrative when income is less than the estimated profit."
                        break;

                    case "04":
                        optionEtapaEmpresa.innerHTML = "My business is operating at large scale and I have utilities. / Scaling means your business is able to handle discounts and volume purchases. a business is lucrative when the income generated are older than bills."
                        break;
                }

            });
        }
    }

    var diasDisponibilidad = () => {
        if (document.querySelector("#id_dias_disponibilidad")) {
            const optionsList = document.querySelector("#id_dias_disponibilidad").querySelectorAll("option")

            optionsList.forEach(optionEtapaEmpresa => {
                switch (optionEtapaEmpresa.getAttribute("value")) {
                    case "":
                        optionEtapaEmpresa.innerHTML = "Select an option"
                        break;

                    case "Lunes":
                        optionEtapaEmpresa.innerHTML = "Monday"
                        break;

                    case "Martes":
                        optionEtapaEmpresa.innerHTML = "Tuesday"
                        break;

                    case "Miercoles":
                        optionEtapaEmpresa.innerHTML = "Wednesday"
                        break;

                    case "Jueves":
                        optionEtapaEmpresa.innerHTML = "Thursday"
                        break;

                    case "Viernes":
                        optionEtapaEmpresa.innerHTML = "Friday"
                        break;

                    case "Sabado":
                        optionEtapaEmpresa.innerHTML = "Saturday"
                        break;

                    case "Domingo":
                        optionEtapaEmpresa.innerHTML = "Sunday"
                        break;
                }

            });
        }
    }

    var gender = () => {
        if (document.querySelector("#id_gender")) {
            const optionsList = document.querySelector("#id_gender").querySelectorAll("option")

            optionsList.forEach(optionEtapaEmpresa => {
                switch (optionEtapaEmpresa.getAttribute("value")) {
                    case "":
                        optionEtapaEmpresa.innerHTML = "Select a gender"
                        break;

                    case "Masculino":
                        optionEtapaEmpresa.innerHTML = "Male"
                        break;

                    case "Femenino":
                        optionEtapaEmpresa.innerHTML = "Female"
                        break;
                }

            });
        }
    }

    var id_prefered_language = () => {
        if (document.querySelector("#id_prefered_language")) {
            const optionsList = document.querySelector("#id_prefered_language").querySelectorAll("option")

            optionsList.forEach(optionEtapaEmpresa => {
                switch (optionEtapaEmpresa.getAttribute("value")) {

                    case "Español":
                        optionEtapaEmpresa.innerHTML = "Spanish"
                        break;

                    case "Ingles":
                        optionEtapaEmpresa.innerHTML = "English"
                        break;
                }

            });
        }
    }

    var questionType = () => {
        if (document.querySelector("#id_type_question")) {
            const optionsList = document.querySelector("#id_type_question").querySelectorAll("option")

            optionsList.forEach(optionEtapaEmpresa => {
                switch (optionEtapaEmpresa.getAttribute("value")) {
                    case "pregunta":
                        optionEtapaEmpresa.innerHTML = "Question"
                        break;

                    case "inquietud":
                        optionEtapaEmpresa.innerHTML = "Concern"
                        break;

                    case "reclamo":
                        optionEtapaEmpresa.innerHTML = "Claim"
                        break;
                }

            });
        }
    }

    var tipoEmpresa = () => {
        if (document.querySelector("#id_tipo_empresa")) {
            const optionsList = document.querySelector("#id_tipo_empresa").querySelectorAll("option")

            optionsList.forEach(optionEtapaEmpresa => {
                switch (optionEtapaEmpresa.getAttribute("value")) {
                    case "":
                        optionEtapaEmpresa.innerHTML = "Select an option"
                        break;

                    case "lucro":
                        optionEtapaEmpresa.innerHTML = "For-profit"
                        break;

                    case "sinlucro":
                        optionEtapaEmpresa.innerHTML = "Non-profit"
                        break;

                    case "empresa_social":
                        optionEtapaEmpresa.innerHTML = "Social enterprise"
                        break;

                    case "no_se":
                        optionEtapaEmpresa.innerHTML = "I'm not sure"
                        break;
                }

            });
        }
    }

    var tipoNegocio = () => {
        if (document.querySelector("#id_tipo_negocio_en_mente")) {
            const optionsList = document.querySelector("#id_tipo_negocio_en_mente").querySelectorAll("option")

            optionsList.forEach(optionEtapaEmpresa => {
                switch (optionEtapaEmpresa.getAttribute("value")) {
                    case "":
                        optionEtapaEmpresa.innerHTML = "Select an option"
                        break;

                    case "lucro":
                        optionEtapaEmpresa.innerHTML = "For-profit"
                        break;

                    case "sinlucro":
                        optionEtapaEmpresa.innerHTML = "Non-profit"
                        break;

                    case "empresa_social":
                        optionEtapaEmpresa.innerHTML = "Social enterprise"
                        break;

                    case "no_se":
                        optionEtapaEmpresa.innerHTML = "I'm not sure"
                        break;
                }

            });
        }
    }

    var industriaNegocio = () => {
        if (document.querySelector("#id_industria_negocio")) {
            const optionsList = document.querySelector("#id_industria_negocio").querySelectorAll("option")

            optionsList.forEach(optionEtapaEmpresa => {
                switch (optionEtapaEmpresa.getAttribute("value")) {
                    case "":
                        optionEtapaEmpresa.innerHTML = "Select an option"
                        break;
                    case "01":
                        optionEtapaEmpresa.innerHTML = "Senior Care / Home Health Care"
                        break;
                    case "02":
                        optionEtapaEmpresa.innerHTML = "Accounting and tax services"
                        break;
                    case "03":
                        optionEtapaEmpresa.innerHTML = "Agriculture / Farm / Ranch"
                        break;
                    case "04":
                        optionEtapaEmpresa.innerHTML = "Animals / Pets"
                        break;
                    case "05":
                        optionEtapaEmpresa.innerHTML = "Architecture / Interior Design"
                        break;
                    case "06":
                        optionEtapaEmpresa.innerHTML = "Crafts / Crafts"
                        break;
                    case "07":
                        optionEtapaEmpresa.innerHTML = "Arts: Music / Plastic / Scenic"
                        break;
                    case "08":
                        optionEtapaEmpresa.innerHTML = "Refractions / Mechanic"
                        break;
                    case "09":
                        optionEtapaEmpresa.innerHTML = "Beauty / Hair care / Cosmetics"
                        break;
                    case "10":
                        optionEtapaEmpresa.innerHTML = "Bookstores / Newspaper and magazine stalls"
                        break;
                    case "11":
                        optionEtapaEmpresa.innerHTML = "Business Consulting / Coaching"
                        break;
                    case "12":
                        optionEtapaEmpresa.innerHTML = "Childcare"
                        break;
                    case "13":
                        optionEtapaEmpresa.innerHTML = "Computer Services / Information Technology"
                        break;
                    case "14":
                        optionEtapaEmpresa.innerHTML = "Construction / Site staff / Repairs"
                        break;
                    case "15":
                        optionEtapaEmpresa.innerHTML = "Counseling / Therapy / Mental Health"
                        break;
                    case "16":
                        optionEtapaEmpresa.innerHTML = "Digital marketing / Electronic commerce / Social networks"
                        break;
                    case "17":
                        optionEtapaEmpresa.innerHTML = "Services for people with disabilities"
                        break;
                    case "18":
                        optionEtapaEmpresa.innerHTML = "Distribution and Transportation"
                        break;
                    case "19":
                        optionEtapaEmpresa.innerHTML = "Education / Training"
                        break;
                    case "20":
                        optionEtapaEmpresa.innerHTML = "Entertainment / Recreation / Events"
                        break;
                    case "21":
                        optionEtapaEmpresa.innerHTML = "Export / Import"
                        break;
                    case "22":
                        optionEtapaEmpresa.innerHTML = "Electronic Commerce / Online Sales"
                        break;
                    case "23":
                        optionEtapaEmpresa.innerHTML = "Fashion / Wedding / Accessories"
                        break;
                    case "24":
                        optionEtapaEmpresa.innerHTML = "Financial Services / Insurance"
                        break;
                    case "25":
                        optionEtapaEmpresa.innerHTML = "Florist / Gifts"
                        break;
                    case "26":
                        optionEtapaEmpresa.innerHTML = "Food Products / Groceries"
                        break;
                    case "27":
                        optionEtapaEmpresa.innerHTML = "Forestry / Wood Products"
                        break;
                    case "28":
                        optionEtapaEmpresa.innerHTML = "Furniture manufacturing / Repair / Sale"
                        break;
                    case "29":
                        optionEtapaEmpresa.innerHTML = "Graphic Design / Website Development"
                        break;
                    case "30":
                        optionEtapaEmpresa.innerHTML = "Renewable energy"
                        break;
                    case "31":
                        optionEtapaEmpresa.innerHTML = "Health / Wellness / Physical Activity / Exercise"
                        break;
                    case "32":
                        optionEtapaEmpresa.innerHTML = "Commercial and domestic cleaning services"
                        break;
                    case "33":
                        optionEtapaEmpresa.innerHTML = "Jewelry / Luxury Items"
                        break;
                    case "34":
                        optionEtapaEmpresa.innerHTML = "Gardening"
                        break;
                    case "35":
                        optionEtapaEmpresa.innerHTML = "Laundry / Dry Cleaning"
                        break;
                    case "36":
                        optionEtapaEmpresa.innerHTML = "Legal services"
                        break;
                    case "37":
                        optionEtapaEmpresa.innerHTML = "Manufacture"
                        break;
                    case "38":
                        optionEtapaEmpresa.innerHTML = "Marketing / Advertising"
                        break;
                    case "39":
                        optionEtapaEmpresa.innerHTML = "Media / Publishing"
                        break;
                    case "40":
                        optionEtapaEmpresa.innerHTML = "Non-Profit / Social Enterprise"
                        break;
                    case "41":
                        optionEtapaEmpresa.innerHTML = "Multimedia / Social networks"
                        break;
                    case "42":
                        optionEtapaEmpresa.innerHTML = "Executive Personal Assistance"
                        break;
                    case "43":
                        optionEtapaEmpresa.innerHTML = "Photography / Sound / Video Services"
                        break;
                    case "44":
                        optionEtapaEmpresa.innerHTML = "Real estate"
                        break;
                    case "45":
                        optionEtapaEmpresa.innerHTML = "Recruitment / Hiring / Personal Services"
                        break;
                    case "46":
                        optionEtapaEmpresa.innerHTML = "Restaurant / Cafe / Bar / Catering"
                        break;
                    case "47":
                        optionEtapaEmpresa.innerHTML = "Sale to the public"
                        break;
                    case "48":
                        optionEtapaEmpresa.innerHTML = "Sporting Goods/Recreation/Outdoor Activities"
                        break;
                    case "49":
                        optionEtapaEmpresa.innerHTML = "Vehicle driving"
                        break;
                    case "50":
                        optionEtapaEmpresa.innerHTML = "Translation / Guide"
                        break;
                    case "51":
                        optionEtapaEmpresa.innerHTML = "Travel / Tourism / Hospitality"
                        break;
                    case "52":
                        optionEtapaEmpresa.innerHTML = "Vet"
                        break;
                    case "53":
                        optionEtapaEmpresa.innerHTML = "Wines / Spirits"
                        break;
                    case "54":
                        optionEtapaEmpresa.innerHTML = "Writing / Editing"
                        break;
                }

            });
        }
    }

    var ayudasNegocio = () => {
        if (document.querySelector("#id_ayudas_para_negocio")) {
            const optionsList = document.querySelector("#id_ayudas_para_negocio").querySelectorAll("option")

            optionsList.forEach(optionEtapaEmpresa => {
                switch (optionEtapaEmpresa.getAttribute("value")) {
                    case "":
                        optionEtapaEmpresa.innerHTML = "Select an option"
                        break;

                    case "Contabilidad":
                        optionEtapaEmpresa.innerHTML = "Accounting"
                        break;

                    case "Auditorias":
                        optionEtapaEmpresa.innerHTML = "Audits"
                        break;

                    case "Ingresos / Egresos":
                        optionEtapaEmpresa.innerHTML = "Income / Expenses"
                        break;

                    case "Presupuesto":
                        optionEtapaEmpresa.innerHTML = "Budget"
                        break;

                    case "Flujo de Efectivo":
                        optionEtapaEmpresa.innerHTML = "Cash Flow"
                        break;

                    case "Planeación Financiera":
                        optionEtapaEmpresa.innerHTML = "Financial planning"
                        break;

                    case "Préstamos y Financiamiento":
                        optionEtapaEmpresa.innerHTML = "Loans and Financing"
                        break;

                    case "Otro tipo de Contabilidad":
                        optionEtapaEmpresa.innerHTML = "Other type of Accounting"
                        break;

                    case "Impuestos":
                        optionEtapaEmpresa.innerHTML = "Taxes"
                        break;

                    case "Compensaciones y Prestaciones":
                        optionEtapaEmpresa.innerHTML = "Compensation and Benefits"
                        break;

                    case "Contratistas y Consultores":
                        optionEtapaEmpresa.innerHTML = "Contractors and Consultants"
                        break;

                    case "Capacitación de personal":
                        optionEtapaEmpresa.innerHTML = "Staff training"
                        break;

                    case "Otra forma de recursos humanos":
                        optionEtapaEmpresa.innerHTML = "Other form of human resources"
                        break;

                    case "Políticas del personal":
                        optionEtapaEmpresa.innerHTML = "Personnel policies"
                        break;

                    case "Reclutamiento y Contratación":
                        optionEtapaEmpresa.innerHTML = "Recruitment and Hiring"
                        break;

                    case "Gestión de voluntarios":
                        optionEtapaEmpresa.innerHTML = "Volunteer management"
                        break;

                    case "Aduanas y Aranceles":
                        optionEtapaEmpresa.innerHTML = "Customs and Tariffs"
                        break;

                    case "Exportación e Importación":
                        optionEtapaEmpresa.innerHTML = "Exportation and importation"
                        break;

                    case "Mercados globales":
                        optionEtapaEmpresa.innerHTML = "Global markets"
                        break;

                    case "Otro sector internacional":
                        optionEtapaEmpresa.innerHTML = "Other international sector"
                        break;

                    case "Outsourcing":
                        optionEtapaEmpresa.innerHTML = "Outsourcing"
                        break;

                    case "Contratos":
                        optionEtapaEmpresa.innerHTML = "Contracts"
                        break;

                    case "Derecho laboral":
                        optionEtapaEmpresa.innerHTML = "Labor law"
                        break;

                    case "Propiedad intelectual":
                        optionEtapaEmpresa.innerHTML = "Intellectual property"
                        break;

                    case "Otra área legal":
                        optionEtapaEmpresa.innerHTML = "Other legal area"
                        break;

                    case "Derecho de la propiedad":
                        optionEtapaEmpresa.innerHTML = "Property law"
                        break;

                    case "Derecho fiscal":
                        optionEtapaEmpresa.innerHTML = "Tax law"
                        break;

                    case "Establecimiento de consejos directivos":
                        optionEtapaEmpresa.innerHTML = "Establishment of boards of directors"
                        break;

                    case "Seguros comerciales":
                        optionEtapaEmpresa.innerHTML = "Commercial insurance"
                        break;

                    case "Estrategias de negocios":
                        optionEtapaEmpresa.innerHTML = "Business strategies"
                        break;

                    case "Reacudación de fondos":
                        optionEtapaEmpresa.innerHTML = "Fundraising"
                        break;

                    case "Crecimiento y Desarrollo Profesional":
                        optionEtapaEmpresa.innerHTML = "Professional Growth and Development"
                        break;

                    case "Liderazgo":
                        optionEtapaEmpresa.innerHTML = "Leadership"
                        break;

                    case "Otras áreas administrativas":
                        optionEtapaEmpresa.innerHTML = "Other administrative areas"
                        break;

                    case "Planeamiento y Programación por metas":
                        optionEtapaEmpresa.innerHTML = "Planning and Programming by goals"
                        break;

                    case "Administración de proyectos":
                        optionEtapaEmpresa.innerHTML = "Project management"
                        break;

                    case "Equilibrio trabajo - vida personal":
                        optionEtapaEmpresa.innerHTML = "Balance work - personal life"
                        break;

                    case "Publicidad y Promoción":
                        optionEtapaEmpresa.innerHTML = "Advertising and Promotional"
                        break;

                    case "Branding e Identidad Corporativa":
                        optionEtapaEmpresa.innerHTML = "Branding and Corporate Identity"
                        break;

                    case "Desarrollo de negocios":
                        optionEtapaEmpresa.innerHTML = "Business development"
                        break;

                    case "Distribución":
                        optionEtapaEmpresa.innerHTML = "Distribution"
                        break;

                    case "Investigación de mercados":
                        optionEtapaEmpresa.innerHTML = "Market research"
                        break;

                    case "Materiales de mercadotecnia":
                        optionEtapaEmpresa.innerHTML = "marketing materials"
                        break;

                    case "Estrategias de mercadotecnia":
                        optionEtapaEmpresa.innerHTML = "Marketing strategies"
                        break;

                    case "Otra forma de mercadotecnia":
                        optionEtapaEmpresa.innerHTML = "Another form of marketing"
                        break;

                    case "Fijación de precios":
                        optionEtapaEmpresa.innerHTML = "Price fixing"
                        break;

                    case "Desarrollo de productos":
                        optionEtapaEmpresa.innerHTML = "Product development"
                        break;

                    case "Relaciones públicas y Medios":
                        optionEtapaEmpresa.innerHTML = "Public Relations and Media"
                        break;

                    case "Redes sociales":
                        optionEtapaEmpresa.innerHTML = "Social networks"
                        break;

                    case "Marketing en línea":
                        optionEtapaEmpresa.innerHTML = "Online marketing"
                        break;

                    case "Redacción y Edición":
                        optionEtapaEmpresa.innerHTML = "Writing and Editing"
                        break;

                    case "Manejo de inventarios":
                        optionEtapaEmpresa.innerHTML = "Inventory management"
                        break;

                    case "Logística":
                        optionEtapaEmpresa.innerHTML = "Logistics"
                        break;

                    case "Manufactura":
                        optionEtapaEmpresa.innerHTML = "Manufacture"
                        break;

                    case "Otras operaciones":
                        optionEtapaEmpresa.innerHTML = "Other operations"
                        break;

                    case "Embalaje y etiquetado":
                        optionEtapaEmpresa.innerHTML = "Packaging and labeling"
                        break;

                    case "Mejora de procesos y optimización":
                        optionEtapaEmpresa.innerHTML = "Process improvement and optimization"
                        break;

                    case "Adquisiciones y Proveedores":
                        optionEtapaEmpresa.innerHTML = "Acquisitions and Suppliers"
                        break;

                    case "Diseño de programas y evaluación":
                        optionEtapaEmpresa.innerHTML = "Program design and evaluation"
                        break;

                    case "Gestión de calidad":
                        optionEtapaEmpresa.innerHTML = "Quality management"
                        break;

                    case "Transporte y Entrega":
                        optionEtapaEmpresa.innerHTML = "Transportation and Delivery"
                        break;

                    case "Servicio / Atención al cliente y Gestion de relaciones comerciales":
                        optionEtapaEmpresa.innerHTML = "Service / Customer Service and Business Relationship Management"
                        break;

                    case "Contratos gubernamentales":
                        optionEtapaEmpresa.innerHTML = "Government contracts"
                        break;

                    case "Generacion de contactos":
                        optionEtapaEmpresa.innerHTML = "Generation of contacts"
                        break;

                    case "Otras áreas de ventas":
                        optionEtapaEmpresa.innerHTML = "Other sales areas"
                        break;

                    case "Venta al público":
                        optionEtapaEmpresa.innerHTML = "Sale to the public"
                        break;

                    case "Venta de servicios":
                        optionEtapaEmpresa.innerHTML = "Sale of services"
                        break;

                    case "Venta al por mayor B2B":
                        optionEtapaEmpresa.innerHTML = "B2B wholesale"
                        break;

                    case "Planificación empresarial":
                        optionEtapaEmpresa.innerHTML = "Business planning"
                        break;

                    case "Franquicias":
                        optionEtapaEmpresa.innerHTML = "Franchises"
                        break;

                    case "Cómo comenzar":
                        optionEtapaEmpresa.innerHTML = "How to start"
                        break;

                    case "Estructura legal":
                        optionEtapaEmpresa.innerHTML = "Legal structure"
                        break;

                    case "Localización y Zonificación":
                        optionEtapaEmpresa.innerHTML = "Location and Zoning"
                        break;

                    case "Otras áreas de Planificación empresarial":
                        optionEtapaEmpresa.innerHTML = "Other areas of Business Planning"
                        break;

                    case "Otras áreas de Planificación empresarial76":
                        optionEtapaEmpresa.innerHTML = "Other areas of Business Planning"
                        break;

                    case "Eficiencia estratégica":
                        optionEtapaEmpresa.innerHTML = "Strategic efficiency"
                        break;

                    case "Negocios sustentables":
                        optionEtapaEmpresa.innerHTML = "Sustainable business"
                        break;

                    case "Productos ecológicos":
                        optionEtapaEmpresa.innerHTML = "Organic products"
                        break;

                    case "Otras áreas de sustentabilidad":
                        optionEtapaEmpresa.innerHTML = "Other areas of sustainability"
                        break;

                    case "Comercio electrónico":
                        optionEtapaEmpresa.innerHTML = "Electronic commerce"
                        break;

                    case "Gestión de tecnología":
                        optionEtapaEmpresa.innerHTML = "Technology management"
                        break;

                    case "Tecnología e Internet":
                        optionEtapaEmpresa.innerHTML = "Technology and the Internet"
                        break;

                    case "Planificación de recursos tecnológicos":
                        optionEtapaEmpresa.innerHTML = "Technological resource planning"
                        break;

                    case "Telecomunicaciones":
                        optionEtapaEmpresa.innerHTML = "Telecommunications"
                        break;

                    case "Diseño de páginas web":
                        optionEtapaEmpresa.innerHTML = "Web-page design"
                        break;

                    case "Otras áreas en tecnología e internet":
                        optionEtapaEmpresa.innerHTML = "Other areas in technology and internet"
                        break;

                    case "Salud y Alimentación":
                        optionEtapaEmpresa.innerHTML = "Health and Food"
                        break;

                    case "Instrospección, Amor y Sanación de Traumas":
                        optionEtapaEmpresa.innerHTML = "Insight, Love and Trauma Healing"
                        break;

                    case "Instroducción al Pensamiento Crítico":
                        optionEtapaEmpresa.innerHTML = "Introduction to Critical Thinking"
                        break;

                    case "Manejo de Inteligencia Emocional":
                        optionEtapaEmpresa.innerHTML = "Emotional Intelligence Management"
                        break;

                    case "Desarrollo de Mindfulness":
                        optionEtapaEmpresa.innerHTML = "Mindfulness Development"
                        break;
                }

            });
        }
    }

    var ayudasNegocioBox = () => {

        const thisInterval = setInterval(
            function () {
                const idBoxAyudasNegocio = document.querySelectorAll("#id_ayudas_para_negocio-ts-dropdown > .optgroup")

                if (idBoxAyudasNegocio.length > 0) {
                    clearInterval(thisInterval)

                    idBoxAyudasNegocio.forEach(element => {
                        const tagsGroup = element.querySelectorAll(".o-group-tags")

                        tagsGroup.forEach(optionEtapaEmpresa => {

                            switch (optionEtapaEmpresa.dataset.value) {
                                case "Contabilidad":
                                    optionEtapaEmpresa.innerHTML = "Accounting"
                                    break;

                                case "Auditorias":
                                    optionEtapaEmpresa.innerHTML = "Audits"
                                    break;

                                case "Ingresos / Egresos":
                                    optionEtapaEmpresa.innerHTML = "Income / Expenses"
                                    break;

                                case "Presupuesto":
                                    optionEtapaEmpresa.innerHTML = "Budget"
                                    break;

                                case "Flujo de Efectivo":
                                    optionEtapaEmpresa.innerHTML = "Cash Flow"
                                    break;

                                case "Planeación Financiera":
                                    optionEtapaEmpresa.innerHTML = "Financial planning"
                                    break;

                                case "Préstamos y Financiamiento":
                                    optionEtapaEmpresa.innerHTML = "Loans and Financing"
                                    break;

                                case "Otro tipo de Contabilidad":
                                    optionEtapaEmpresa.innerHTML = "Other type of Accounting"
                                    break;

                                case "Impuestos":
                                    optionEtapaEmpresa.innerHTML = "Taxes"
                                    break;

                                case "Compensaciones y Prestaciones":
                                    optionEtapaEmpresa.innerHTML = "Compensation and Benefits"
                                    break;

                                case "Contratistas y Consultores":
                                    optionEtapaEmpresa.innerHTML = "Contractors and Consultants"
                                    break;

                                case "Capacitación de personal":
                                    optionEtapaEmpresa.innerHTML = "Staff training"
                                    break;

                                case "Otra forma de recursos humanos":
                                    optionEtapaEmpresa.innerHTML = "Other form of human resources"
                                    break;

                                case "Políticas del personal":
                                    optionEtapaEmpresa.innerHTML = "Personnel policies"
                                    break;

                                case "Reclutamiento y Contratación":
                                    optionEtapaEmpresa.innerHTML = "Recruitment and Hiring"
                                    break;

                                case "Gestión de voluntarios":
                                    optionEtapaEmpresa.innerHTML = "Volunteer management"
                                    break;

                                case "Aduanas y Aranceles":
                                    optionEtapaEmpresa.innerHTML = "Customs and Tariffs"
                                    break;

                                case "Exportación e Importación":
                                    optionEtapaEmpresa.innerHTML = "Exportation and importation"
                                    break;

                                case "Mercados globales":
                                    optionEtapaEmpresa.innerHTML = "Global markets"
                                    break;

                                case "Otro sector internacional":
                                    optionEtapaEmpresa.innerHTML = "Other international sector"
                                    break;

                                case "Outsourcing":
                                    optionEtapaEmpresa.innerHTML = "Outsourcing"
                                    break;

                                case "Contratos":
                                    optionEtapaEmpresa.innerHTML = "Contracts"
                                    break;

                                case "Derecho laboral":
                                    optionEtapaEmpresa.innerHTML = "Labor law"
                                    break;

                                case "Propiedad intelectual":
                                    optionEtapaEmpresa.innerHTML = "Intellectual property"
                                    break;

                                case "Otra área legal":
                                    optionEtapaEmpresa.innerHTML = "Other legal area"
                                    break;

                                case "Derecho de la propiedad":
                                    optionEtapaEmpresa.innerHTML = "Property law"
                                    break;

                                case "Derecho fiscal":
                                    optionEtapaEmpresa.innerHTML = "Tax law"
                                    break;

                                case "Establecimiento de consejos directivos":
                                    optionEtapaEmpresa.innerHTML = "Establishment of boards of directors"
                                    break;

                                case "Seguros comerciales":
                                    optionEtapaEmpresa.innerHTML = "Commercial insurance"
                                    break;

                                case "Estrategias de negocios":
                                    optionEtapaEmpresa.innerHTML = "Business strategies"
                                    break;

                                case "Reacudación de fondos":
                                    optionEtapaEmpresa.innerHTML = "Fundraising"
                                    break;

                                case "Crecimiento y Desarrollo Profesional":
                                    optionEtapaEmpresa.innerHTML = "Professional Growth and Development"
                                    break;

                                case "Liderazgo":
                                    optionEtapaEmpresa.innerHTML = "Leadership"
                                    break;

                                case "Otras áreas administrativas":
                                    optionEtapaEmpresa.innerHTML = "Other administrative areas"
                                    break;

                                case "Planeamiento y Programación por metas":
                                    optionEtapaEmpresa.innerHTML = "Planning and Programming by goals"
                                    break;

                                case "Administración de proyectos":
                                    optionEtapaEmpresa.innerHTML = "Project management"
                                    break;

                                case "Equilibrio trabajo - vida personal":
                                    optionEtapaEmpresa.innerHTML = "Balance work - personal life"
                                    break;

                                case "Publicidad y Promoción":
                                    optionEtapaEmpresa.innerHTML = "Advertising and Promotional"
                                    break;

                                case "Branding e Identidad Corporativa":
                                    optionEtapaEmpresa.innerHTML = "Branding and Corporate Identity"
                                    break;

                                case "Desarrollo de negocios":
                                    optionEtapaEmpresa.innerHTML = "Business development"
                                    break;

                                case "Distribución":
                                    optionEtapaEmpresa.innerHTML = "Distribution"
                                    break;

                                case "Investigación de mercados":
                                    optionEtapaEmpresa.innerHTML = "Market research"
                                    break;

                                case "Materiales de mercadotecnia":
                                    optionEtapaEmpresa.innerHTML = "marketing materials"
                                    break;

                                case "Estrategias de mercadotecnia":
                                    optionEtapaEmpresa.innerHTML = "Marketing strategies"
                                    break;

                                case "Otra forma de mercadotecnia":
                                    optionEtapaEmpresa.innerHTML = "Another form of marketing"
                                    break;

                                case "Fijación de precios":
                                    optionEtapaEmpresa.innerHTML = "Price fixing"
                                    break;

                                case "Desarrollo de productos":
                                    optionEtapaEmpresa.innerHTML = "Product development"
                                    break;

                                case "Relaciones públicas y Medios":
                                    optionEtapaEmpresa.innerHTML = "Public Relations and Media"
                                    break;

                                case "Redes sociales":
                                    optionEtapaEmpresa.innerHTML = "Social networks"
                                    break;

                                case "Marketing en línea":
                                    optionEtapaEmpresa.innerHTML = "Online marketing"
                                    break;

                                case "Redacción y Edición":
                                    optionEtapaEmpresa.innerHTML = "Writing and Editing"
                                    break;

                                case "Manejo de inventarios":
                                    optionEtapaEmpresa.innerHTML = "Inventory management"
                                    break;

                                case "Logística":
                                    optionEtapaEmpresa.innerHTML = "Logistics"
                                    break;

                                case "Manufactura":
                                    optionEtapaEmpresa.innerHTML = "Manufacture"
                                    break;

                                case "Otras operaciones":
                                    optionEtapaEmpresa.innerHTML = "Other operations"
                                    break;

                                case "Embalaje y etiquetado":
                                    optionEtapaEmpresa.innerHTML = "Packaging and labeling"
                                    break;

                                case "Mejora de procesos y optimización":
                                    optionEtapaEmpresa.innerHTML = "Process improvement and optimization"
                                    break;

                                case "Adquisiciones y Proveedores":
                                    optionEtapaEmpresa.innerHTML = "Acquisitions and Suppliers"
                                    break;

                                case "Diseño de programas y evaluación":
                                    optionEtapaEmpresa.innerHTML = "Program design and evaluation"
                                    break;

                                case "Gestión de calidad":
                                    optionEtapaEmpresa.innerHTML = "Quality management"
                                    break;

                                case "Transporte y Entrega":
                                    optionEtapaEmpresa.innerHTML = "Transportation and Delivery"
                                    break;

                                case "Servicio / Atención al cliente y Gestion de relaciones comerciales":
                                    optionEtapaEmpresa.innerHTML = "Service / Customer Service and Business Relationship Management"
                                    break;

                                case "Contratos gubernamentales":
                                    optionEtapaEmpresa.innerHTML = "Government contracts"
                                    break;

                                case "Generacion de contactos":
                                    optionEtapaEmpresa.innerHTML = "Generation of contacts"
                                    break;

                                case "Otras áreas de ventas":
                                    optionEtapaEmpresa.innerHTML = "Other sales areas"
                                    break;

                                case "Venta al público":
                                    optionEtapaEmpresa.innerHTML = "Sale to the public"
                                    break;

                                case "Venta de servicios":
                                    optionEtapaEmpresa.innerHTML = "Sale of services"
                                    break;

                                case "Venta al por mayor B2B":
                                    optionEtapaEmpresa.innerHTML = "B2B wholesale"
                                    break;

                                case "Planificación empresarial":
                                    optionEtapaEmpresa.innerHTML = "Business planning"
                                    break;

                                case "Franquicias":
                                    optionEtapaEmpresa.innerHTML = "Franchises"
                                    break;

                                case "Cómo comenzar":
                                    optionEtapaEmpresa.innerHTML = "How to start"
                                    break;

                                case "Estructura legal":
                                    optionEtapaEmpresa.innerHTML = "Legal structure"
                                    break;

                                case "Localización y Zonificación":
                                    optionEtapaEmpresa.innerHTML = "Location and Zoning"
                                    break;

                                case "Otras áreas de Planificación empresarial":
                                    optionEtapaEmpresa.innerHTML = "Other areas of Business Planning"
                                    break;

                                case "Otras áreas de Planificación empresarial76":
                                    optionEtapaEmpresa.innerHTML = "Other areas of Business Planning"
                                    break;

                                case "Eficiencia estratégica":
                                    optionEtapaEmpresa.innerHTML = "Strategic efficiency"
                                    break;

                                case "Negocios sustentables":
                                    optionEtapaEmpresa.innerHTML = "Sustainable business"
                                    break;

                                case "Productos ecológicos":
                                    optionEtapaEmpresa.innerHTML = "Organic products"
                                    break;

                                case "Otras áreas de sustentabilidad":
                                    optionEtapaEmpresa.innerHTML = "Other areas of sustainability"
                                    break;

                                case "Comercio electrónico":
                                    optionEtapaEmpresa.innerHTML = "Electronic commerce"
                                    break;

                                case "Gestión de tecnología":
                                    optionEtapaEmpresa.innerHTML = "Technology management"
                                    break;

                                case "Tecnología e Internet":
                                    optionEtapaEmpresa.innerHTML = "Technology and the Internet"
                                    break;

                                case "Planificación de recursos tecnológicos":
                                    optionEtapaEmpresa.innerHTML = "Technological resource planning"
                                    break;

                                case "Telecomunicaciones":
                                    optionEtapaEmpresa.innerHTML = "Telecommunications"
                                    break;

                                case "Diseño de páginas web":
                                    optionEtapaEmpresa.innerHTML = "Web-page design"
                                    break;

                                case "Otras áreas en tecnología e internet":
                                    optionEtapaEmpresa.innerHTML = "Other areas in technology and internet"
                                    break;

                                case "Salud y Alimentación":
                                    optionEtapaEmpresa.innerHTML = "Health and Food"
                                    break;

                                case "Instrospección, Amor y Sanación de Traumas":
                                    optionEtapaEmpresa.innerHTML = "Insight, Love and Trauma Healing"
                                    break;

                                case "Instroducción al Pensamiento Crítico":
                                    optionEtapaEmpresa.innerHTML = "Introduction to Critical Thinking"
                                    break;

                                case "Manejo de Inteligencia Emocional":
                                    optionEtapaEmpresa.innerHTML = "Emotional Intelligence Management"
                                    break;

                                case "Desarrollo de Mindfulness":
                                    optionEtapaEmpresa.innerHTML = "Mindfulness Development"
                                    break;
                            }
                        });
                    });
                }

            }, 20);

    }

    var ayudasNegocioBoxTag = () => {

        const thisInterval = setInterval(
            function () {
                if (document.querySelector("#id_ayudas_para_negocio")) {
                    if (document.querySelector("#id_ayudas_para_negocio").closest(".o-form__field").querySelector(".ts-control")) {
                        const idBoxAyudasNegocio = document.querySelector("#id_ayudas_para_negocio").closest(".o-form__field").querySelector(".ts-control").querySelectorAll(".o-tag")

                        if (idBoxAyudasNegocio.length > 0) {
                            idBoxAyudasNegocio.forEach(optionEtapaEmpresa => {

                                switch (optionEtapaEmpresa.dataset.value) {
                                    case "Contabilidad":
                                        optionEtapaEmpresa.innerHTML = "Accounting"
                                        break;

                                    case "Auditorias":
                                        optionEtapaEmpresa.innerHTML = "Audits"
                                        break;

                                    case "Ingresos / Egresos":
                                        optionEtapaEmpresa.innerHTML = "Income / Expenses"
                                        break;

                                    case "Presupuesto":
                                        optionEtapaEmpresa.innerHTML = "Budget"
                                        break;

                                    case "Flujo de Efectivo":
                                        optionEtapaEmpresa.innerHTML = "Cash Flow"
                                        break;

                                    case "Planeación Financiera":
                                        optionEtapaEmpresa.innerHTML = "Financial planning"
                                        break;

                                    case "Préstamos y Financiamiento":
                                        optionEtapaEmpresa.innerHTML = "Loans and Financing"
                                        break;

                                    case "Otro tipo de Contabilidad":
                                        optionEtapaEmpresa.innerHTML = "Other type of Accounting"
                                        break;

                                    case "Impuestos":
                                        optionEtapaEmpresa.innerHTML = "Taxes"
                                        break;

                                    case "Compensaciones y Prestaciones":
                                        optionEtapaEmpresa.innerHTML = "Compensation and Benefits"
                                        break;

                                    case "Contratistas y Consultores":
                                        optionEtapaEmpresa.innerHTML = "Contractors and Consultants"
                                        break;

                                    case "Capacitación de personal":
                                        optionEtapaEmpresa.innerHTML = "Staff training"
                                        break;

                                    case "Otra forma de recursos humanos":
                                        optionEtapaEmpresa.innerHTML = "Other form of human resources"
                                        break;

                                    case "Políticas del personal":
                                        optionEtapaEmpresa.innerHTML = "Personnel policies"
                                        break;

                                    case "Reclutamiento y Contratación":
                                        optionEtapaEmpresa.innerHTML = "Recruitment and Hiring"
                                        break;

                                    case "Gestión de voluntarios":
                                        optionEtapaEmpresa.innerHTML = "Volunteer management"
                                        break;

                                    case "Aduanas y Aranceles":
                                        optionEtapaEmpresa.innerHTML = "Customs and Tariffs"
                                        break;

                                    case "Exportación e Importación":
                                        optionEtapaEmpresa.innerHTML = "Exportation and importation"
                                        break;

                                    case "Mercados globales":
                                        optionEtapaEmpresa.innerHTML = "Global markets"
                                        break;

                                    case "Otro sector internacional":
                                        optionEtapaEmpresa.innerHTML = "Other international sector"
                                        break;

                                    case "Outsourcing":
                                        optionEtapaEmpresa.innerHTML = "Outsourcing"
                                        break;

                                    case "Contratos":
                                        optionEtapaEmpresa.innerHTML = "Contracts"
                                        break;

                                    case "Derecho laboral":
                                        optionEtapaEmpresa.innerHTML = "Labor law"
                                        break;

                                    case "Propiedad intelectual":
                                        optionEtapaEmpresa.innerHTML = "Intellectual property"
                                        break;

                                    case "Otra área legal":
                                        optionEtapaEmpresa.innerHTML = "Other legal area"
                                        break;

                                    case "Derecho de la propiedad":
                                        optionEtapaEmpresa.innerHTML = "Property law"
                                        break;

                                    case "Derecho fiscal":
                                        optionEtapaEmpresa.innerHTML = "Tax law"
                                        break;

                                    case "Establecimiento de consejos directivos":
                                        optionEtapaEmpresa.innerHTML = "Establishment of boards of directors"
                                        break;

                                    case "Seguros comerciales":
                                        optionEtapaEmpresa.innerHTML = "Commercial insurance"
                                        break;

                                    case "Estrategias de negocios":
                                        optionEtapaEmpresa.innerHTML = "Business strategies"
                                        break;

                                    case "Reacudación de fondos":
                                        optionEtapaEmpresa.innerHTML = "Fundraising"
                                        break;

                                    case "Crecimiento y Desarrollo Profesional":
                                        optionEtapaEmpresa.innerHTML = "Professional Growth and Development"
                                        break;

                                    case "Liderazgo":
                                        optionEtapaEmpresa.innerHTML = "Leadership"
                                        break;

                                    case "Otras áreas administrativas":
                                        optionEtapaEmpresa.innerHTML = "Other administrative areas"
                                        break;

                                    case "Planeamiento y Programación por metas":
                                        optionEtapaEmpresa.innerHTML = "Planning and Programming by goals"
                                        break;

                                    case "Administración de proyectos":
                                        optionEtapaEmpresa.innerHTML = "Project management"
                                        break;

                                    case "Equilibrio trabajo - vida personal":
                                        optionEtapaEmpresa.innerHTML = "Balance work - personal life"
                                        break;

                                    case "Publicidad y Promoción":
                                        optionEtapaEmpresa.innerHTML = "Advertising and Promotional"
                                        break;

                                    case "Branding e Identidad Corporativa":
                                        optionEtapaEmpresa.innerHTML = "Branding and Corporate Identity"
                                        break;

                                    case "Desarrollo de negocios":
                                        optionEtapaEmpresa.innerHTML = "Business development"
                                        break;

                                    case "Distribución":
                                        optionEtapaEmpresa.innerHTML = "Distribution"
                                        break;

                                    case "Investigación de mercados":
                                        optionEtapaEmpresa.innerHTML = "Market research"
                                        break;

                                    case "Materiales de mercadotecnia":
                                        optionEtapaEmpresa.innerHTML = "marketing materials"
                                        break;

                                    case "Estrategias de mercadotecnia":
                                        optionEtapaEmpresa.innerHTML = "Marketing strategies"
                                        break;

                                    case "Otra forma de mercadotecnia":
                                        optionEtapaEmpresa.innerHTML = "Another form of marketing"
                                        break;

                                    case "Fijación de precios":
                                        optionEtapaEmpresa.innerHTML = "Price fixing"
                                        break;

                                    case "Desarrollo de productos":
                                        optionEtapaEmpresa.innerHTML = "Product development"
                                        break;

                                    case "Relaciones públicas y Medios":
                                        optionEtapaEmpresa.innerHTML = "Public Relations and Media"
                                        break;

                                    case "Redes sociales":
                                        optionEtapaEmpresa.innerHTML = "Social networks"
                                        break;

                                    case "Marketing en línea":
                                        optionEtapaEmpresa.innerHTML = "Online marketing"
                                        break;

                                    case "Redacción y Edición":
                                        optionEtapaEmpresa.innerHTML = "Writing and Editing"
                                        break;

                                    case "Manejo de inventarios":
                                        optionEtapaEmpresa.innerHTML = "Inventory management"
                                        break;

                                    case "Logística":
                                        optionEtapaEmpresa.innerHTML = "Logistics"
                                        break;

                                    case "Manufactura":
                                        optionEtapaEmpresa.innerHTML = "Manufacture"
                                        break;

                                    case "Otras operaciones":
                                        optionEtapaEmpresa.innerHTML = "Other operations"
                                        break;

                                    case "Embalaje y etiquetado":
                                        optionEtapaEmpresa.innerHTML = "Packaging and labeling"
                                        break;

                                    case "Mejora de procesos y optimización":
                                        optionEtapaEmpresa.innerHTML = "Process improvement and optimization"
                                        break;

                                    case "Adquisiciones y Proveedores":
                                        optionEtapaEmpresa.innerHTML = "Acquisitions and Suppliers"
                                        break;

                                    case "Diseño de programas y evaluación":
                                        optionEtapaEmpresa.innerHTML = "Program design and evaluation"
                                        break;

                                    case "Gestión de calidad":
                                        optionEtapaEmpresa.innerHTML = "Quality management"
                                        break;

                                    case "Transporte y Entrega":
                                        optionEtapaEmpresa.innerHTML = "Transportation and Delivery"
                                        break;

                                    case "Servicio / Atención al cliente y Gestion de relaciones comerciales":
                                        optionEtapaEmpresa.innerHTML = "Service / Customer Service and Business Relationship Management"
                                        break;

                                    case "Contratos gubernamentales":
                                        optionEtapaEmpresa.innerHTML = "Government contracts"
                                        break;

                                    case "Generacion de contactos":
                                        optionEtapaEmpresa.innerHTML = "Generation of contacts"
                                        break;

                                    case "Otras áreas de ventas":
                                        optionEtapaEmpresa.innerHTML = "Other sales areas"
                                        break;

                                    case "Venta al público":
                                        optionEtapaEmpresa.innerHTML = "Sale to the public"
                                        break;

                                    case "Venta de servicios":
                                        optionEtapaEmpresa.innerHTML = "Sale of services"
                                        break;

                                    case "Venta al por mayor B2B":
                                        optionEtapaEmpresa.innerHTML = "B2B wholesale"
                                        break;

                                    case "Planificación empresarial":
                                        optionEtapaEmpresa.innerHTML = "Business planning"
                                        break;

                                    case "Franquicias":
                                        optionEtapaEmpresa.innerHTML = "Franchises"
                                        break;

                                    case "Cómo comenzar":
                                        optionEtapaEmpresa.innerHTML = "How to start"
                                        break;

                                    case "Estructura legal":
                                        optionEtapaEmpresa.innerHTML = "Legal structure"
                                        break;

                                    case "Localización y Zonificación":
                                        optionEtapaEmpresa.innerHTML = "Location and Zoning"
                                        break;

                                    case "Otras áreas de Planificación empresarial":
                                        optionEtapaEmpresa.innerHTML = "Other areas of Business Planning"
                                        break;

                                    case "Otras áreas de Planificación empresarial76":
                                        optionEtapaEmpresa.innerHTML = "Other areas of Business Planning"
                                        break;

                                    case "Eficiencia estratégica":
                                        optionEtapaEmpresa.innerHTML = "Strategic efficiency"
                                        break;

                                    case "Negocios sustentables":
                                        optionEtapaEmpresa.innerHTML = "Sustainable business"
                                        break;

                                    case "Productos ecológicos":
                                        optionEtapaEmpresa.innerHTML = "Organic products"
                                        break;

                                    case "Otras áreas de sustentabilidad":
                                        optionEtapaEmpresa.innerHTML = "Other areas of sustainability"
                                        break;

                                    case "Comercio electrónico":
                                        optionEtapaEmpresa.innerHTML = "Electronic commerce"
                                        break;

                                    case "Gestión de tecnología":
                                        optionEtapaEmpresa.innerHTML = "Technology management"
                                        break;

                                    case "Tecnología e Internet":
                                        optionEtapaEmpresa.innerHTML = "Technology and the Internet"
                                        break;

                                    case "Planificación de recursos tecnológicos":
                                        optionEtapaEmpresa.innerHTML = "Technological resource planning"
                                        break;

                                    case "Telecomunicaciones":
                                        optionEtapaEmpresa.innerHTML = "Telecommunications"
                                        break;

                                    case "Diseño de páginas web":
                                        optionEtapaEmpresa.innerHTML = "Web-page design"
                                        break;

                                    case "Otras áreas en tecnología e internet":
                                        optionEtapaEmpresa.innerHTML = "Other areas in technology and internet"
                                        break;

                                    case "Salud y Alimentación":
                                        optionEtapaEmpresa.innerHTML = "Health and Food"
                                        break;

                                    case "Instrospección, Amor y Sanación de Traumas":
                                        optionEtapaEmpresa.innerHTML = "Insight, Love and Trauma Healing"
                                        break;

                                    case "Instroducción al Pensamiento Crítico":
                                        optionEtapaEmpresa.innerHTML = "Introduction to Critical Thinking"
                                        break;

                                    case "Manejo de Inteligencia Emocional":
                                        optionEtapaEmpresa.innerHTML = "Emotional Intelligence Management"
                                        break;

                                    case "Desarrollo de Mindfulness":
                                        optionEtapaEmpresa.innerHTML = "Mindfulness Development"
                                        break;


                                }
                            });
                        }
                    }
                }
            }, 10);
    }

    var headerSteps = () => {
        if (document.querySelector(".o-steps__action")) {

            let textStep = document.querySelector(".o-steps__action");


            if (textStep.innerHTML.trim() == "Completar Información acerca de tu negocio") {
                textStep.innerHTML = "Complete information about your business"
            } else if (textStep.innerHTML.trim() == "Completar Información personal") {
                textStep.innerHTML = "Complete Personal Information"
            }
        }
    }

    optionsEtapaEmpresa()
    optionsEtapaNegocio()
    diasDisponibilidad()
    gender()
    id_prefered_language()
    questionType()
    tipoEmpresa()
    tipoNegocio()
    industriaNegocio()
    // ayudasNegocio()
    ayudasNegocioBox()
    ayudasNegocioBoxTag()
    headerSteps()
}