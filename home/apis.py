import requests

class ConnectApi:
  url_api = None
  data_save = None
  rta_json = None
  dict_paises = []

  def paises_america(self):
    self.url_api = 'https://restcountries.com/v2/all'

    self.data_save = requests.get(self.url_api)

    self.rta_json = self.data_save.json()

    for data_paises in self.rta_json:
      data_dict_paises = (
        (data_paises['name'], data_paises['name'])
      )

      self.dict_paises.append(data_dict_paises)


    return self.dict_paises
    