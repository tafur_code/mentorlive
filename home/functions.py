import os
import requests
import datetime

from django.conf import settings
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from django.core.exceptions import ValidationError
from social_django.models import UserSocialAuth
from django.contrib.sites.models import Site


def send_user_mail(object_user, action):
    subject = ""
    list_dest = []

    if action == "register_user":
        subject = 'Confirmación Registro Exitoso'
        template = get_template('email/register_confirmation.html')
        list_dest = [object_user.email]

    elif action == "edit_user":
        subject = 'Modificación de datos Exitosa'
        template = get_template('email/edit_profile_confirmation.html')
        list_dest = [object_user.email]

    elif action == "schedule_user":
        subject = 'Agendamiento de Mentoría'
        template = get_template('email/schedule_appointment.html')
        list_dest = [object_user.mentor, object_user.emprendedor.email]

    elif action == "suggestion":
        subject = 'Sugerencia de Mentoría'
        template = get_template('email/suggestion_mentoria.html')
        list_dest = [object_user.mentor, object_user.emprendedor.email]

    elif action == "schedule_state_user":
        subject = 'Estado de Mentoría'
        template = get_template('email/state_appointment.html')
        list_dest = [object_user.mentor, object_user.emprendedor.email]

    elif action == "mentor_register":
        subject = 'Confirmación Registro Exitoso'
        template = get_template('email/register_mentor_confirmation.html')
        list_dest = [object_user.email]

    elif action == "contact_user":
        subject = 'Contacto'
        template = get_template('email/contact.html')
        list_dest = [object_user.email_contact]

    content = template.render({
        'user': object_user,
        "site": Site.objects.get_current()
    })

    message = EmailMultiAlternatives(subject,  # Titulo
                                     '',
                                     "Altruism Now Mentors",  # Remitente
                                     list_dest)  # Destinatario

    message.attach_alternative(content, 'text/html')
    message.send()


def validate_user_authenticate(user):
    validate = False

    if user.is_authenticated:
        validate = True

    return validate

def validate_user_authenticate_emprendedor(user, Object):
    validate = False
    
    if(isinstance(user, Object)):
        if user.is_authenticated and not user.user_rol == "1":
            validate = True
    else:
        validate = False 

    return validate


def validate_href_contnt(value):
    if "https://" in value:
        return value
    else:
        raise ValidationError(
            "Este campo tiene que ser una url, asegurese que contenga https://"
        )


def validate_title_of_page_profile(user):
    title_page = ''

    for users in UserSocialAuth.objects.all():
        # Titulo para nombre de mentores
        if users.extra_data['email_address'] == user.email:
            if users.extra_data['first_name'] and users.extra_data['last_name']:
                title_page = f'{ users.extra_data["first_name"] } { users.extra_data["last_name"] }'
        else:
            # Titulo para nombre de emprendedores
            title_page = user.names

    return title_page


def validate_dias_estimados(citas):
    current_time = datetime.datetime.now()
    hora_reunion = ""

    a = datetime.datetime(current_time.year, current_time.month, current_time.day,
                          current_time.hour, current_time.minute, current_time.second)

    for cita in citas:
        b = datetime.datetime(cita.created_at.year, cita.created_at.month, cita.created_at.day,
                              cita.created_at.hour, cita.created_at.minute, cita.created_at.second)

        c = a-b
        minutes = divmod(c.seconds, 60)
        diferencia_dias = ""

        if c.days == 0:
            if minutes[0] <= 5:
                diferencia_dias = f"un momento"
            elif minutes[0] <= 60:
                diferencia_dias = f"{ minutes[0] } minutos"
            else:
                diferencia_dias = f"{ round(c.seconds/3600) } horas"
        else:
            if c.days == 1:
                diferencia_dias = f"{ c.days } dia"
            else:
                diferencia_dias = f"{ c.days } dias"

    return diferencia_dias
