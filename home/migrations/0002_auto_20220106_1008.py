# Generated by Django 3.2.8 on 2022-01-06 15:08

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='detaillist',
            options={'verbose_name': 'Plan mentores/emprendedores'},
        ),
        migrations.AlterField(
            model_name='detaillist',
            name='description_emprendedor',
            field=ckeditor.fields.RichTextField(verbose_name='Plan emprendedores'),
        ),
        migrations.AlterField(
            model_name='detaillist',
            name='description_mentors',
            field=ckeditor.fields.RichTextField(verbose_name='Plan mentores'),
        ),
    ]
