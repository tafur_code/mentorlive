import datetime
import csv

from social_django.models import UserSocialAuth

from home.models import AdsContactMentor
from rol_users.models import Emprendedor, Mentor
from cita.models import Cita
from user.models import UserCustom


def ads_contact(request):
    ads_contact_mentor = AdsContactMentor.objects.all()

    return {
        'ads_contact_mentor': ads_contact_mentor
    }


def data_rol_user(request):
    data_emprendedor_id = []
    data_mentor_id = []

    if request.user.is_authenticated:
        if not request.user.user_rol == '1':
            data_emprendedor_id = Emprendedor.objects.filter(
                id_emprendedor=request.user.id).first()

        else:
            data_mentor_id = Mentor.objects.filter(
                id_mentor=request.user.id).first()

    return {
        'data_mentor_id': data_mentor_id,
        'data_emprendedor_id': data_emprendedor_id
    }


def get_indicatives(request):

    dict_indicates = []

    with open('./paises.csv') as File:
        reader = csv.DictReader(File)
        for row in reader:
            dict_indicates.append(row)

    return {
        'dict_indicates': dict_indicates
    }


def count_notifications(request):
    citas = 0

    if isinstance(request.user, UserCustom):
        if request.user.user_rol == '1':
            user = UserSocialAuth.objects.filter(user=request.user).first()
            citas = Cita.objects.filter(
                mentor=user).filter(state="espera").count()
        else:
            user = UserCustom.object.filter(id=request.user.id).first()
            citas = Cita.objects.filter(
                emprendedor=user).filter(state="espera").count()

    return {
        "citas_count": citas
    }


def get_active_mentoria_to_emprendedor(request):
    cita_active = ""
    cita_finished = ""
    id_cita_active = -1
    id_cita_finished = -1
    count_cita_active = False
    count_cita_finished = False

    current_time = datetime.datetime.now()

    if isinstance(request.user, UserCustom):
        user = UserCustom.object.filter(id=request.user.id).first()

        if not user.user_rol == "1" and not user.user_rol == "ad":

            cita_active = Cita.objects.filter(
                emprendedor=user).filter(state="aceptada").first()

            if cita_active:

                a = datetime.datetime(current_time.year, current_time.month, current_time.day,
                                      current_time.hour, current_time.minute, current_time.second)

                b = datetime.datetime(cita_active.fecha_confirmada_reunion.year, cita_active.fecha_confirmada_reunion.month, cita_active.fecha_confirmada_reunion.day,
                                      cita_active.fecha_confirmada_reunion.hour, cita_active.fecha_confirmada_reunion.minute, cita_active.fecha_confirmada_reunion.second)
                c = b-a

                if c.total_seconds() <= 86400:
                    id_cita_active = cita_active.id
                    count_cita_active = True

            cita_finished = Cita.objects.filter(
                emprendedor=user).filter(state="aceptada_emprendedor").first()

            if cita_finished:
                if cita_finished.fecha_confirmada_reunion < current_time:
                    id_cita_finished = cita_finished.id
                    count_cita_finished = True

    return {
        "id_cita_active": id_cita_active,
        "id_cita_finished": id_cita_finished,
        "count_cita_active": count_cita_active,
        "count_cita_finished": count_cita_finished,
    }
