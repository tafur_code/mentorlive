from django.db import models

# Richtext of django
from ckeditor.fields import RichTextField

# importing validation errors
from django.core.exceptions import ValidationError


def validate_href_contnt(value):
    if "https://" in value:
        return value
    else:
        raise ValidationError(
            "Este campo tiene que ser una url, asegurese que contenga https://"
        )

# Create your models here.


class IntroHero(models.Model):
    title = models.CharField(
        max_length=100, verbose_name='Titulo', blank=False)
    subtitle = models.CharField(max_length=300, verbose_name='Subtitulo')
    paragraph = models.CharField(
        max_length=300, verbose_name='Parrafo', blank=True)
    text_button = models.CharField(
        max_length=50, verbose_name='Texto botón', blank=False)
    href_button = models.CharField(
        max_length=1000,
        verbose_name='Enlace del botón',
        blank=False,
        validators=[validate_href_contnt]
    )
    text_button_secondary = models.CharField(
        max_length=50, verbose_name='Texto segundo botón', blank=True)
    href_button_secondary = models.CharField(
        max_length=1000,
        verbose_name='Enlace del segundo botón',
        blank=True,
        validators=[validate_href_contnt]
    )
    image = models.ImageField(
        default='null', verbose_name='Imagen de fondo', upload_to='hero_image')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Creado el')

    class Meta:
        verbose_name = 'Hero'
        verbose_name_plural = 'Heros'

    def __str__(self):
        return self.title

    # def save(self, *args, **kwargs):
    #     super(IntroHero, self).save(*args, **kwargs)


class DetailList(models.Model):
    title = "Cards lista detalle emprendedores/mentores"
    description_mentors = RichTextField(
        verbose_name='Plan mentores', config_name='list_plans_ckeditor')
    description_emprendedor = RichTextField(
        verbose_name='Plan emprendedores', config_name='list_plans_ckeditor')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Creado el')

    class Meta:
        verbose_name = 'Plan mentores/emprendedores'

    def __str__(self):
        return self.title


class Banner(models.Model):
    title = models.CharField(
        max_length=100,
        verbose_name="Titulo"
    )
    description = models.TextField(
        max_length=500, verbose_name='Descripción', blank=True)
    text_button = models.CharField(
        max_length=50, verbose_name='Texto botón', blank=True)
    href_button = models.CharField(
        max_length=1000,
        verbose_name='Enlace del botón',
        blank=True,
        validators=[validate_href_contnt]
    )
    image = models.ImageField(
        default='null', verbose_name='Imagen informativa', upload_to='banner_centered')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Creado el')

    class Meta:
        verbose_name = 'Banner como lo hacemos'

    def __str__(self):
        return self.title


class AdsContactMentor(models.Model):
    title = models.CharField(
        max_length=100,
        verbose_name="Titulo"
    )
    description = models.TextField(
        max_length=500, verbose_name='Descripción')
    created_at = models.DateTimeField(
        auto_now_add=True, verbose_name='Creado el')

    class Meta:
        verbose_name = 'Ads Contacta a tu mentor'

    def __str__(self):
        return self.title
