from django.contrib.sites.models import Site
from django.shortcuts import render

from home.models import IntroHero, DetailList, Banner, AdsContactMentor
from generalSettings.models import BrandSettings, ContactInformation


# Create your views here.
def index_view(request):
    # list of intro heros
    intro_heros = IntroHero.objects.all()
    detail_list = DetailList.objects.all()
    banner = Banner.objects.all()
    brand_settings = BrandSettings.objects.all()
    contact_information = ContactInformation.objects.all()

    return render(request, "pages/index.html", {
        "title": "Página principal",
        'list_intro_hero': intro_heros,
        'detail_list': detail_list,
        'banner': banner,
        'brand_settings': brand_settings,
        'contact_information': contact_information
    })
