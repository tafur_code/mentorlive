from django.contrib import admin
from .models import IntroHero, DetailList, Banner, AdsContactMentor

# Register your models here.


class IntroHeroAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    readonly_fields = ('created_at',)
    list_display = ('title', 'created_at')
    search_fields = ('title', 'description')


class DetailListAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    readonly_fields = ('created_at',)
    list_display = ('title', 'created_at')
    search_fields = ('title', 'description')


class BannerAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    readonly_fields = ('created_at',)
    list_display = ('title', 'created_at')
    search_fields = ('title', 'description')


class AdsContactMentorAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    readonly_fields = ('created_at',)
    list_display = ('title', 'created_at')
    search_fields = ('title', 'description')


admin.site.register(IntroHero, IntroHeroAdmin)
admin.site.register(DetailList, DetailListAdmin)
admin.site.register(Banner, BannerAdmin)
admin.site.register(AdsContactMentor, AdsContactMentorAdmin)
