from modeltranslation.translator import translator, TranslationOptions
from .models import IntroHero, DetailList, Banner, AdsContactMentor


class IntroHeroTranslationOptions(TranslationOptions):
    fields = ('title', 'subtitle', 'paragraph', 'text_button', 'text_button_secondary')


class DetailListTranslationOptions(TranslationOptions):
    fields = ('description_mentors', 'description_emprendedor')


class BannerTranslationOptions(TranslationOptions):
    fields = ('title', 'description', 'text_button')


class AdsContactTranslationOptions(TranslationOptions):
    fields = ('title', 'description')


translator.register(IntroHero, IntroHeroTranslationOptions)
translator.register(DetailList, DetailListTranslationOptions)
translator.register(Banner, BannerTranslationOptions)
translator.register(AdsContactMentor, AdsContactTranslationOptions)
