from faulthandler import disable
from django.db import models
from django.conf import settings
from social_django.models import UserSocialAuth
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.urls import reverse

from user.models import UserCustom

# Create your models here.

STATES_SUGESTION = (
    ('espera', _('En espera')),
    ('aceptada', _('Aceptada')),
    ('cancelada', _('Cancelada')),
)


class Suggestion(models.Model):
    mentor = models.ForeignKey(
        UserSocialAuth, on_delete=models.CASCADE, verbose_name='Correo de contacto del Mentor')

    emprendedor = models.ForeignKey(UserCustom,
                                    verbose_name='Correo de contacto del Emprendedor', on_delete=models.CASCADE)

    description_text = models.TextField(
        max_length=500, verbose_name=_('Cuentale al emprendedor por que estas interesado'))

    state = models.CharField(
        'Estado',
        max_length=100,
        choices=STATES_SUGESTION,
        blank=True
    )

    created_at = models.DateTimeField(
        #    verbose_name='Creado el', blank=True, default=datetime.datetime.now())
        verbose_name='Creado el', blank=True, default=timezone.now)

    class Meta:
        verbose_name = 'Sugerencia'
        verbose_name_plural = 'Sugerencias'

    def __str__(self):
        return f"{ self.id } - { self.mentor }"

    def get_absolute_url(self):
        return reverse("profile")
