from django.shortcuts import render
from django.views.generic import CreateView
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.translation import gettext_lazy as _

from .models import Suggestion
from .forms import CreateSuggestion

# Create your views here.


class CreteSuggestionView(SuccessMessageMixin, CreateView):
    model = Suggestion
    form_class = CreateSuggestion
    template_name = 'pages/user/create_suggestion.html'
    success_message = _(
        "Se le ha notificado al Emprendedor respecto a la sugerencia.")

    def get(self, request):

        form = self.form_class

        return render(request, self.template_name, {'title': 'Enviar sugerencia', 'form': form})
