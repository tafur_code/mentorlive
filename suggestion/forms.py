from django import forms
from django.utils.translation import gettext_lazy as _

from .models import Suggestion
from home.functions import send_user_mail


class CreateSuggestion(forms.ModelForm):
    class Meta:
        model = Suggestion
        fields = (
            'mentor',
            'emprendedor',
            'description_text',
        )

    def save(self, commit=True):
        suggestion = super().save(commit=False)
        suggestion.state = 'espera'

        send_user_mail(suggestion, 'suggestion')

        if commit:
            suggestion.save()

        return suggestion


class EditSuggestion(forms.ModelForm):
    class Meta:
        model = Suggestion
        fields = (
            'state',
        )

    def save(self, commit=True):
        suggestion = super().save(commit=False)
        # suggestion.state = 'espera'

        # send_user_mail(suggestion, 'suggestion')

        if commit:
            suggestion.save()

        return suggestion
