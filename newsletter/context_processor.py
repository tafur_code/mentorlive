from .models import Newsletter


def get_newsletter(request):
    newsletter_info = Newsletter.objects.values_list('title', 'slug')
    dict_newsletter_values = []

    for info_newsletter in newsletter_info:

        value_list = {
            'title': info_newsletter[0],
            'slug':  info_newsletter[1]
        }

        dict_newsletter_values.append(value_list)
    
    return {
        'newsletter_info': dict_newsletter_values
    }
