import imp
from django.contrib import admin

from .models import mentorsCan, stepsMentoria

# Register your models here.


class MentorsCanAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    list_display = ('title_band',)
    search_fields = ('title_band',)


class StepsAdmin(admin.ModelAdmin):
    # Para agregar campos de solo lectura en los modelos
    list_display = ('title',)
    search_fields = ('title',)


admin.site.register(mentorsCan, MentorsCanAdmin)
admin.site.register(stepsMentoria, StepsAdmin)
