from django.db import models

# Richtext of django
from ckeditor.fields import RichTextField

# Create your models here.


class mentorsCan(models.Model):
    list_objects = RichTextField(
        verbose_name='Los mentores pueden', config_name='list_plans_ckeditor')
    title_band = models.CharField(
        max_length=100, verbose_name='Titulo banda', blank=False)
    text_band = models.CharField(
        max_length=100, verbose_name='Texto banda', blank=False)

    class Meta:
        verbose_name = 'Lista cosas que pueden hacer los mentores'

    def __str__(self):
        return self.title_band


class stepsMentoria(models.Model):
    title = models.CharField(
        max_length=100, verbose_name='Titulo paso', blank=False)
    text = models.CharField(
        max_length=100, verbose_name='Texto paso', blank=False)

    class Meta:
        verbose_name = 'Pasos para aprovechar mentoria'

    def __str__(self):
        return self.title
