from modeltranslation.translator import translator, TranslationOptions

from .models import mentorsCan, stepsMentoria


class mentorsCanTranslationOptions(TranslationOptions):
    fields = ('list_objects', 'title_band', 'text_band')


class stepsMentoriaTranslationOptions(TranslationOptions):
    fields = ('title', 'text')


translator.register(mentorsCan, mentorsCanTranslationOptions)
translator.register(stepsMentoria, stepsMentoriaTranslationOptions)
